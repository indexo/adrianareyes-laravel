<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;

class PropertyTest extends DuskTestCase
{
    /**
     * @test
     */
    public function index()
    {
        /* Administración: Inmuebles */
        $this->browse(function ($first) {
            $first->loginAs(User::where('name', 'Testing')->first())
                ->visit('/administracion/inmuebles')
                ->assertSee('Inmuebles');
        });
    }
}
