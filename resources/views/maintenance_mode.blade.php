<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <title>{!! env('APP_NAME') !!} | Proximamente</title>

    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    <style>
        html,
        body{
            height: 100%;
        }

        @media (max-width: 799px) {
            #cover {
                background-color: #222;
                background-size: cover;
                height: 100%;
                text-align: center;
                display: flex;
                align-items: center;
            }
        }

        @media (min-width: 800px) {
            #cover {
                background: #222 url('/public/construction.gif') center center no-repeat;
                background-size: cover;
                height: 100%;
                text-align: center;
                display: flex;
                align-items: center;
            }
        }

        #cover-caption {
            width: 100%;
        }
    </style>
    <script src="//www.google.com/recaptcha/api.js"
            async
            defer></script>
    <script>
        function onSubmit(token) {
            document.getElementById("contact-us").submit();
        }
    </script>
</head>
<body>
    <section id="cover">
        <div id="cover-caption">
            <div id="container" class="container">
                <div class="row text-white">
                    <div class="col-sm-10 offset-sm-1 text-center">
                        <h1 class="display-3">P&aacute;gina en construcci&oacute;n</h1>
                        <div class="info-form">
                            @include('frontend.partials.contacto')
                        </div>
                        <br>
                        <a href="{{ route('login') }}" class="btn btn-outline-success">Administraci&oacute;n</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</body>
</html>