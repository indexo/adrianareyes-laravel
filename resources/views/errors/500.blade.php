@extends('layouts.frontend')

@section('title', 'Error 500')

@section('content')
    <h1>Error interno. Intente nuevamente o contacte un administrador.</h1>
@endsection