<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <title>{{ config('app.name') }} | @yield('title')</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.css" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/leftmenu.css') }}">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    @stack('css_files')
    <style type="text/css">
        @stack('css')
    </style>
    @stack('js_top')
</head>
<body>

 <div class="app-body" id="v-app">
    @if(Auth::check())
    @include('admin.partials.leftmenu')
    @endif

    <div class="container"  id="app">
        <div id="panel-body">
            <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ action('WebController@index') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="{{ action('WebController@admin') }}">Administraci&oacute;n</a></li>
                    @yield('breadcrumb')
                </ol>
            </nav>
        </div>
     @yield('content')
        </div><!-- /Panel-Body -->
   </div><!-- /app container -->
 </div><!-- /app-body -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.js"></script>
<script src="{{ asset('public/js/app.js') }}"></script>
@stack('js_files')
<script type="text/javascript">
    @stack('js')
</script>
</body>
</html>

@if(session()->has('mensaje'))
    <?php $msj = session()->pull('mensaje'); ?>
    <script>
        swal(
            '{!! $msj['titulo'] !!}',
            '{!! $msj['mensaje'] !!}',
            '{!! $msj['tipo'] !!}'
        );
    </script>
@endif