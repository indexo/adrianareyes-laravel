<!-- Header -->
@include('frontend.partials.header')
<!-- Contenido -->
<div id="v-app">
    <div id="app">
        <div id="index-body">
            @yield('content')
        </div>
    </div><!-- /Index-Body -->
</div>
<!-- Footer -->
@include('frontend.partials.footer')

@if(session()->has('mensaje'))
    <?php $msj = session()->pull('mensaje'); ?>
    <script>
        swal(
            '{!! $msj['titulo'] !!}',
            '{!! $msj['mensaje'] !!}',
            '{!! $msj['tipo'] !!}'
        );
    </script>
@endif