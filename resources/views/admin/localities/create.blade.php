@extends('layouts.admin')

@section('title', 'Localidades - Nueva')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('LocationController@index') }}">Localidades</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nueva</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('LocationController@store') }}" class="form col-md">
		{{ csrf_field() }}
		@include('admin.localities.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-plus"></i> Crear Localidad</button>
	</form>
@endsection