<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="name">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" @if(old('name')) value="{{ old('name') }}" @elseif(isset($location) && $location->name) value="{{ $location->name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="department_id">Departamento</label>
		<select class="form-control" id="department_id" name="department_id">
			<option value="" @if(!old('department_id') || (isset($location) && $location->department_id)) selected @endif>Elija un departamento</option>
			@forelse(App\Department::all() as $d)
				<option  value="{{ $d->id }}" @if(old('department_id') == $d->id) selected @elseif(isset($location) && $location->department_id == $d->id) selected @endif>{!! $d->name !!}</option>
			@empty
				<option value="" disabled="" selected="">No hay departamentos disponibles</option>
			@endforelse
		</select>
	</fieldset>
</div>