@extends('layouts.admin')

@section('title', 'Localidades - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Localidades</li>
@endsection

@section('content')
<a href="{{ action('LocationController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Localidad</a>
<hr>
<table class="table table-bordered text-center" id="localities">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Departamento</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($localities as $l)
			<tr>
				<td>{!! $l->name !!}</td>
				<td>{!! $l->department->name !!}</td>
				<td>
					<form method="POST" action="{{ action('LocationController@destroy', $l) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('LocationController@edit', $l) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="4" class="text-center">No hay localidads</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#localities').DataTable({
		order: [[ 2, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush