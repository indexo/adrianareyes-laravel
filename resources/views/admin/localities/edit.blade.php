@extends('layouts.admin')

@section('title', 'Exterior - Editar: ' . $location->name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('LocationController@index') }}">Localidades</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('LocationController@edit', $location) }}">{!! $location->name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('LocationController@update', $location) }}" class="form col-md">
		{{ csrf_field() }}
		{{ method_field('put') }}
		@include('admin.localities.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Localidad</button>
	</form>
@endsection