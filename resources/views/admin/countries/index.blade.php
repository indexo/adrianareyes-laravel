@extends('layouts.admin')

@section('title', 'Países - Inicio')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Pa&iacute;ses</li>
@endsection

@section('content')
<a href="{{ action('CountryController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Pa&iacute;s</a>
<hr>
<table class="table table-bordered text-center" id="countries">
	<thead>
		<tr>
			<th>C&oacute;digo(ISO 3)</th>
			<th>Nombre</th>
			<th>C&oacute;digo Telef&oacutenico</th>
			<th>Moneda (S&iacute;mbolo)</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($countries as $c)
			<tr>
				<td>{{ strtoupper($c->iso3) }} </td>
				<td>{{ $c->name }}</td>
				<td>+{{ $c->phone_code }}</td>
				<td>{!! title_case($c->currency) !!} (<strong>{!! $c->currency_symbol !!}</strong>)</td>
				<td>
					<form method="POST" action="{{ action('CountryController@destroy', $c) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('CountryController@edit', $c) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="4" class="text-center">No hay pa&iacute;ses</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#countries').DataTable({
		order: [[ 2, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush