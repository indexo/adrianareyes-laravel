<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="name">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" @if(old('name')) value="{{ old('name') }}" @elseif(isset($country)) value="{{ $country->name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
    <fieldset class="form-group col-md-4">
        <label class="control-label" for="iso3">Nombre corto (<a href="http://www.fao.org/countryprofiles/iso3list/es/" target="_blank">ISO 3</a>)</label>
        <input
                type="text"
                class="form-control"
                id="iso3"
                name="iso3"
                @if(old('iso3'))
                value="{{ old('iso3') }}"
                @elseif(isset($country) && $country->iso3)
                value="{{ $country->iso3 }}"
                @endif>
    </fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="phone_code">C&oacute;digo Telef&oacute;nico</label>
		<input type="number" class="form-control" id="phone_code" name="phone_code" @if(old('phone_code')) value="{{ old('phone_code') }}" @elseif(isset($country)) value="{{ $country->phone_code }}"@endif></input>
	</fieldset>
</div>
<div class="row">
    <fieldset class="form-group col-md-4">
        <label class="control-label" for="currency">Moneda <small>(Ej.: <i>pesos uruguayos</i>)</small></label>
        <input type="text" class="form-control" id="currency" name="currency" @if(old('currency')) value="{{ old('currency') }}" @elseif(isset($country)) value="{{ $country->currency }}"@endif></input>
    </fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
        <label class="control-label" for="currency_symbol">Moneda <small>(S&iacute;mbolo)</small></label>
		<input type="text" class="form-control" id="currency_symbol" name="currency_symbol" @if(old('currency_symbol')) value="{{ old('currency_symbol') }}" @elseif(isset($country)) value="{{ $country->currency_symbol }}"@endif></input>
	</fieldset>
</div>
<div class="row">
    <fieldset class="form-group col-md-4">
        <label for="currency_exchange" class="controla-lebel">Cambio de moneda <small>(en base d&oacute;lares, <a target="_blank" href="http://xe.com/es/currencyconverter/convert/?Amount=1&From=USD&To=UYU">referencia</a>)</small></label>
        <input
                type="text"
                class="form-control"
                id="currency_exchange"
                name="currency_exchange"
                @if(old('currency_exchange'))
                value="{{ old('currency_exchange') }}"
                @elseif(isset($country) && $country->currency_exchange)
                value="{{ $country->currency_exchange }}"
                @endif>
    </fieldset>
</div>