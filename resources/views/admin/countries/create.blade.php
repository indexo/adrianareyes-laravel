@extends('layouts.admin')

@section('title', 'Países - Nuevo')

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page"><a href="{{ action('CountryController@index') }}">Pa&iacute;ses</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('CountryController@store') }}" class="form col-md">
		{{ csrf_field() }}
		@include('admin.countries.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-plus"></i> Crear Pa&iacute;s</button>
	</form>
@endsection