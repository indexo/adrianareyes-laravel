@extends('layouts.admin')

@section('title', 'Países - Editar: '. $country->name)

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page"><a href="{{ action('CountryController@index') }}">Pa&iacute;ses</a></li>
    <li class="breadcrumb-item" aria-current="page"><a href="{{ action('CountryController@edit', $country) }}">{!! $country->name !!}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('CountryController@update', $country) }}" class="form col-md">
		{{ csrf_field() }}
		{{ method_field('put') }}
		@include('admin.countries.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Pa&iacute;s</button>
	</form>
@endsection