@extends('layouts.admin')

@section('title', 'Calendario')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Calendario</li>
@endsection

@section('content')
<div class="row">
	<div class="col-md-4">
		<form class="form" method="POST" @if(isset($event)) action="{{ action('CalendarController@update', $event) }}"@else action="{{ action('CalendarController@store') }}"@endif>
			{{csrf_field()}}
			@if(isset($event))
				{{ method_field('put')}}
			@endif
			<div class="row">
				<fieldset class="form-group col-md">
					<label class="control-label" for="title">T&iacute;tulo</label>
					<input class="form-control" id="title" name="title" @if(isset($event)) value="{{ $event->title }}"@elseif(old('title')) value="{{ old('title') }}"@endif>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md-6">
					<label class="control-label" for="start_date">Fecha de Inicio</label>
					<input type="date" class="form-control" id="start_date" name="start_date" @if(isset($event)) value="{{ $event->start_date }}"@elseif(old('start_date')) value="{{ old('start_date') }}"@else v-model="startDate"@endif>
				</fieldset>
				<fieldset class="form-group col-md-6">
					<label class="control-label" for="start_time">Hora</label>
					<input type="time" class="form-control" id="start_time" name="start_time" @if(isset($event)) value="{{ $event->start_time }}"@elseif(old('start_time')) value="{{ old('start_time') }}"@else v-model="startTime"@endif>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group">
					<label class="control-label" for="all_day">Todo el d&iacute;a</label>
					<input type="checkbox" class="form-control" id="all_day" name="all_day" @if(isset($event) && !$event->end_date) checked @elseif(old('all_day')) checked @else v-model="allDay"@endif>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md-6">
					<label class="control-label" for="start_date">Fecha Final</label>
					<input type="date" class="form-control" id="end_date" name="end_date" :disabled="allDay" @if(isset($event)) value="{{ $event->end_date }}"@elseif(old('end_date')) value="{{ old('end_date') }}"@else :value="startDate" @endif>
				</fieldset>
				<fieldset class="form-group col-md-6">
					<label class="control-label" for="start_time">Hora</label>
					<input type="time" class="form-control" id="end_time" name="end_time" :disabled="allDay" @if(isset($event)) value="{{ $event->end_time }}"@elseif(old('end_time')) value="{{ old('end_time') }}"@else :value="startTime" @endif>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md">
					<label class="control-label" for="description">Descripci&oacute;n</label>
					<textarea class="form-control" id="description" name="description">@if(isset($event)){{ $event->description }}@elseif(old('description')){{ old('description') }}@endif</textarea>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md">
					<button class="btn btn-success">@if(isset($event)) <i class="fa fa-floppy-o"></i> Actualizar evento @else <i class="fa fa-plus-circle"></i> Crear evento @endif</button>
				</fieldset>
			</div>
		</form>
	</div>
	<div class="col-md-8">
		<div class="agenda">
		    <div class="table-responsive">
		        <table class="table table-condensed table-bordered table-striped text-center" id="events">
		            <thead>
		                <tr>
		                	<th>Inicio</th>
		                    <th>Fin</th>
		                    <th>T&iacute;tulo</th>
		                    <th>Acciones</th>
		                </tr>
		            </thead>
		            <tbody>
		            	@forelse($events as $e)
		            		<tr>
		            			<td>{{ $e->start_date . ' ' . $e->start_time }}</td>
		            			<td>@if($e->end_date && $e->end_time){{ $e->end_date . ' ' . $e->end_time }}@else N/A @endif</td>
		            			<td><a href="{{ action('CalendarController@edit', $e) }}">{{ $e->title }}</a></td>
		            			<td>
		            				<form method="POST" action="{{ action('CalendarController@destroy', $e) }}">
		            					{{ csrf_field() }}
		            					{{ method_field('delete') }}
		            					<button class="btn btn-danger"><i class="fa fa-trash"></i></button>
		            				</form>
		            			</td>
		            		</tr>
		            	@empty
		            		<td colspan="4" class="text-center">No hay eventos disponibles</td>
		            	@endforelse
		            </tbody>
		        </table>
		    </div>
		</div>
	</div>
</div>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#events').DataTable({
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
	
	if($('#all_day').attr('checked') === 'checked') {
		$('#end_date').attr('disabled', 'disabled');
		$('#end_time').attr('disabled', 'disabled');
	}
	$('#all_day').change(function(e) {
		if(this.checked) {
			$('#end_date').attr('disabled', 'disabled');
			$('#end_time').attr('disabled', 'disabled');
		} else {
			$('#end_date').removeAttr('disabled');
			$('#end_time').removeAttr('disabled');
		}
	});
@endpush