@extends('layouts.admin')

@section('title', 'Mensajes - Inicio')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Mensajes</li>
@endsection

<?php \Carbon\Carbon::setLocale('es'); ?>

@section('content')
    <div class="row">
        <div class="card" style="width:100%;">
            <ul class="list-group list-group-flush">
                @forelse($messages as $message)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            <p>De:<br>&nbsp;
                                @if(!$message->replies()->count())
                                    <strong>{!! $message->first_name . ' ' . $message->last_name !!}</strong>
                                @else
                                    {!! $message->first_name . ' ' . $message->last_name !!}
                                @endif
                                @if($message->ip != '127.0.0.1')
                                (<a href="http://www.ip-tracker.org/locator/ip-lookup.php?ip={{ $message->ip }}" target="_blank">{{ $message->ip }}</a>)
                                @else
                                ({{ $message->ip }})
                                @endif
                            </p>
                        </div>
                        <div class="col">
                            <p>Fecha:<br>&nbsp;
                                @if(!$message->replies()->count())
                                    <strong>{!! $message->created_at . ' (' . $message->created_at->diffForHumans() . ')' !!}</strong>
                                @else
                                    {!! $message->created_at . ' (' . $message->created_at->diffForHumans() . ')' !!}
                                @endif
                            </p>
                        </div>
                        @if($message->property)
                            <div class="col">
                                <p>Propiedad de referencia:<br>
                                    @if(!$message->replies()->count())
                                        <strong>
                                            <a href="{{ action('WebController@showProperty', $message->property->reference()) }}" target="_blank">#{{ $message->property->reference() }}</a>
                                        </strong>
                                    @else
                                        <a href="{{ action('WebController@showProperty', $message->property->reference()) }}" target="_blank">#{{ $message->property->reference() }}</a>
                                    @endif
                                </p>
                            </div>
                        @endif
                        <div class="col">
                            <form method="POST" action="{{ action('MessageController@destroy', $message) }}" class="text-right">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="btn-group" role="group" aria-label="Acciones">
                                    <a href="{{ action('MessageController@show', $message) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                @empty
                    <li class="list-group-item text-center">
                        <i>No hay mensajes</i>
                    </li>
                @endforelse
            </ul>
        </div>
    </div>
@endsection