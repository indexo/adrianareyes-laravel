@extends('layouts.admin')

@section('title', 'Mensaje: #' . $message->id)

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page"><a href="{{ action('MessageController@index') }}">Mensajes</a></li>
    <li class="breadcrumb-item active" aria-current="page">#{{ $message->id }}</li>
@endsection

<?php \Carbon\Carbon::setLocale('es'); ?>

@section('content')
    @if($message->property)
    <div class="row">
        <div class="col">
            <strong>Propiedad:</strong> <a href="{{ action('WebController@showProperty', $message->property->reference()) }}" target="_blank">#{{ $message->property->reference() }}</a>
        </div>
    </div>
    <hr>
    @endif
    <div class="row">
        <div class="card" style="width:100%;">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <p>
                        <i class="fa fa-user"></i> <a href="mailto:{{ $message->email }}">{!! $message->first_name . ' ' . $message->last_name !!}</a>
                    </p>
                    {!! $message->message  !!}
                    <form method="POST" action="{{ action('MessageController@destroy', $message) }}" class="text-right">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <small>
                            {{ $message->created_at->diffForHumans() }}
                            &nbsp;
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </small>
                    </form>
                </li>
                @foreach($message->replies() as $reply)
                    <li class="list-group-item text-right">
                        <p>
                            @if($reply->user->avatar)
                                <img src="{{ action('UserController@getAvatar', $reply->user) }}" class="img img-thumbnail" alt="{{ $reply->user->name }}">
                            @else
                                <i class="fa fa-user"></i>
                            @endif
                            &nbsp;<i><strong>{!! $reply->user->name !!}</strong></i>
                        </p>
                        {!! $reply->message  !!}
                        <form method="POST" action="{{ action('MessageController@destroy', $reply) }}" class="text-right">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <small>
                                {{ $reply->created_at->diffForHumans() }}
                                &nbsp;
                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            </small>
                        </form>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr>
    <div class="row">
        <form action="{{ action('MessageController@reply', $message) }}" method="POST" class="form col-12">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <textarea name="message" id="message" class="form-control"></textarea>
            </fieldset>
            <fieldset class="form-group text-right">
                <button class="btn btn-success" type="submit"><i class="fa fa-envelope"></i> Enviar respuesta</button>
            </fieldset>
        </form>
    </div>
@endsection

@push('js_files')
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
@endpush

@push('js')
    CKEDITOR.replace( 'message' );
@endpush
