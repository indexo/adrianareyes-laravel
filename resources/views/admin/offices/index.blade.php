@extends('layouts.admin')

@section('title', 'Oficinas - Inicio')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Oficinas</li>
@endsection

@section('content')
<p><a href="{{ action('OfficeController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Oficina</a></p>
<div class="row">
	<div class="col">
		<table class="table table-bordered table-striped text-center" id="offices">
			<thead>
				<tr>
					<th>Direcci&oacute;n</th>
					<th>Tel&eacute;fonos</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@forelse($offices as $o)
					<tr>
						<td>{{ $o->address }}</td>
						<td>{!! $o->phonify() !!}</td>
						<td>
							<form method="POST" action="{{ action('OfficeController@destroy', $o) }}">
								{{ csrf_field() }}
								{{ method_field('delete') }}
								<div class="btn-group" role="group" aria-label="Acciones">
									<a href="{{ action('OfficeController@edit', $o) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
								</div>
							</form>
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="3" class="text-center">No hay oficinas</td>
					</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#offices').DataTable({
		order: [[ 3, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush