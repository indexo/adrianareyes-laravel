@extends('layouts.admin')

@section('title', 'Oficinas - Editar: ' . $office->address)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('OfficeController@index') }}">Oficinas</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('OfficeController@edit', $office) }}">{!! $office->address !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('OfficeController@update', $office) }}" enctype="multipart/form-data" class="form col-md-6">
		{{ csrf_field()}}
		{{ method_field('put') }}
		@include('admin.offices.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Oficina</button>
	</form>
@endsection