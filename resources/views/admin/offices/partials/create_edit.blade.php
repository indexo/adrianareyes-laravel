<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="address">Direcci&oacute;n</label>
		<input type="text" class="form-control" name="address" id="address" @if(old('address')) value="{{ old('address') }}"@elseif(isset($office) && $office->address) value="{!! $office->address !!}"@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="city">Ciudad</label>
		<input type="text" class="form-control" name="city" id="city" @if(old('city')) value="{{ old('city') }}"@elseif(isset($office) && $office->city) value="{!! $office->city !!}"@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="country">Pa&iacute;s</label>
		<input type="text" class="form-control" name="country" id="country" @if(old('country')) value="{{ old('country') }}"@elseif(isset($office) && $office->country) value="{!! $office->country !!}"@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="phones">Tel&eacute;fonos <small>(separados por coma)</small></label>
		<input type="text" class="form-control" name="phones" id="phones" @if(old('phones')) value="{{ old('phones') }}"@elseif(isset($office) && $office->phones) value="{!! $office->phones !!}"@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="emails">Correos electr&oacute;nicos <small>(separados por coma)</small></label>
		<input type="text" class="form-control" name="emails" id="emails" @if(old('emails')) value="{{ old('emails') }}"@elseif(isset($office) && $office->emails) value="{!! $office->emails !!}"@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="google_map_code">C&oacute;digo de Google Maps</label>
		<textarea class="form-control" name="google_map_code" id="google_map_code" style="height:450px;">@if(old('google_map_code')){{ old('google_map_code') }}@elseif(isset($office) && $office->google_map_code){{ $office->google_map_code }}@endif</textarea>
	</fieldset>
</div>
<h3 class="text-center">Horarios <small>(para Google)</small></h3>

<h4>Lunes</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_mon">Abre</label>
		<input type="time" class="form-control" name="open_mon" id="open_mon" @if(old('open_mon')) value="{{ old('open_mon') }}"@elseif(isset($office) && $office->open_mon) value="{!! $office->open_mon !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_mon">Cierra</label>
		<input type="time" class="form-control" name="close_mon" id="close_mon" @if(old('close_mon')) value="{{ old('close_mon') }}"@elseif(isset($office) && $office->close_mon) value="{!! $office->close_mon !!}"@endif>
	</fieldset>
</div>

<h4>Martes</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_tue">Abre</label>
		<input type="time" class="form-control" name="open_tue" id="open_tue" @if(old('open_tue')) value="{{ old('open_tue') }}"@elseif(isset($office) && $office->open_tue) value="{!! $office->open_tue !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_tue">Cierra</label>
		<input type="time" class="form-control" name="close_tue" id="close_tue" @if(old('close_tue')) value="{{ old('close_tue') }}"@elseif(isset($office) && $office->close_tue) value="{!! $office->close_tue !!}"@endif>
	</fieldset>
</div>

<h4>Mi&eacute;rcoles</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_wed">Abre</label>
		<input type="time" class="form-control" name="open_wed" id="open_wed" @if(old('open_wed')) value="{{ old('open_wed') }}"@elseif(isset($office) && $office->open_wed) value="{!! $office->open_wed !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_wed">Cierra</label>
		<input type="time" class="form-control" name="close_wed" id="close_wed" @if(old('close_wed')) value="{{ old('close_wed') }}"@elseif(isset($office) && $office->close_wed) value="{!! $office->close_wed !!}"@endif>
	</fieldset>
</div>

<h4>Jueves</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_thu">Abre</label>
		<input type="time" class="form-control" name="open_thu" id="open_thu" @if(old('open_thu')) value="{{ old('open_thu') }}"@elseif(isset($office) && $office->open_thu) value="{!! $office->open_thu !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_thu">Cierra</label>
		<input type="time" class="form-control" name="close_thu" id="close_thu" @if(old('close_thu')) value="{{ old('close_thu') }}"@elseif(isset($office) && $office->close_thu) value="{!! $office->close_thu !!}"@endif>
	</fieldset>
</div>

<h4>Viernes</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_fri">Abre</label>
		<input type="time" class="form-control" name="open_fri" id="open_fri" @if(old('open_fri')) value="{{ old('open_fri') }}"@elseif(isset($office) && $office->open_fri) value="{!! $office->open_fri !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_fri">Cierra</label>
		<input type="time" class="form-control" name="close_fri" id="close_fri" @if(old('close_fri')) value="{{ old('close_fri') }}"@elseif(isset($office) && $office->close_fri) value="{!! $office->close_fri !!}"@endif>
	</fieldset>
</div>

<h4>S&aacute;bado</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_sat">Abre</label>
		<input type="time" class="form-control" name="open_sat" id="open_sat" @if(old('open_sat')) value="{{ old('open_sat') }}"@elseif(isset($office) && $office->open_sat) value="{!! $office->open_sat !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_sat">Cierra</label>
		<input type="time" class="form-control" name="close_sat" id="close_sat" @if(old('close_sat')) value="{{ old('close_sat') }}"@elseif(isset($office) && $office->close_sat) value="{!! $office->close_sat !!}"@endif>
	</fieldset>
</div>

<h4>Domingo</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="open_sun">Abre</label>
		<input type="time" class="form-control" name="open_sun" id="open_sun" @if(old('open_sun')) value="{{ old('open_sun') }}"@elseif(isset($office) && $office->open_sun) value="{!! $office->open_sun !!}"@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label class="control-label" for="close_sun">Cierra</label>
		<input type="time" class="form-control" name="close_sun" id="close_sun" @if(old('close_sun')) value="{{ old('close_sun') }}"@elseif(isset($office) && $office->close_sun) value="{!! $office->close_sun !!}"@endif>
	</fieldset>
</div>

<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="schedules">Horarios</label>
		<textarea class="form-control" name="schedules" id="schedules">@if(old('schedules')){{ old('schedules') }}@elseif(isset($office) && $office->schedules){!! $office->schedules !!}@endif</textarea>
	</fieldset>
</div>

<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label" for="is_main">Oficina Principal <input class="form-control" type="checkbox" id="is_main" name="is_main" @if(old('is_main') || (isset($office) && $office->main())) checked @endif></label>
		
	</fieldset>
</div>

@push('js_files')
<script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
@endpush

@push('js')
	ClassicEditor
		.create( document.querySelector( '#schedules' ) )
		.then( editor => {
			console.log( editor );
		} )
		.catch( error => {
			console.error( error );
		} );
@endpush