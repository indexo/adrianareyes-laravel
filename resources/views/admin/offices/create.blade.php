@extends('layouts.admin')

@section('title', 'Oficinas - Nueva')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('OfficeController@index') }}">Oficinas</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nueva</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('OfficeController@store') }}" enctype="multipart/form-data" class="form col-md-6">
		{{ csrf_field()}}
		@include('admin.offices.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Crear Oficina</button>
	</form>
@endsection