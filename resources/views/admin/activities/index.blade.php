@extends('layouts.admin')

@section('title', 'Actividades')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">Actividades</li>
@endsection

@section('content')
<div class="row">
    <table class="table table-bordered table-striped text-center"
           :id="activities"
           width="100%">
        <thead>
            <tr>
                <th><a href="#" @click.prevent="orderActivity('user')">Administrador</a></th>
                <th><a href="#" @click.prevent="orderActivity('ip')">Direcci&oacute;n IP</a></th>
                <th><a href="#" @click.prevent="orderActivity('country')">Pa&iacute;s</a></th>
                <th><a href="#" @click.prevent="orderActivity('referer')">Referido</a></th>
                <th><a href="#" @click.prevent="orderActivity('action')">Acci&oacute;n</a></th>
                <th><a href="#" @click.prevent="orderActivity('date')">Fecha</a></th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in activities.data">
                <td v-if="row.user">
                    <img v-show="row.user.avatar"
                         :src="'/administracion/usuarios/avatar/' + row.user.id"
                         :alt="'Imagen de ' + row.user.name"
                         class="rounded"
                         width="50px">
                    <a :href="'/administracion/usuarios/' + row.user.id"
                       target="_blank"
                       v-html="row.user.name"
                    ></a>
                </td>
                <td v-else>N/A</td>

                <td>
                    <a v-if="row.remote_addr !== '127.0.0.1'"
                       :href="'http://www.ip-tracker.org/locator/ip-lookup.php?ip=' + row.remote_addr"
                       v-html="row.remote_addr"
                       target="_blank"></a>
                    <p v-else>Local</p>
                </td>

                <td>
                    <p v-if="row.country"
                       v-html="row.country"></p>
                    <p v-else>N/A</p>
                </td>

                <td>
                    <a v-if="row.referer"
                       :href="row.referer"
                       target="_blank"
                       v-html="row.referer"></a>
                    <p v-else>N/A</p>
                </td>

                <td>
                    <a v-if="row.request_uri"
                       :href="row.request_uri"
                       target="_blank"
                       v-html="row.request_uri"></a>
                    <p v-else>N/A</p>
                </td>

                <td v-html="row.timeago"></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>Administrador</th>
                <th>Direcci&oacute;n IP</th>
                <th>Pa&iacute;s</th>
                <th>Referido</th>
                <th>Acci&oacute;n</th>
                <th>Fecha</th>
            </tr>
        </tfoot>
    </table>

    <nav>
        <ul class="pagination justify-content-center" v-show="pagination.total > 0" v-cloak>
            <li class="page-item"
                v-bind:class="{ 'disabled': pagination.currentPage === pagination.from}">
                <a class="page-link"
                   href="#"
                   @click.prevent="loadActivities(pagination.currentPage-1)"
                >Anterior</a>
            </li>
            <li class="page-item"
                v-if="page >= pagination.currentPage"
                v-for="page in pagination.currentPage+5"
                v-bind:class="{ 'active': page === pagination.currentPage}">
                <a class="page-link"
                   href="#"
                   @click.prevent="loadActivities(page)"
                   v-html="page"
                ></a>
            </li>
            <li class="page-item"
                v-bind:class="{ 'disabled': pagination.currentPage === pagination.lastPage}">
                <a class="page-link"
                   href="#"
                   @click.prevent="loadActivities(pagination.currentPage+1)"
                >Siguiente</a>
            </li>
        </ul>
    </nav>
</div>
@endsection