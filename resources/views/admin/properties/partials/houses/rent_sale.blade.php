<h3>Informaci&oacute;n de Inmueble</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="ground_footage"
			   class="control-label"
		>Metraje Terreno</label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="ground_footage"
			   id="ground_footage"
			   @if(old('ground_footage')) value="{{ old('ground_footage') }}"
			   @elseif(isset($property)) value="{{ $property->ground_footage }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="built_footage"
			   class="control-label"
        >Metraje Constru&iacute;do</label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="built_footage"
			   id="built_footage"
			   @if(old('built_footage')) value="{{ old('built_footage') }}"
			   @elseif(isset($property)) value="{{ $property->built_footage }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="distance_from_sea"
			   class="control-label"
        >Distancia del mar <small>(m)</small></label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="distance_from_sea"
			   id="distance_from_sea"
			   @if(old('distance_from_sea')) value="{{ old('distance_from_sea') }}"
			   @elseif(isset($property)) value="{{ $property->distance_from_sea }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="distance_from_beach"
			   class="control-label"
        >Distancia de la playa</label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="distance_from_beach"
			   id="distance_from_beach"
			   @if(old('distance_from_beach')) value="{{ old('distance_from_beach') }}"
			   @elseif(isset($property)) value="{{ $property->distance_from_beach }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="price"
			   class="control-label"
        >Precio</label>
		<currency class="form-control"
				  @if(old('price')) data="{{ old('price') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->price }}"
				  @endif
				  name="price"/>
	</fieldset>
	<fieldset class="form-check form-check-inline col-md">
		<input class="form-check-input"
               type="checkbox"
               id="financing"
               name="financing"
               @if(old('financing') || (isset($property->_tdo) && $property->_tdo->financing)) checked @endif>
        <label for="financing"
               class="form-check-label"
        >Financiaci&oacute;n</label>
	</fieldset>
</div><!-- /Información de inmueble -->
<hr>

<h3>Informaci&oacute;n del Alquiler</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label class="control-label"
			   for="january"
        >Enero</label>
		<currency class="form-control"
				  @if(old('january')) data="{{ old('january') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->january }}"
				  @endif
				  name="january"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="january_1st_fortnight"
			   class="control-label"
        >Primera quincena</label>
		<currency class="form-control"
				  @if(old('january_1st_fortnight')) data="{{ old('january_1st_fortnight') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->january_1st_fortnight }}"
				  @endif
				  name="january_1st_fortnight"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="january_2nd_fortnight"
			   class="control-label"
        >Segunda quincena</label>
		<currency class="form-control"
				  @if(old('january_2nd_fortnight')) data="{{ old('january_2nd_fortnight') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->january_2nd_fortnight }}"
				  @endif
				  name="january_2nd_fortnight"/>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="february"
			   class="control-label"
        >Febrero</label>
		<currency class="form-control"
				  @if(old('february')) data="{{ old('february') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->february }}"
				  @endif
				  name="february"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="february_1st_fortnight"
			   class="control-label"
        >Primera quincena</label>
		<currency class="form-control"
				  @if(old('february_1st_fortnight')) data="{{ old('february_1st_fortnight') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->february_1st_fortnight }}"
				  @endif
				  name="february_1st_fortnight"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="february_2nd_fortnight"
			   class="control-label"
        >Segunda quincena</label>
		<currency class="form-control"
				  @if(old('february_2nd_fortnight')) data="{{ old('february_2nd_fortnight') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->february_2nd_fortnight }}"
				  @endif
				  name="february_2nd_fortnight"/>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="season"
			   class="control-label"
        >Temporada</label>
		<currency class="form-control"
				  @if(old('season')) data="{{ old('season') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->season }}"
				  @endif
				  name="season"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="easter"
			   class="control-label"
        >Semana Santa</label>
		<currency class="form-control"
				  @if(old('easter')) data="{{ old('easter') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->easter }}"
				  @endif
				  name="easter"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="annual"
			   class="control-label"
        >Anual</label>
		<currency class="form-control"
				  @if(old('annual')) data="{{ old('annual') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->annual }}"
				  @endif
				  name="annual"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="winter"
			   class="control-label"
        >Invierno</label>
		<currency class="form-control"
				  @if(old('winter')) data="{{ old('winter') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->winter }}"
				  @endif
				  name="winter"/>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="high_season_daily"
			   class="control-label"
        >Temporada Alta (diario)</label>
		<currency class="form-control"
				  @if(old('high_season_daily')) data="{{ old('high_season_daily') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->high_season_daily }}"
				  @endif
				  name="high_season_daily"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="high_season_daily_min_days"
			   class="control-label"
        >Temporada Alta mínimo de días</label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="high_season_daily_min_days"
			   id="high_season_daily_min_days"
			   @if(old('high_season_daily_min_days')) value="{{ old('high_season_daily_min_days') }}"
			   @elseif(isset($property->_tdo)) value="{{ $property->_tdo->high_season_daily_min_days }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="low_season_daily"
			   class="control-label"
        >Temporada Baja (diario)</label>
        <currency class="form-control"
                  @if(old('low_season_daily')) data="{{ old('low_season_daily') }}"
                  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->low_season_daily }}"
                  @endif
                  name="low_season_daily"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="low_season_daily_min_days"
			   class="control-label"
        >Temporada Baja mínimo de días</label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="low_season_daily_min_days"
			   id="low_season_daily_min_days"
			   @if(old('low_season_daily_min_days')) value="{{ old('low_season_daily_min_days') }}"
			   @elseif(isset($property->_tdo)) value="{{ $property->_tdo->low_season_daily_min_days }}"
				@endif>
	</fieldset>
</div><!-- /Información de alquiler -->
<hr>

<h3>Comodidades</h3>
<div class="row">
    @forelse(App\Environment::all() as $env)
        <environment
                :id="{{ $env->id }}"
                label="{!! $env->name !!}"
                type="{{ $env->type }}"
                @if(old('environments.'.$env->id))
                value="{{ old('environments.'.$env->id) }}"
                @elseif(isset($property) && $property->environment($env->id))
                value="{{ $property->environment($env->id)->value }}"
                @endif
        ></environment>
    @empty
        <p>
            No hay ambientes disponibles.
            <a target="_blank"
               href="{{ action('EnvironmentController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Comodidades -->
<hr>

<h3>Exterior</h3>
<div class="row">
    @forelse(App\Exterior::all() as $ext)
        <exterior
                :id="{{ $ext->id }}"
                label="{!! $ext->name !!}"
                type="{{ $ext->type }}"
                @if(old('exterior.'.$ext->id))
                value="{{ old('exterior.'.$ext->id) }}"
                @elseif(isset($property) && $property->exterior($ext->id))
                value="{{ $property->exterior($ext->id)->value }}"
                @endif
        ></exterior>
    @empty
        <p>
            No hay exteriores disponibles.
            <a target="_blank"
               href="{{ action('ExteriorController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Exterior -->
<hr>

<!-- Apartamento de Invitados -->
<h3>
	<guest-room
			@callback="toggleGuestRoom"
			@if(old('guest_room') || (isset($property->tdi) && $property->tdi->guest_room))
			:value="true"
			@endif
	></guest-room>
</h3>
<div v-show="guestRoom">
	<div class="row">
		<fieldset class="form-group col-md">
			<label
					for="guest_room_rooms"
					class="control-label">Dormitorios</label>
			<input
					class="form-control"
					type="number"
					min="0"
					name="guest_room_rooms"
					id="guest_room_rooms"
					@if(old('guest_room_rooms'))
					value="{{ old('guest_room_rooms') }}"
					@elseif(isset($property->tdi))
					value="{{ $property->tdi->guest_room_rooms }}"
					@endif>
		</fieldset>

		<fieldset class="form-group col-md">
			<label
					for="guest_room_bathrooms"
					class="control-label">Ba&ntilde;os</label>
			<input
					class="form-control"
					type="number"
					min="0"
					name="guest_room_bathrooms"
					id="guest_room_bathrooms"
					@if(old('guest_room_bathrooms'))
					value="{{ old('guest_room_bathrooms') }}"
					@elseif(isset($property->tdi))
					value="{{ $property->tdi->guest_room_bathrooms }}"
					@endif>
		</fieldset>
		<fieldset class="form-check form-check-inline col-md">
			<input
					class="form-check-input"
					type="checkbox"
					name="guest_room_kitchen"
					id="guest_room_kitchen"
					@if(old('guest_room_kitchen') || (isset($property->tdi) && $property->tdi->guest_room_kitchen))
					checked
					@endif>
			<label
					for="guest_room_kitchen"
					class="form-check-label"
			>Cocina</label>
		</fieldset>
		<fieldset class="form-check form-check-inline col-md">
			<input
					class="form-check-input"
					type="checkbox"
					name="guest_room_kitchenette"
					id="guest_room_kitchenette"
					@if(old('guest_room_kitchenette') || (isset($property->tdi) && $property->tdi->guest_room_kitchenette))
					checked
					@endif>
			<label
					for="guest_room_kitchenette"
					class="form-check-label">Kitchenette</label>
		</fieldset>
		<fieldset class="form-check form-check-inline col-md">
			<input
					class="form-check-input"
					type="checkbox"
					name="guest_room_living_room"
					id="guest_room_living_room"
					@if(old('guest_room_living_room') || (isset($property->tdi) && $property->tdi->guest_room_living_room))
					checked
					@endif>
			<label
					for="guest_room_living_room"
					class="form-check-label"
			>Living comedor</label>

		</fieldset>
	</div>
</div><!-- /Apartamentos de Invitados -->
<hr>

<h3>Equipamiento</h3>
<div class="row">
    @forelse(App\Equipment::all() as $eq)
        <equipment
                :id="{{ $eq->id }}"
                label="{!! $eq->name !!}"
                type="{{ $eq->type }}"
                @if(old('equipment.'.$eq->id))
                value="{{ old('equipment.'.$eq->id) }}"
                @elseif(isset($property) && $property->equipment($eq->id))
                value="{{ $property->equipment($eq->id)->value }}"
                @endif
        ></equipment>
    @empty
        <p>
            No hay equipamientos disponibles.
            <a target="_blank"
               href="{{ action('EquipmentController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- Equipamiento -->
<hr>

<h3>{!! __('properties.services_amenities_title') !!}</h3>
<div class="row">
    @forelse(App\ServiceAmenity::all() as $sa)
        <service-amenity
                :id="{{ $sa->id }}"
                label="{!! $sa->name !!}"
                type="{{ $sa->type }}"
                @if(old('services_amenities.'.$sa->id))
                value="{{ old('services_amenities.'.$sa->id) }}"
                @elseif(isset($property) && $property->service_amenity($sa->id))
                value="{{ $property->service_amenity($sa->id)->value }}"
                @endif
        ></service-amenity>
    @empty
        <p>
            No hay servicios o amenities disponibles.
            <a target="_blank"
               href="{{ action('ServiceAmenityController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Servicios y Amenities -->
<hr>

<h3>Gastos</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="expenses_property_tax"
			   class="control-label"
        >Contribuci&oacute;n inmobiliaria</label>
        <currency class="form-control"
                  @if(old('expenses_property_tax')) data="{{ old('expenses_property_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_property_tax }}"
                  @endif
                  name="expenses_property_tax"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="expenses_school_tax"
			   class="control-label"
        >Impuesto de Primaria</label>
        <currency class="form-control"
                  @if(old('expenses_school_tax')) data="{{ old('expenses_school_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_school_tax }}"
                  @endif
                  name="expenses_school_tax"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="expenses_gardener"
			   class="control-label"
        >Jardinero</label>
        <currency class="form-control"
                  @if(old('expenses_gardener')) data="{{ old('expenses_gardener') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_gardener }}"
                  @endif
                  name="expenses_gardener"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="expenses_pool_maintenance"
			   class="control-label"
        >Mantenimiento de piscina</label>
        <currency class="form-control"
                  @if(old('expenses_pool_maintenance')) data="{{ old('expenses_pool_maintenance') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_pool_maintenance }}"
                  @endif
                  name="expenses_pool_maintenance"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="expenses_alarm_vigilance"
			   class="control-label"
        >Alarma y vigilancia</label>
        <currency class="form-control"
                  @if(old('expenses_alarm_vigilance')) data="{{ old('expenses_alarm_vigilance') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_alarm_vigilance }}"
                  @endif
                  name="expenses_alarm_vigilance"/>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label for="description"
			   class="control-label"
        >Descripci&oacute;n</label>
		<textarea class="form-control"
				  name="description"
				  id="description"
        >@if(old('description')){{ old('description') }}@elseif(isset($property)){{ $property->description }}@endif</textarea>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="notes"
			   class="control-label"
        >Observaciones</label>
		<textarea class="form-control"
				  name="notes"
				  id="notes"
        >@if(old('notes')){{ old('notes') }}@elseif(isset($property)){{ $property->notes }}@endif</textarea>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="images"
			   class="control-label"
        >Im&aacute;genes</label>
		<input class="form-control"
			   type="file"
			   name="images[]"
			   id="images"
			   multiple>
	</fieldset>
</div>

