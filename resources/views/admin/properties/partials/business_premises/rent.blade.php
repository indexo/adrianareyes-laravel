<h3>Informaci&oacute;n de Inmueble</h3>

<h4>Precio alquiler</h4>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="annual"
			   class="control-label"
		>Anual</label>
		<currency class="form-control"
				  @if(old('annual')) data="{{ old('annual') }}"
				  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->annual }}"
				  @endif
				  name="annual"/>
    </fieldset>
    <fieldset class="form-group col-md">
		<label for="temporary"
			   class="control-label"
        >Temporal</label>
        <currency class="form-control"
                  @if(old('temporary')) data="{{ old('temporary') }}"
                  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->temporary }}"
                  @endif
                  name="temporary"/>
	</fieldset>
</div><!-- /Precio Alquiler -->
<hr>

<!-- Especificaciones -->
<div class="row">
	<fieldset class="form-group col-md">
		<label for="area"
			   class="control-label"
        >Superficie</label>
		<input class="form-control"
			   type="number"
			   name="area"
			   id="area"
			   @if(old('area')) value="{{ old('area') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->area }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="floors"
			   class="control-label"
        >Plantas</label>
		<input class="form-control"
			   type="number"
			   name="floors"
			   id="floors"
			   @if(old('floors')) value="{{ old('floors') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->floors }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="subsoil"
			   class="control-label"
        >Subsuelo</label>
		<input class="form-control"
			   type="number"
			   name="subsoil"
			   id="subsoil"
			   @if(old('subsoil')) value="{{ old('subsoil') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->subsoil }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="low_level"
			   class="control-label"
        >P.B.</label>
		<input class="form-control"
			   type="number"
			   name="low_level"
			   id="low_level"
			   @if(old('low_level')) value="{{ old('low_level') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->low_level }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="mezzanine"
			   class="control-label"
        >Entrepiso</label>
		<input class="form-control"
			   type="number"
			   name="mezzanine"
			   id="mezzanine"
			   @if(old('mezzanine')) value="{{ old('mezzanine') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->mezzanine }}"
				@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="bathrooms"
			   class="control-label"
        >Ba&ntilde;os</label>
		<input class="form-control"
			   type="number"
			   name="bathrooms"
			   id="bathrooms"
			   @if(old('bathrooms')) value="{{ old('bathrooms') }}"
			   @elseif(isset($property->tdi)) value="{{ $property->tdi->bathrooms }}"
				@endif>
	</fieldset>
	<fieldset class="form-check form-check-inline col-md">
		<input class="form-check-input"
               type="checkbox"
               id="private"
               name="private"
               @if(old('private') || isset($property->tdi->private)) checked @endif>
        <label for="private"
               class="form-check-label"
        >Privado</label>

	</fieldset>
</div>
<div class="row">
    <fieldset class="form-check form-check-inline col-md">
		<input class="form-check-input"
               type="checkbox"
               id="kitchenette"
               name="kitchenette"
               @if(old('kitchenette') || isset($property->tdi->kitchenette)) checked @endif>
        <label for="kitchenette"
               class="form-check-label"
        >Kitchenette</label>
	</fieldset>
	<fieldset class="form-check form-check-inline col-md">
        <input class="form-check-input"
           type="checkbox"
           id="kitchen"
           name="kitchen"
           @if(old('kitchen') || isset($property->tdi->kitchen)) checked @endif>
        <label for="kitchen"
               class="form-check-label"
        >Cocina</label>
	</fieldset>
</div><!-- /Especificaciones -->

<h3>Gastos</h3>
<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="expenses"
			   class="control-label"
        >Expensas</label>
		<expenses-form
				amount="@if(old('expenses')){{ old('expenses') }}@elseif(isset($property) && $property->expenses){{ $property->expenses }}@else{{ "0" }}@endif"
				range="@if(old('expenses_range')){{ old('expenses_range') }}@elseif(isset($property) && $property->expenses_range){{ $property->expenses_range }}@endif"
		></expenses-form>
	</fieldset>
</div><!-- /Gastos -->
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label for="description"
			   class="control-label"
        >Descripci&oacute;n</label>
		<textarea class="form-control"
				  name="description"
				  id="description"
        >@if(old('description')){{ old('description') }}@elseif(isset($property)){{ $property->description }}@endif</textarea>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="notes"
			   class="control-label"
        >Observaciones</label>
		<textarea class="form-control"
				  name="notes"
				  id="notes"
        >@if(old('notes')){{ old('notes') }}@elseif(isset($property)){{ $property->notes }}@endif</textarea>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="images"
			   class="control-label"
        >Im&aacute;genes</label>
		<input class="form-control"
			   type="file"
			   name="images[]"
			   id="images"
			   multiple>
	</fieldset>
</div>