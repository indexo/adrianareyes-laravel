<h3>Informaci&oacute;n de Inmueble</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="built_footage"
			   class="control-label"
		>Metraje <small>(m&sup2;)</small></label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="built_footage"
			   id="built_footage"
			   @if(old('built_footage')) value="{{ old('built_footage') }}"
			   @elseif(isset($property)) value="{{ $property->built_footage }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="distance_from_sea"
			   class="control-label"
        >Distancia del mar <small>(m)</small></label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="distance_from_sea"
			   id="distance_from_sea"
			   @if(old('distance_from_sea')) value="{{ old('distance_from_sea') }}"
			   @elseif(isset($property)) value="{{ $property->distance_from_sea }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="price"
			   class="control-label"
        >Precio</label>
        <currency class="form-control"
                  @if(old('price')) data="{{ old('price') }}"
                  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->price }}"
                  @endif
                  name="price"/>
	</fieldset>
	<fieldset class="form-check form-check-inline col-md">
		<input class="form-check-input"
               type="checkbox"
               id="financing"
               name="financing"
               @if(old('financing') || isset($property->_tdo) && $property->_tdo->financing) checked @endif>
        <label for="financing"
               class="form-check-label"
        >Financiaci&oacute;n</label>
	</fieldset>
</div>
<hr>

<h3>Comodidades</h3>
<div class="row">
    @forelse(App\Environment::all() as $env)
        <environment
                :id="{{ $env->id }}"
                label="{!! $env->name !!}"
                type="{{ $env->type }}"
                @if(old('environments.'.$env->id))
                value="{{ old('environments.'.$env->id) }}"
                @elseif(isset($property) && $property->environment($env->id))
                value="{{ $property->environment($env->id)->value }}"
                @endif
        ></environment>
    @empty
        <p>
            No hay comodidades disponibles.
            <a target="_blank"
href="{{ action('EnvironmentController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Comodidades -->
<hr>

<h3>Equipamiento</h3>
<div class="row">
    @forelse(App\Equipment::all() as $eq)
        <equipment
                :id="{{ $eq->id }}"
                label="{!! $eq->name !!}"
                type="{{ $eq->type }}"
                @if(old('equipment.'.$eq->id))
                value="{{ old('equipment.'.$eq->id) }}"
                @elseif(isset($property) && $property->equipment($eq->id))
                value="{{ $property->equipment($eq->id)->value }}"
                @endif
        ></equipment>
    @empty
        <p>
            No hay equipamientos disponibles.
            <a target="_blank"
href="{{ action('EquipmentController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Equipamiento -->
<hr>

<h3>{!! __('properties.services_amenities_title') !!}</h3>
<div class="row">
    @forelse(App\ServiceAmenity::all() as $sa)
        <service-amenity
                :id="{{ $sa->id }}"
                label="{!! $sa->name !!}"
                type="{{ $sa->type }}"
                @if(old('services_amenities.'.$sa->id))
                value="{{ old('services_amenities.'.$sa->id) }}"
                @elseif(isset($property) && $property->service_amenity($sa->id))
                value="{{ $property->service_amenity($sa->id)->value }}"
                @endif
        ></service-amenity>
    @empty
        <p>
            No hay servicios o amenities disponibles.
            <a target="_blank"
href="{{ action('ServiceAmenityController@create') }}"> Agregar</a>
        </p>
    @endforelse
</div><!-- /Servicios y Amenities -->
<hr>

<h3>Gastos</h3>
<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="expenses"
			   class="control-label">Expensas</label>
		<expenses-form
				@if(old('expenses')) amount="{{ old('expenses') }}"@elseif(isset($property) && $property->expenses) amount="{{ $property->expenses }}"@else amount="{{ "0" }}"@endif
				@if(old('expenses_range')) range="{{ old('expenses_range') }}"@elseif(isset($property) && $property->expenses_range) range="{{ $property->expenses_range }}"@endif
		></expenses-form>
	</fieldset><!-- /Expensas -->
	<fieldset class="form-group col-md">
		<label for="expenses_property_tax"
			   class="control-label"
        >Contribuci&oacute;n inmobiliaria</label>
        <currency class="form-control"
                  @if(old('expenses_property_tax')) data="{{ old('expenses_property_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_property_tax }}"
                  @endif
                  name="expenses_property_tax"/>
	</fieldset><!-- /Contribucion inmobiliaria -->
	<fieldset class="form-group col-md">
		<label for="expenses_school_tax"
			   class="control-label"
        >Impuesto de Primaria</label>
        <currency class="form-control"
                  @if(old('expenses_school_tax')) data="{{ old('expenses_school_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_school_tax }}"
                  @endif
                  name="expenses_school_tax"/>
	</fieldset><!-- /Impuesto de primaria -->
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label for="description"
			   class="control-label"
        >Descripci&oacute;n</label>
		<textarea class="form-control"
				  name="description"
				  id="description"
        >@if(old('description')){{ old('description') }}@elseif(isset($property)){{ $property->description }}@endif</textarea>
	</fieldset><!-- /Descripcion -->
	<fieldset class="form-group col-md">
		<label for="notes"
			   class="control-label"
        >Observaciones</label>
		<textarea class="form-control"
				  name="notes"
				  id="notes"
        >@if(old('notes')){{ old('notes') }}@elseif(isset($property)){{ $property->notes }}@endif</textarea>
	</fieldset><!-- /Observaciones -->
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="images"
			   class="control-label"
        >Im&aacute;genes</label>
		<input class="form-control"
			   type="file"
			   name="images[]"
			   id="images"
			   multiple>
	</fieldset>
</div>