<h3>Informaci&oacute;n de Inmueble</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label
				for="price"
                class="control-label">Precio</label>
        <currency class="form-control"
                  @if(old('price')) data="{{ old('price') }}"
                  @elseif(isset($property->_tdo)) data="{{ $property->_tdo->price }}"
                  @endif
                  name="price"/>
	</fieldset>
	<fieldset class="form-check form-check-inline col-md">
		<input class="form-check-input"
               type="checkbox"
               id="financing"
               name="financing"
               @if(old('financing') || (isset($property->_tdo) && $property->_tdo->financing))
               checked
               @endif>
        <label for="financing"
               class="form-check-label"
        >Financiaci&oacute;n</label>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label
                for="property_location"
                class="control-label">Ubicaci&oacute;n</label>
		<input
                class="form-control"
                type="text"
                name="property_location"
                id="location"
                @if(old('property_location'))
                value="{{ old('property_location') }}"
                @elseif(isset($property) && isset($property->tdi))
                value="{{ $property->tdi->location }}"
                @endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="access"
class="control-label">Accesos</label>
		<input class="form-control"
type="text"
               name="access"
               id="access"
               @if(old('access'))
               value="{{ old('access') }}"
               @elseif(isset($property) && isset($property->tdi))
               value="{{ $property->tdi->access }}"
               @endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label
                for="area"
                class="control-label">Superficie</label>
		<input
                class="form-control"
                type="number"
                name="area"
                id="area"
                @if(old('area'))
                value="{{ old('area') }}"
                @elseif(isset($property))
                value="{{ $property->area }}"
                @endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label
                for="topography"
                class="control-label">Topograf&iacute;a</label>
		<input
                class="form-control"
                type="text"
                name="topography"
                id="topography"
                @if(old('topography'))
                value="{{ old('topography') }}"
                @elseif(isset($property) && isset($property->tdi))
                value="{{ $property->tdi->topography }}"
                @endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label
                for="activity"
                class="control-label">Actividad</label>
		<input
                class="form-control"
                type="text"
                name="activity"
                id="activity"
                @if(old('activity'))
                value="{{ old('activity') }}"
                @elseif(isset($property) && isset($property->tdi))
                value="{{ $property->tdi->activity }}"
                @endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label
                for="productivity"
                class="control-label">Productividad</label>
		<input
                class="form-control"
                type="text"
                name="productivity"
                id="productivity"
                @if(old('productivity'))
                value="{{ old('productivity') }}"
                @elseif(isset($property) && isset($property->tdi))
                value="{{ $property->tdi->productivity }}"
                @endif>
	</fieldset>
</div>
<hr>

<h3>Especificaciones</h3>
<!-- Tajamar -->
<div class="row">
    <property-specification
            checkbox="spec_cutwater"
            description="spec_cutwater_description"
            label="Tajamar"
            @if(old('spec_cutwater') || (isset($property->tdi) && $property->tdi->spec_cutwater))
            :checked="true"
            @endif
            @if(old('spec_cutwater_description'))
            text="{!! old('spec_cutwater_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->spec_cutwater_description !!}"
            @endif
    ></property-specification>
</div><!-- /Tajamar -->
<!-- Alambrados -->
<div class="row">
    <property-specification
            checkbox="spec_wiring"
            description="spec_wiring_description"
            label="Alambrados"
            @if(old('spec_wiring') || (isset($property->tdi) && $property->tdi->spec_wiring))
            :checked="true"
            @endif
            @if(old('spec_wiring_description'))
            text="{!! old('spec_wiring_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->spec_wiring_description !!}"
            @endif
    ></property-specification>
</div><!-- / Alambrados -->
<!-- Ríos y Arroyos -->
<div class="row">
    <property-specification
            checkbox="spec_rivers_streams"
            description="spec_rivers_streams_description"
            label="R&iacute;os y Arroyos"
            @if(old('spec_rivers_streams') || (isset($property->tdi) && $property->tdi->spec_rivers_streams))
            :checked="true"
            @endif
            @if(old('spec_rivers_streams_description'))
            text="{!! old('spec_rivers_streams_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->spec_rivers_streams_description !!}"
            @endif
    ></property-specification>
</div><!-- /Ríos y Arroyos -->
<!-- Montes -->
<div class="row">
    <property-specification
            checkbox="spec_hills"
            description="spec_hills_description"
            label="Montes"
            @if(old('spec_hills') || (isset($property->tdi) && $property->tdi->spec_hills))
            :checked="true"
            @endif
            @if(old('spec_hills_description'))
            text="{!! old('spec_hills_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->spec_hills_description !!}"
            @endif
    ></property-specification>
</div><!-- /Montes -->
<!-- Luz -->
<div class="row">
    <property-specification
            checkbox="spec_light"
            description="spec_light_description"
            label="Luz"
            @if(old('spec_light') || (isset($property->tdi) && $property->tdi->spec_light))
            :checked="true"
            @endif
            @if(old('spec_light_description'))
            text="{!! old('spec_light_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->spec_light_description !!}"
            @endif
    ></property-specification>
</div>
<!-- /Luz -->
<hr>

<h2>Construcciones</h2>
<div class="row">
    <property-house
            @callback="toggleHouse"
            @if(old('house') || (isset($property->tdi) && $property->tdi->house))
            :value="true"
            @endif
    ></property-house>
</div>

<div v-show="csHouse">
	<h3>Comodidades</h3>
	<div class="row">
        <!-- Comodidades -->
        @forelse(App\Environment::all() as $env)
            <environment
                :id="{{ $env->id }}"
                label="{!! $env->name !!}"
                type="{{ $env->type }}"
                @if(old('environments.'.$env->id))
                value="{{ old('environments.'.$env->id) }}"
                @elseif(isset($property) && $property->environment($env->id))
                value="{{ $property->environment($env->id)->value }}"
                @endif
            ></environment>
        @empty
            <p>
                No hay ambientes disponibles.
                <a target="_blank"
href="{{ action('EnvironmentController@create') }}"> Agregar</a>
            </p>
        @endforelse
	</div><!-- /Comodidades -->
	<hr>

	<h3>Exterior</h3>
	<div class="row">
        @forelse(App\Exterior::all() as $ext)
            <exterior
                    :id="{{ $ext->id }}"
                    label="{!! $ext->name !!}"
                    type="{{ $ext->type }}"
                    @if(old('exterior.'.$ext->id))
                    value="{{ old('exterior.'.$ext->id) }}"
                    @elseif(isset($property) && $property->exterior($ext->id))
                    value="{{ $property->exterior($ext->id)->value }}"
                    @endif
            ></exterior>
        @empty
            <p>
                No hay exteriores disponibles.
                <a target="_blank"
                   href="{{ action('ExteriorController@create') }}"> Agregar</a>
            </p>
        @endforelse
	</div><!-- /Exterior -->
	<hr>
</div>

<!-- Apartamento de Invitados -->
<h3>
    <guest-room
            @callback="toggleGuestRoom"
            @if(old('guest_room') || (isset($property->tdi) && $property->tdi->guest_room))
            :value="true"
            @endif
    ></guest-room>
</h3>
<div v-show="guestRoom">
    <div class="row">
        <fieldset class="form-group col-md">
            <label
                    for="guest_room_rooms"
                    class="control-label">Dormitorios</label>
            <input
                    class="form-control"
                    type="number"
                    min="0"
                    name="guest_room_rooms"
                    id="guest_room_rooms"
                    @if(old('guest_room_rooms'))
                    value="{{ old('guest_room_rooms') }}"
                    @elseif(isset($property->tdi))
                    value="{{ $property->tdi->guest_room_rooms }}"
                    @endif>
        </fieldset>

        <fieldset class="form-group col-md">
            <label
                    for="guest_room_bathrooms"
                    class="control-label">Ba&ntilde;os</label>
            <input
                    class="form-control"
                    type="number"
                    min="0"
                    name="guest_room_bathrooms"
                    id="guest_room_bathrooms"
                    @if(old('guest_room_bathrooms'))
                    value="{{ old('guest_room_bathrooms') }}"
                    @elseif(isset($property->tdi))
                    value="{{ $property->tdi->guest_room_bathrooms }}"
                    @endif>
        </fieldset>
        <fieldset class="form-check form-check-inline col-md">
            <input
                    class="form-check-input"
                    type="checkbox"
                    name="guest_room_kitchen"
                    id="guest_room_kitchen"
                    @if(old('guest_room_kitchen') || (isset($property->tdi) && $property->tdi->guest_room_kitchen))
                    checked
                    @endif>
            <label
                    for="guest_room_kitchen"
                    class="form-check-label"
            >Cocina</label>
        </fieldset>
        <fieldset class="form-check form-check-inline col-md">
            <input
                    class="form-check-input"
                    type="checkbox"
                    name="guest_room_kitchenette"
                    id="guest_room_kitchenette"
                    @if(old('guest_room_kitchenette') || (isset($property->tdi) && $property->tdi->guest_room_kitchenette))
                    checked
                    @endif>
            <label
                    for="guest_room_kitchenette"
                    class="form-check-label">Kitchenette</label>
        </fieldset>
        <fieldset class="form-check form-check-inline col-md">
            <input
                    class="form-check-input"
                    type="checkbox"
                    name="guest_room_living_room"
                    id="guest_room_living_room"
                    @if(old('guest_room_living_room') || (isset($property->tdi) && $property->tdi->guest_room_living_room))
                    checked
                    @endif>
            <label
                    for="guest_room_living_room"
                    class="form-check-label"
            >Living comedor</label>

        </fieldset>
    </div>
</div><!-- /Apartamentos de Invitados -->
<hr>

<!-- Más especificaciones -->
<!-- Galpones -->
<div class="row">
    <property-specification
            checkbox="sheds"
            description="sheds_description"
            label="Galpones"
            @if(old('sheds') || (isset($property->tdi) && $property->tdi->sheds))
            :checked="true"
            @endif
            @if(old('sheds_description'))
            text="{!! old('sheds_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->sheds_description !!}"
            @endif
    ></property-specification>
</div><!-- Galpones -->
<!-- Establos -->
<div class="row">
    <property-specification
            checkbox="stables"
            description="stables_description"
            label="Establos"
            @if(old('stables') || (isset($property->tdi) && $property->tdi->stables))
            :checked="true"
            @endif
            @if(old('stables_description'))
            text="{!! old('stables_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->stables_description !!}"
            @endif
    ></property-specification>
</div><!-- /Establos -->
<!-- Otros -->
<div class="row">
    <property-specification
            checkbox="others"
            description="others_description"
            label="Luz"
            @if(old('others') || (isset($property->tdi) && $property->tdi->others))
            :checked="true"
            @endif
            @if(old('others_description'))
            text="{!! old('others_description') !!}"
            @elseif(isset($property) && $property->tdi)
            text="{!! $property->tdi->others_description !!}"
            @endif
    ></property-specification>
</div><!-- /Otros -->
<!-- /Más especificaciones -->
<hr>

<h3>Gastos</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label
                for="expenses_property_tax"
                class="control-label">Contribuci&oacute;n inmobiliaria</label>
        <currency class="form-control"
                  @if(old('expenses_property_tax'))
                  data="{{ old('expenses_property_tax') }}"
                  @elseif(isset($property))
                  data="{{ $property->expenses_property_tax }}"
                  @endif
                  name="expenses_property_tax"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label
                for="expenses_school_tax"
                class="control-label">Impuesto de Primaria</label>
        <currency class="form-control"
                  @if(old('expenses_school_tax'))
                  data="{{ old('expenses_school_tax') }}"
                  @elseif(isset($property))
                  data="{{ $property->expenses_school_tax }}"
                  @endif
                  name="expenses_school_tax"/>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label
                for="description"
                class="control-label">Descripci&oacute;n</label>
		<textarea
                class="form-control"
                name="description"
                id="description"
        >@if(old('description')){{ old('description') }}@elseif(isset($property)){{ $property->description }}@endif</textarea>
	</fieldset>
	<fieldset class="form-group col-md">
		<label
                for="notes"
                class="control-label">Observaciones</label>
		<textarea
                class="form-control"
                name="notes"
                id="notes"
        >@if(old('notes')){{ old('notes') }}@elseif(isset($property)){{ $property->notes }}@endif</textarea>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md-6">
		<label
                for="images"
                class="control-label">Im&aacute;genes</label>
		<input
                class="form-control"
                type="file"
                name="images[]"
                id="images"
                multiple>
	</fieldset>
</div>

