<h3>Informaci&oacute;n de Inmueble</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="price"
			   class="control-label"
		>Precio</label>
		<currency class="form-control"
                  @if(old('price')) data="{{ old('price') }}"
                  @elseif(isset($property)) data="{{ $property->_tdo->price }}"
                  @endif
				  name="price"/>
	</fieldset>
    <fieldset class="form-check form-check-inline col-md">
        <input class="form-check-input"
               type="checkbox"
               id="financing"
               name="financing"
               @if(old('financing') || (isset($property->_tdo) && $property->_tdo->financing)) checked @endif>
        <label for="financing"
               class="form-check-label"
        >Financiaci&oacute;n</label>
    </fieldset>
	<fieldset class="form-group col-md">
		<label for="distance_from_sea"
			   class="control-label"
        >Distancia del mar <small>(m)</small></label>
		<input class="form-control"
			   type="number"
			   min="0"
			   name="distance_from_sea"
			   id="distance_from_sea"
			   @if(old('distance_from_sea')) value="{{ old('distance_from_sea') }}"
			   @elseif(isset($property)) value="{{ $property->distance_from_sea }}"
				@endif>
	</fieldset>
</div>
<hr>

<h3>Informaci&oacute;n de Venta</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="area"
			   class="control-label"
        >Superficie</label>
		<input class="form-control"
			   type="number"
			   name="area"
			   id="area"
			   @if(old('area')) value="{{ old('area') }}"
			   @elseif(isset($property)) value="{{ $property->area }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="frontyard"
			   class="control-label"
        >Frente</label>
		<input class="form-control"
			   type="number"
			   name="frontyard"
			   id="frontyard"
			   @if(old('frontyard')) value="{{ old('frontyard') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->frontyard }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="backyard"
			   class="control-label"
        >Fondo</label>
		<input class="form-control"
			   type="number"
			   name="backyard"
			   id="backyard"
			   @if(old('backyard')) value="{{ old('backyard') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->backyard }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="sides"
			   class="control-label"
        >Laterales</label>
		<input class="form-control"
			   type="number"
			   name="sides"
			   id="sides"
			   @if(old('sides')) value="{{ old('sides') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->sides }}"
				@endif>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label for="fot"
			   class="control-label"
        >FOT</label>
		<input class="form-control"
			   type="number"
			   name="fot"
			   id="fot"
			   @if(old('fot')) value="{{ old('fot') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->FOT }}"
				@endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="fos"
			   class="control-label"
        >FOS</label>
		<input class="form-control"
			   type="number"
			   name="fos"
			   id="fos"
			   @if(old('fos')) value="{{ old('fos') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->FOS }}"
				@endif>
	</fieldset>
</div>
<hr>

<h3>Retiros</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="ret_frontyard"
			   class="control-label"
        >Frente</label>
		<input class="form-control"
			   type="number"
			   name="ret_frontyard"
			   id="ret_frontyard"
			   @if(old('ret_frontyard')) value="{{ old('ret_frontyard') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->ret_frontyard }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="ret_backyard"
			   class="control-label"
        >Fondo</label>
		<input class="form-control"
			   type="number"
			   name="ret_backyard"
			   id="ret_backyard"
			   @if(old('ret_backyard')) value="{{ old('ret_backyard') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->ret_backyard }}"
				@endif>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="ret_bilateral"
			   class="control-label"
        >Bilateral</label>
		<input class="form-control"
			   type="number"
			   name="ret_bilateral"
			   id="ret_bilateral"
			   @if(old('ret_bilateral')) value="{{ old('ret_bilateral') }}"
			   @elseif(isset($property) && $property->tdi) value="{{ $property->tdi->ret_bilateral }}"
				@endif>
	</fieldset>
</div>

<h3>Gastos</h3>
<div class="row">
	<fieldset class="form-group col-md">
		<label for="expenses_property_tax"
			   class="control-label"
        >Contribuci&oacute;n inmobiliaria</label>
        <currency class="form-control"
                  @if(old('expenses_property_tax')) data="{{ old('expenses_property_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_property_tax }}"
                  @endif
                  name="expenses_property_tax"/>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="expenses_school_tax"
			   class="control-label"
        >Impuesto de Primaria</label>
        <currency class="form-control"
                  @if(old('expenses_school_tax')) data="{{ old('expenses_school_tax') }}"
                  @elseif(isset($property)) data="{{ $property->expenses_school_tax }}"
                  @endif
                  name="expenses_school_tax"/>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md">
		<label for="description"
			   class="control-label"
        >Descripci&oacute;n</label>
		<textarea class="form-control"
				  name="description"
				  id="description"
        >@if(old('description')){{ old('description') }}@elseif(isset($property)){{ $property->description }}@endif</textarea>
	</fieldset>
	<fieldset class="form-group col-md">
		<label for="notes"
			   class="control-label"
        >Observaciones</label>
		<textarea class="form-control"
				  name="notes"
				  id="notes"
        >@if(old('notes')){{ old('notes') }}@elseif(isset($property)){{ $property->notes }}@endif</textarea>
	</fieldset>
</div>
<hr>

<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="images"
			   class="control-label"
        >Im&aacute;genes</label>
		<input class="form-control"
			   type="file"
			   name="images[]"
			   id="images"
			   multiple>
	</fieldset>
</div>

