@extends('layouts.admin')

<?php
    $imgdir = 'images' . DIRECTORY_SEPARATOR . 'properties' . DIRECTORY_SEPARATOR;
?>

@section('title')
Inmuebles - {{ isset($property) ? 'Editar: ' . $property->reference() : 'Nuevo' }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page"><a href="{{ action('PropertyController@index') }}">Inmuebles</a></li>
    @if(isset($property))
        <li class="breadcrumb-item" aria-current="page"><a href="{{ action('PropertyController@edit', $property) }}">{{ $property->reference() }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Editar</li>
    @else
        <li class="breadcrumb-item active" aria-current="page">Nuevo</li>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <!-- Tipo de inmueble -->
            <select id="property_type"
                    class="form-control"
                    v-model="propertyType"
                    @change="loadOps">
                <option :value="null"
                        disabled=""
                        selected=""
                >Elija un tipo de inmueble</option>
                <option value="apartment">Apartamento</option>
                <option value="house">Casa</option>
                <option value="countryside">Chacra / Campo</option>
                <option value="business_premise">Local Comercial</option>
                <option value="land">Terreno</option>
            </select><!-- /select -->
        </div><!-- /.col-md-4 -->
        <div class="col-md-4">
            <!-- Tipo de operación -->
            <select id="operation_type"
                    class="form-control"
                    v-model="operationType"
                    @change="redirectToForm"
                    :disabled="propertyType == null">
                <option :value="null"
                        :selected="null"
                        disabled>Elija una operaci&oacute;n</option>
                <option v-for="op in this.ops"
                        v-bind:value="op.value"
                >@{{ op.text }}</option>
            </select>
        </div><!-- /.col-md-4 -->
    </div><!-- /.row -->
    <hr>

    <?php
        // Creamos el tipo de inmueble y operación del pasado en la URL por método HTTP GET o bien obtenemos ambos de la propiedad ya existente
        if ( isset(request()->tdi) ) {
            $tdi = request()->tdi;
        } elseif ( isset($property) && $property->tdi ) {
            $tdi = $property->tdi();
        } else {
            $tdi = null;
        }

        if ( isset(request()->tdo) ) {
            $tdo = request()->tdo;
        } elseif ( isset($property) && $property->tdo ) {
            $tdo = $property->tdo();
        } else {
            $tdo = null;
        }
    ?>

    @if($tdo && $tdi)
        <!-- Formulario de Inmueble -->
        <form   @if(isset($property))
                action="{{ action('PropertyController@update', $property, [ 'tdi' => request()->tdi, 'tdo' => request()->tdo ] ) }}"
                @else
                action="{{ action('PropertyController@store', [ 'tdi' => request()->tdi, 'tdo' => request()->tdo ] ) }}"
                @endif
                method="POST"
                class="form"
                enctype="multipart/form-data">

        <!-- Verificación de formulario -->
        {{ csrf_field() }}
        @if(isset($property))
            {{ method_field('put') }}
        @endif

        <!-- Tipos de inmueble y operación  -->
            <input  type="hidden"
                    name="_tdi"
                    value="{{ $tdi }}">
            <input  type="hidden"
                    name="_tdo"
                    value="{{ $tdo }}">

            <!-- Tipos de moneda -->
            <div class="row">
                @if(isset($property) && $property->rent || $tdo === 'rent' || $tdo === 'rent-sale')
                    <!-- Moneda Alquiler -->
                    <form-currency name="currency_rent"
                                   v-model="currencyRent"
                                   label="Moneda para alquiler"
                                   :options="{{ App\Country::all() }}"
                                   @if(old('currency_rent'))
                                        :default="{{ old('currency_rent') }}"
                                   @elseif(isset($property) && $property->rent && $property->rent->country())
                                       :default="{{ $property->rent->country()->id }}"
                                   @endif
                    ></form-currency><!-- /Moneda Alquiler -->
                @endif

                @if(isset($property) && $property->sale || $tdo === 'sale' || $tdo === 'rent-sale')
                    <!-- Moneda Venta -->
                    <form-currency name="currency_sale"
                                   label="Moneda para venta"
                                   :options="{{ App\Country::all() }}"
                                   @if(old('currency_sale'))
                                        :default="{{ old('currency_sale') }}"
                                   @elseif(isset($property) && $property->sale &&  $property->sale->country())
                                        :default="{{ $property->sale->country()->id }}"
                                   @endif
                    ></form-currency><!-- /Moneda Venta -->
                @endif

                    <!-- Moneda Gastos -->
                    <form-currency name="currency_prop"
                                   label="Moneda para gastos"
                                   :options="{{ App\Country::all() }}"
                                   @if(old('currency_prop'))
                                        :default="{{ old('currency_prop') }}"
                                   @elseif(isset($property) && $property->country())
                                        :default="{{ $property->country()->id }}"
                                   @endif
                    ></form-currency><!-- /Moneda Gastos -->
            </div><!-- /Tipos de moneda -->

        @switch($tdi)
            @case('apartment')
            @switch($tdo)
                @case('rent')
                <!-- Apartamentos: Alquiler -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Apartamentos: Alquiler</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>
                                <label  for="rented"
                                        class="form-check-label"
                                >Alquilado</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.apartments.rent')
                <!-- /Apartamentos: Alquiler -->
                @break
                @case('sale')
                <!-- Apartamentos: Venta -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Apartamentos: Venta</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label  for="sold"
                                        class="form-check-label"
                                >Vendido</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.apartments.sale')
                <!-- /Apartamentos: Alquiler -->
                @break
                @case('rent-sale')
                <!-- Apartamentos: Alquiler y venta -->
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="text-center">Apartamentos: Alquiler y venta</h1>
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-4 form-inline">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>

                                <label  for="rented"
                                        class="form-check-label"
                                >Alquilado</label>
                            </fieldset>
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label  for="sold"
                                        class="form-check-label"
                                >Vendido</label>
                            </fieldset>
                        </div><!-- /.col-md-4 .form-inline -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.apartments.rent_sale')
                <!-- /Apartamentos: Alquiler y venta -->
                @break
            @endswitch
            @break
            @case('house')
            @switch($tdo)
                @case('rent')
                <!-- Casas: Alquiler -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Casas: Alquiler</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>
                                <label  for="rented"
                                        class="form-check-label"
                                >Alquilado</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.houses.rent')
                <!-- /Casas: Alquiler -->
                @break
                @case('sale')
                <!-- Casas: Venta -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Casas: Venta</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label  for="sold"
                                        class="form-check-label"
                                >Vendido</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.houses.sale')
                <!-- /Casas: Venta -->
                @break
                @case('rent-sale')
                <!-- Casas: Alquiler y venta -->
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="text-center">Casas: Alquiler y venta</h1>
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-4 form-inline">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>
                                <label  for="rented"
                                        class="form-check-label"
                                >Alquilado</label>
                            </fieldset>
                            <fieldset class="form-group">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label for="sold" class="form-check-label">Vendido</label>
                            </fieldset>
                        </div><!-- .col-md-4 .form-inline -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.houses.rent_sale')
                <!-- /Casas: Alquiler y venta -->
                @break
            @endswitch
            @break
            @case('countryside')
            <!-- Chacras y campos: Venta -->
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="text-center">Campos y chacras: Venta</h1>
                    </div><!-- /.col-md-10 -->
                    <div class="col-md-2">
                        <fieldset class="form-check form-check-inline">
                            <input  class="form-check-input"
                                    type="checkbox"
                                    id="sold"
                                    name="sold"
                                    @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                    checked
                                    @endif>
                            <label  for="sold"
                                    class="form-check-label"
                            >Vendido</label>
                        </fieldset>
                    </div><!-- /.col-md-2 -->
                </div><!-- /.row -->
                <hr>

            @include('admin.properties.partials.countrysides.sale')
            <!-- /Chacras y campos: Venta -->
            @break
            @case('business_premise')
            @switch($tdo)
                @case('rent')
                <!-- Locales comerciales: Alquiler -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Locales comerciales: Alquiler</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>
                                <label  for="rented"
                                        class="form-check-label"
                                >Alquilado</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.business_premises.rent')
                <!-- /Locales comerciales: Alquiler -->
                @break
                @case('sale')
                <!-- Locales comerciales: Venta -->
                    <div class="row">
                        <div class="col-md-10">
                            <h1 class="text-center">Locales comerciales: Venta</h1>
                        </div><!-- /.col-md-10 -->
                        <div class="col-md-2">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label  for="sold"
                                        class="form-check-label"
                                >Vendido</label>
                            </fieldset>
                        </div><!-- /.col-md-2 -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.business_premises.sale')
                <!-- /Locales comerciales: Venta -->
                @break
                @case('rent-sale')
                <!-- Locales comerciales: Alquiler y venta -->
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="text-center">Locales comerciales: Alquiler y venta</h1>
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-4 form-inline">
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="rented"
                                        name="rented"
                                        @if(old('rented') || (isset($property->_tdo) && $property->_tdo->rented))
                                        checked
                                        @endif>
                                <label for="rented" class="form-check-label">Alquilado</label>
                            </fieldset>
                            <fieldset class="form-check form-check-inline">
                                <input  class="form-check-input"
                                        type="checkbox"
                                        id="sold"
                                        name="sold"
                                        @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                        checked
                                        @endif>
                                <label  for="sold"
                                        class="form-check-label"
                                >Vendido</label>
                            </fieldset>
                        </div><!-- /.col-md-4 .form-inline -->
                    </div><!-- /.row -->
                    <hr>

                @include('admin.properties.partials.business_premises.rent_sale')
                <!-- /Locales comerciales: Alquiler y venta -->
                @break
            @endswitch
            @break
            @case('land')
            <!-- Terrenos: Venta -->
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="text-center">Terrenos: Venta</h1>
                    </div><!-- /.col-md-10 -->
                    <div class="col-md-2">
                        <fieldset class="form-check form-check-inline">
                            <input  class="form-check-input"
                                    type="checkbox"
                                    id="sold"
                                    name="sold"
                                    @if(old('sold') || (isset($property->_tdo) && $property->_tdo->sold))
                                    checked
                                    @endif>
                            <label  for="sold"
                                    class="form-check-label">Vendido</label>
                        </fieldset>
                    </div><!-- /.col-md-2 -->
                </div><!-- /.row -->
                <hr>

            @include('admin.properties.partials.lands.sale')
            <!-- /Terrenos: Venta -->
            @break
        @endswitch

        <!-- Geografía -->
            <div class="row">
                <!-- País -->
                <geography-field
                        label="Pa&iacute;s"
                        :callback="selectedCountry"
                        placeholder="Elija un pa&iacute;s"
                        name="country"
                        :collection="countries"
                        @onchange="selectCountry"
                        @if(old('country'))
                        selected="{{ old('country') }}"
                        @elseif(isset($property) && $property->zone)
                        selected="{{ $property->zone->location->department->country->id }}"
                        @endif
                ></geography-field>
                <!-- Departamento -->
                <geography-field
                        label="Departamento"
                        :callback="selectedDepartment"
                        placeholder="Elija un departamento"
                        name="department"
                        :collection="departments"
                        @onchange="selectDepartment"
                        @if(old('department'))
                        selected="{{ old('department') }}"
                        @elseif(isset($property) && $property->zone)
                        selected="{{ $property->zone->location->department->id }}"
                        @else
                        :disabled="selectedCountry < 1"
                        @endif
                ></geography-field>
                <!-- Localidad -->
                <geography-field
                        label="Localidades"
                        :callback="selectedLocation"
                        placeholder="Elija una localidad"
                        name="location"
                        :collection="localities"
                        @onchange="selectLocation"
                        @if(old('location'))
                        selected="{{ old('location') }}"
                        @elseif(isset($property) && $property->zone)
                        selected="{{ $property->zone->location->id }}"
                        @else
                        :disabled="selectedDepartment < 1"
                        @endif
                ></geography-field>
                <!-- Zona -->
                <geography-field
                        label="Zonas"
                        :callback="selectedZone"
                        placeholder="Elija una zona"
                        name="zone"
                        :collection="zones"
                        @if(old('zone'))
                        selected="{{ old('zone') }}"
                        @elseif(isset($property) && $property->zone)
                        selected="{{ $property->zone->id }}"
                        @else
                        :disabled="selectedLocation < 1"
                        @endif
                ></geography-field>
            </div><!-- / Geografía -->

            <div class="row">
                <fieldset class="form-group col-md-6">
                    <label for="address"
                           class="control-label">
                        Direcci&oacute;n <small>(no visible)</small>
                    </label>
                    <input  class="form-control"
                            type="text"
                            name="address"
                            id="address"
                            @if(old('address'))
                            value="{{ old('address') }}"
                            @elseif(isset($property))
                            value="{{ $property->address }}"
                            @elseif(isset($property))
                            value="{{ $property->address }}"
                            @endif>
                </fieldset>
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-md-6">
                    <fieldset class="form-check form-check-inline">
                        <input  class="form-check-input"
                                type="checkbox"
                                id="featured"
                                name="featured"
                                @if(old('featured') || isset($property->featured))
                                checked
                                @endif>
                        <label for="featured" class="form-check-label"><strong>Mueble Destacado</strong></label>
                    </fieldset>
                </div>
            </div><!-- /.row -->

            <div class="row"
                 style="margin-top: 30px;">
                <fieldset class="form-group col-md">
                    <button class="btn btn-success"
                            type="submit"
                    >Guardar propiedad</button>
                </fieldset>
            </div><!-- /.row -->
        </form>
    @endif

    @if(isset($property))
        <!-- Imágenes -->
        <h3 class="text-center">Im&aacute;genes</h3>
        <hr>
        <form id="borrar-imagenes" method="POST" action="{{ action('PropertyController@deleteImages', ['property' => $property ]) }}">
            {{ csrf_field() }}
            {{ method_field('delete') }}
            <table class="table table-bordered table-striped" role="table" id="imagenes">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">ID</th>
                        <th class="text-center">Tama&ntilde;o</th>
                        <th class="text-center">Fecha de actualizaci&oacute;n</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($property->images as $image)
                    <tr>
                        <td><input type="checkbox" name="img[]" value="{{ $image->id }}"></td>
                        <td># {{ $image->id }}</td>
                        <td>{{ round(\Storage::size($imgdir . $image->name)/1000) }} KB</td>
                        <td>{{ $image->updated_at }}</td>
                        <td class="text-center">
                            <div class="btn-group" role="group" aria-label="Acciones">
                                <a class="btn btn-primary imagen-tooltip" data-toggle="tooltip" title="<img style='width: 200%;padding: 1px;' src='{{ url('imagenes/inmuebles/'.$image->name) }}'>"><i class="fa fa-eye"></i></a>
                                @if($image->cover)
                                <a class="btn btn-secondary portada-actual-tooltip" data-toggle="tooltip" title="Portada actual"><i class="fa fa-picture-o"></i></a>
                                @else
                                <a class="btn btn-primary portada-tooltip" data-toggle="tooltip" title="Establecer como portada" onclick="setCover({{ $image->id }})"><i class="fa fa-picture-o"></i></a>
                                @endif
                                <button class="btn btn-danger" onclick="deleteImage({{ $image->id }})"><i class="fa fa-trash"></i></button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar im&aacute;genes</button>
        </form>
        <!-- /Imágenes -->
    @endif

@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@endpush

@if(isset($property))
    @push('js')
    $(function() {
        $('#imagenes').DataTable({
            order: [[ 2, "desc" ]],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
            }
        });

        $('.imagen-tooltip').tooltip({
            animated: 'fade',
            placement: 'auto',
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-inner" style="background-color: transparent;"></div></div>'
        });

        $('.portada-tooltip').tooltip({
            animated: 'fade',
            placement: 'auto'
        });

        $('.portada-actual-tooltip').tooltip({
            animated: 'fade',
            placement: 'auto'
        });

        function setCover(id) {
            $('#borrar-imagenes').attr('action', `{{ action('PropertyController@setCover', [ 'property' => $property]) }}?id=${id}`);
            $('#borrar-imagenes').attr('method', 'POST');
            $('#borrar-imagenes').submit();
        }

        function deleteImage(id) {
            $('#borrar-imagenes').attr('action', `{{ action('PropertyController@deleteImages', [ 'property' => $property]) }}?id=${id}`);
            $('#borrar-imagenes').submit();
        }
    })
    @endpush
@endif
