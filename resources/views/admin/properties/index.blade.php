@extends('layouts.admin')

@section('title', 'Inmuebles - Inicio')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Inmuebles</li>
@endsection

@section('content')
<p><a href="{{ action('PropertyController@create') }}"
   class="btn btn-primary">
        <i class="fa fa-plus"></i> Agregar Inmueble
    </a>
</p>
<table class="table table-bordered table-striped text-center" id="properties">
	<thead>
		<tr>
			<th>Referencia</th>
			<th>Direcci&oacute;n</th>
			<th>Precio</th>
			<th>Estado</th>
			<th>Destacado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($properties as $p)
			<tr>
                <td class="text-center"><strong>#{{ $p->reference() }}</strong></td>
				<td>{!! $p->address !!}</td>
				<td>
                    <?php
                        $sign = '&dollar;';
                        if($p->sale && $p->rent) {
                            $sign = $p->sale->country()->currency_symbol;
                        } else if($p->sale) {
                            $sign = $p->sale->country()->currency_symbol;
                        } else {
                            $sign = $p->rent->country()->currency_symbol;
                        }

                        if($p->sale) {
                            $price = $p->sale->price;
                        } elseif($p->rent) {
                            $price = $p->rent->january;
                        } else {
                            $price = 0;
                        }
                    ?>
                    {!! $sign !!}
					{{ @number_format($price,0,",",".") }}
                </td>
				<td>
					<?php
						$status = '';

						if($p->sale && $p->rent) {
							$status = $p->sale->status() . ' / ' . $p->rent->status();
						} else if($p->sale) {
							$status = $p->sale->status();
						} else {
							$status = $p->rent->status();
						}
					?>
					{{ $status }}
				</td>
				<td>
					@if($p->featured)
					SI
					@else
					NO
					@endif
				</td>
				<td>
					<form 
						method="POST"
						action="{{ action('PropertyController@destroy', $p) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div 
							class="btn-group" 
							role="group" 
							aria-label="Acciones">
							<a href="{{ action('WebController@showProperty', $p->reference()) }}"
								class="btn btn-primary"
								target="_blank">
								<i class="fa fa-eye"></i>
							</a>
							<a 
								href="{{ action('PropertyController@edit', $p) }}"
								class="btn btn-primary">
								<i class="fa fa-pencil"></i>
							</a>
							<button 
							 	type="submit"
								class="btn btn-danger">
								<i class="fa fa-trash"></i>
							</button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
    $('#properties').DataTable({
    language: {
    url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
    }
    });
@endpush