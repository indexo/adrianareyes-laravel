@extends('layouts.admin')

@section('title', 'Configuraci&oacute;n')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Configuraci&oacute;n</li>
@endsection

@section('content')
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading"><i class="fa fa-exclamation-circle"></i> La configuraci&oacute;n puede afectar el correcto funcionamiento del sistema</h4>
    <p>La informaci&oacute;n puede ser modificada directamente en el archivo <i>.env</i>.</p>
    <hr>
    <p class="mb-0">Se recomienda que el uso de &eacute;sta configuraci&oacute;n se realice exclusivamente por los administradores.</p>
</div>
<div class="row">
	<div class="col-md-5">
		<h1>Base de datos</h1>

		<form action="{{ action('InstallationController@createDB') }}" method="POST" class="form">
			{{ csrf_field() }}
			<fieldset class="form-group">
				<label for="database-host" class="control-label">
					Host
				</label>
				<input type="text" name="database_host" id="database-host" class="form-control" value="{{ old('database_host') ? old('database_host') : env('DB_HOST') }}">
			</fieldset>
			<fieldset class="form-group">
				<label for="database-name" class="control-label">
					Nombre
				</label>
				<input type="text" name="database_name" id="database-name" class="form-control" value="{{ old('database_name') ? old('database_name') : env('DB_DATABASE') }}">
			</fieldset>
			<fieldset class="form-group">
				<label for="database-user" class="control-label">
					Administrador
				</label>
				<input type="text" name="database_user" id="database-user" class="form-control" value="{{ old('database_user') ? old('database_user') : env('DB_USERNAME') }}">
			</fieldset>
			<fieldset class="form-group">
				<label for="database-password" class="control-label">
					Contrase&ntilde;a
				</label>
				<input type="password" name="database_password" id="database-password" class="form-control" value="{{ old('database_password') ? old('database_password') : env('DB_PASSWORD') }}">
			</fieldset>
			<fieldset class="form-group">
				<button type="submit" class="btn btn-success">Guardar</button>
			</fieldset>
		</form>

		<form action="{{ action('InstallationController@resetConf') }}" method="POST" class="form">
			{{ csrf_field() }}
			<fieldset class="form-group">
				<button type="submit" class="btn btn-success">Reiniciar archivo de configuraci&oacute;n</button>
			</fieldset>
		</form>

		<form action="{{ action('InstallationController@migrateTables') }}" method="POST" class="form">
			{{ csrf_field() }}
			<fieldset class="form-group">
				<button type="submit" class="btn btn-success">Migrar tablas</button>
			</fieldset>
		</form>

		<form action="{{ action('InstallationController@resetTables') }}" method="POST" class="form">
			{{ csrf_field() }}
			<fieldset class="form-group">
				<button type="submit" class="btn btn-success">Reiniciar Base de datos</button>
			</fieldset>
		</form>

		<form action="{{ action('InstallationController@exportDB') }}" method="POST" class="form">
			{{ csrf_field() }}
			<fieldset class="form-group">
				<button type="submit" class="btn btn-success">Exportar BD</button>
				<small>S&oacute;lo disponible en Linux</small>
			</fieldset>
		</form>

		<form action="{{ action('InstallationController@erase_activities') }}"
              class="form"
              method="POST">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <button type="submit"
                        class="btn btn-warning"
                >Limpiar actividades antiguas</button>
            </fieldset>
        </form>
	</div>
    <div class="col-md-5">
        <h1>Correo electr&oacute;nico</h1>
        <p class="lead">
            <a href="https://mailgun.com/" target="_blank">Configurar dominio</a>
        </p>

        <form action="{{ action('InstallationController@configEmail') }}" method="POST" class="form">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <label for="domain" class="control-label">Dominio</label>
                <input type="text" class="form-control" id="domain" name="domain" value="{{ old('domain') ? old('domain') : env('MAILGUN_DOMAIN') }}">
            </fieldset>
            <fieldset class="form-group">
                <label for="password" class="control-label">Contrase&ntilde;a Secreta</label>
                <input type="text" class="form-control" id="password" name="password" value="{{ old('password') ? old('password') : env('MAILGUN_SECRET') }}">
            </fieldset>
            <fieldset class="form-group">
                <label for="email-from" class="control-label">
                    Direcci&oacute;n de env&iacute;o
                </label>
                <input type="email" name="email_from" id="email-from" class="form-control" value="{{ old('email_from') ? old('email_from') : env('MAIL_USERNAME') }}">
            </fieldset>
            <fieldset class="form-group">
                <label for="email-password" class="control-label">
                    Contrase&ntilde;a de direcci&oacute;n de env&iacute;o
                </label>
                <input type="password" name="email_password" id="email-password" class="form-control" value="{{ old('email_password') ? old('email_password') : env('MAIL_PASSWORD') }}">
            </fieldset>
            <fieldset class="form-group">
                <button type="submit" class="btn btn-success">Guardar</button>
            </fieldset>
        </form>
    </div>
</div>
@endsection