@extends('layouts.admin')

@section('title', 'Equipamiento - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Equipamiento</li>
@endsection

@section('content')
<p><a href="{{ action('EquipmentController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Equipamiento</a></p>
<table class="table table-bordered table-striped text-center" id="equipment">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Tipo</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($equipment as $e)
			<tr>
				<td>{!! $e->name !!}</td>
				<td>
					<?php
						switch($e->type) {
							case 'int':
								$type = 'N&uacute;mero';
								break;
							case 'yesno':
								$type = 'S&iacute; o No';
								break;
							default:
								$type = 'Personalizado';
						}
					?>
					{!! $type !!}
				</td>
				<td>
					<form method="POST" action="{{ action('EquipmentController@destroy', $e->id, $e) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group">
							<a href="{{ action('EquipmentController@edit', $e->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="3">No hay equipamiento</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.css"/>
@endpush

@push('js_files')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.js"></script>
@endpush

@push('js')
    $('#equipment').DataTable({
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
        }
    });
@endpush