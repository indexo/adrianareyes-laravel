@extends('layouts.admin')

@section('title', 'Zonas - Nueva')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ZoneController@index') }}">Zonas</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nueva</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('ZoneController@store') }}" class="form col-md">
		{{ csrf_field() }}
		@include('admin.zones.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-plus"></i> Crear Zona</button>
	</form>
@endsection