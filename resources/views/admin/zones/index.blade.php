@extends('layouts.admin')

@section('title', 'Zonas - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Zonas</li>
@endsection

@section('content')
<p><a href="{{ action('ZoneController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Zona</a></p>
<table class="table table-bordered table-striped text-center" id="zones">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Localidad</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($zones as $z)
			<tr>
				<td>{!! $z->name !!} </td>
				<td>{!! $z->location->name !!}</td>
				<td>
					<form method="POST" action="{{ action('ZoneController@destroy', $z) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('ZoneController@edit', $z) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="4" class="text-center">No hay zonas</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#zones').DataTable({
		order: [[ 2, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush