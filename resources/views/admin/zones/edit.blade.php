@extends('layouts.admin')

@section('title', 'Zonas - Editar: ' . $zone->name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ZoneController@index') }}">Zonas</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ZoneController@edit', $zone) }}">{!! $zone->name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('ZoneController@update', $zone) }}" class="form col-md">
		{{ csrf_field() }}
		{{ method_field('put') }}
		@include('admin.zones.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Zona</button>
	</form>
@endsection