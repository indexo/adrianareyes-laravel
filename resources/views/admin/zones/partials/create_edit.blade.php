<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="name">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" @if(old('name')) value="{{ old('name') }}" @elseif(isset($zone) && $zone->name) value="{{ $zone->name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="location_id">Localidad</label>
		<select class="form-control" id="location_id" name="location_id">
			<option value="" @if(!old('location_id') || (isset($zone) && $zone->location_id)) selected @endif>Elija una localidad</option>
			@forelse(App\Location::all() as $l)
				<option  value="{{ $l->id }}" @if(old('location_id') == $l->id) selected @elseif(isset($zone) && $zone->location_id == $l->id) selected @endif>{!! $l->name !!}</option>
			@empty
				<option value="" disabled="" selected="">No hay localidades disponibles</option>
			@endforelse
		</select>
	</fieldset>
</div>