<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="title">T&iacute;tulo</label>
		<input type="text" class="form-control" id="title" name="title" @if(old('title')) value="{{ old('title') }}" @elseif(isset($service)) value="{{ $service->title }}" @endif>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="description">Descripci&oacute;n</label>
		<textarea class="form-control" id="description" name="description">@if(old('description')){{ old('description') }}@elseif(isset($service)){{ $service->description }}@endif</textarea>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="icon">&Iacute;cono <small>(<a href="http://fontawesome.io/icons/" target="_blank">Font Awesome</a>)</small></label>
		<input type="text" class="form-control" id="icon" name="icon" @if(old('icon')) value="{{ old('icon') }}" @elseif(isset($service)) value="{{ $service->icon }}" @endif>
	</fieldset>
</div>