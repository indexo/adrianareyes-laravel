@extends('layouts.admin')

@section('title', 'Servicios - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Servicios</li>
@endsection

@section('content')
<div class="row">
	<div class="col">
		<?php
			$site = App\Site::first();
		?>
		<form class="form" method="POST" action="{{ action('SiteController@services') }}">
			{{ csrf_field() }}
			<fieldset class="form-group col-md-8">
				<label for="services_description">Introducci&oacute;n</label>
				<textarea rows="10" class="form-control" id="services_description" name="services_description">@if(old('services')){{ old('services') }}@elseif(isset($site) && $site->services){{ $site->services }}@endif</textarea>
			</fieldset>
			<fieldset class="form-group col-md-12">
				<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Guardar</button>
			</fieldset>
		</form>
	</div>
</div>
<hr>
<p><a href="{{ action('ServiceController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Servicio</a></p>
<div class="row">
	<div class="col">
		<table class="table table-bordered text-center" id="services">
			<thead>
				<tr>
					<th>&Iacute;cono</th>
					<th>T&iacute;tulo</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@forelse($services as $s)
					<tr>
						<td>@if($s->icon) <i class="fa {{ $s->icon }}"></i> @else N/A @endif </td>
						<td>{{ $s->title }}</td>
						<td>
							<form method="POST" action="{{ action('ServiceController@destroy', $s) }}">
								{{ csrf_field() }}
								{{ method_field('delete') }}
								<div class="btn-group" role="group" aria-label="Acciones">
									<a href="{{ action('ServiceController@edit', $s) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
								</div>
							</form>
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="3" class="text-center">No hay servicios</td>
					</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#services').DataTable({
		order: [[ 3, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush