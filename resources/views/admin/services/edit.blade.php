@extends('layouts.admin')

@section('title', 'Servicios - Editar: ' . $service->title)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ServiceController@index') }}">Servicios</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ServiceController@edit', $service) }}">{!! $service->title !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('ServiceController@update', $service) }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		{{ method_field('put') }}
		@include('admin.services.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Servicio</button>
	</form>
@endsection