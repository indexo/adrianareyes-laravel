@extends('layouts.admin')

@section('title', 'Servicios - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('ServiceController@index') }}">Servicios</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('ServiceController@store') }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		@include('admin.services.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Crear Servicio</button>
	</form>
@endsection