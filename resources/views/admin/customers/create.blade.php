@extends('layouts.admin')

@section('title', 'Clientes - Nuevo')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('CustomerController@index') }}">Clientes</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('CustomerController@store') }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		@include('admin.customers.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-customer-plus"></i> Crear Cliente</button>
	</form>
@endsection