@extends('layouts.admin')

@section('title', 'Clientes - Editar: ' . $customer->first_name . ' ' . $customer->last_name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('CustomerController@index') }}">Clientes</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('CustomerController@edit', $customer) }}">{!! $customer->first_name !!} {!! $customer->last_name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('CustomerController@update', $customer) }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		{{ method_field('put') }}
		@include('admin.customers.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Cliente</button>
	</form>
@endsection