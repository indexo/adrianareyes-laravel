<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="first_name">Nombre</label>
		<input type="text" class="form-control" id="first_name" name="first_name" @if(old('first_name')) value="{{ old('first_name') }}" @elseif(isset($customer)) value="{{ $customer->first_name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="last_name">Apellido</label>
		<input type="text" class="form-control" id="last_name" name="last_name" @if(old('last_name')) value="{{ old('last_name') }}" @elseif(isset($customer)) value="{{ $customer->last_name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="email">Correo Electr&oacute;nico</label>
		<input type="email" class="form-control" id="email" name="email" @if(old('email')) value="{{ old('email') }}" @elseif(isset($customer)) value="{{ $customer->email }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="address">Direcci&oacute;n</label>
		<input type="text" class="form-control" id="address" name="address" @if(old('address')) value="{{ old('address') }}" @elseif(isset($customer)) value="{{ $customer->address }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="phones">Tel&eacute;fonos <small>(uno por l&iacute;nea)</small></label>
		<textarea class="form-control" id="phones" name="phones">@if(old('phones')){{ old('phones') }}@elseif(isset($customer)){{ $customer->phones }}@endif</textarea>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="type">Tipo</label>
		<select class="form-control" id="type" name="type">
			<option value="" @if(old('type') === null && !isset($customer)) selected @endif disabled>Elija un tipo de cliente</option>
			<option value="tenant" @if(old('type') === 'tenant' || (isset($customer) && $customer->type === 'tenant')) selected @endif >Inquilino</option>
			<option value="owner" @if(old('type') === 'owner' || (isset($customer) && $customer->type === 'owner')) selected @endif >Propietario</option>
		</select>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="notes">Notas</label>
		<textarea class="form-control" id="notes" name="notes">@if(old('notes')){{ old('notes') }}@elseif(isset($customer)){{ $customer->notes }}@endif</textarea>
	</fieldset>
</div>