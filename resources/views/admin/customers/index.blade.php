@extends('layouts.admin')

@section('title', 'Clientes - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page">Clientes</li>
@endsection

@section('content')
<a href="{{ action('CustomerController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Cliente</a>
<hr>
<table class="table table-bordered" id="customers">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Correo Electr&oacute;nico</th>
			<th>Direcci&oacute;n</th>
			<th>Tel&eacute;fonos</th>
			<th>Tipo</th>
			<th>Notas</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($customers as $c)
			<tr>
				<td>{{ $c->first_name . ' ' . $c->last_name }}</td>
				<td><a href="mailto:{{ $c->email }}" target="_blank">{{ $c->email }}</a></td>
				<td><a href="https://www.google.com.uy/maps/place/{{ $c->address }}" target="_blank">{{ $c->address }}</a></td>
				<td>{!! $c->phone_links !!}</td>
				<td>{{ $c->type === 'owner' ? 'Propietario' : 'Inquilino' }}</td>
				<td>{{ $c->notes }}</td>
				<td>
					<form method="POST" action="{{ action('CustomerController@destroy', $c) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('CustomerController@edit', $c) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="7" class="text-center">No hay clientes</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.js"></script>
@endpush

@push('js')
	$('#customers').DataTable({
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'excelHtml5',
				messageTop: 'Clientes',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5 ]
				}
			},
			{
				extend: 'csvHtml5',
				messageTop: 'Clientes',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5 ]
				}
			},
			{
				extend: 'pdfHtml5',
				messageTop: 'Clientes',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5 ]
				}
			}
        ],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush