<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="name">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" @if(old('name')) value="{{ old('name') }}" @elseif(isset($user)) value="{{ $user->name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="email">Correo Electr&oacute;nico</label>
		<input type="email" class="form-control" id="email" name="email" @if(old('email')) value="{{ old('email') }}" @elseif(isset($user)) value="{{ $user->email }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="password">Contrase&ntilde;a</label>
		<input type="password" class="form-control" id="password" name="password"></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="password_confirmation">Confirmar contrase&ntilde;a</label>
		<input type="password" class="form-control" id="password_confirmation" name="password_confirmation"></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-6">
		<label class="control-label" for="avatar">Foto</label>
		<input type="file" class="form-control" id="avatar" name="avatar"></input>
	</fieldset>
	@if(isset($user) && $user->avatar)
	<div class="col-md-6">
		<p>Foto actual</p>
		<img src="{{ action('UserController@getAvatar', $user) }}" alt="Foto de {{ $user->name }}" class="img-thumbnail" width="100px">
	</div>
	@endif
</div>
<div class="row">
	<fieldset class="form-group col-md-6">
		<label for="signature" class="control-label">Firma de correo electr&oacute;nico</label>
		<textarea name="signature" id="signature" class="form-control">@if(old('signature')){!! old('signature') !!}@elseif(isset($user)){!! $user->signature !!}@endif</textarea>
	</fieldset>
</div>

@push('js_files')
	<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
@endpush

@push('js')
	CKEDITOR.replace( 'signature' );
@endpush