@extends('layouts.admin')

@section('title', 'Administradores - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Administradores</li>
@endsection

@section('content')
<p><a href="{{ action('UserController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Administrador</a></p>
<table class="table table-bordered table-striped text-center" id="users">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Correo Electr&oacute;nico</th>
			<th>Registro</th>
			<th>Estado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($users as $u)
			<tr>
				<td><a href="{{ action('UserController@edit', $u) }}" target="_blank">{{ $u->name }}</a></td>
				<td>{{ $u->email }}</td>
				<td>{{ Carbon\Carbon::parse($u->created_at, 'America/Montevideo')->format('h:i:s d-m-Y') }}</td>
				<td>{{ $u->status() ? 'Activo' : 'Inactivo' }}</td>
				<td>
					@if(\Auth::id() !== $u->id)
					<form method="POST" action="{{ action('UserController@destroy', $u) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('UserController@edit', $u) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
						
						
					</form>
					@else
					<a href="{{ action('UserController@edit', $u) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
					@endif
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="6">No hay usuarios</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#users').DataTable({
		order: [[ 3, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush