@extends('layouts.admin')

@section('title', 'Administradores - Editar: ' . $user->name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('UserController@index') }}">Administradores</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('UserController@edit', $user) }}">{!! $user->name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('UserController@update', $user) }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		{{ method_field('put') }}
		@include('admin.users.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Administrador</button>
	</form>
@endsection