@extends('layouts.admin')

@section('title', 'Administrador - Nuevo')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('UserController@index') }}">Administradores</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('UserController@store') }}" enctype="multipart/form-data" class="form col-md">
		{{ csrf_field()}}
		@include('admin.users.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-user-plus"></i> Crear Administrador</button>
	</form>
@endsection