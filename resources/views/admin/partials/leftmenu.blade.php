<?php 
$site = \App\Site::first();
if(!$site) {
  $site = new \StdClass;
  $site->company_name = 'Sistema Inmobiliario';
}

$url = request()->url();
$url = explode("/", $url);
$url = $url[count($url)-1];

?>
<div class="nav-side-menu">
    <div class="brand">
		{{ $site->company_name }}<br>
      <strong><i>Administraci&oacute;n &nbsp;</i></strong>

    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

        <div class="user text-center col-xs-7">
            <a href="{{ action('UserController@edit', Auth::user()) }}">
                <img src="{{ action('UserController@getAvatar', Auth::user()) }}" alt="{{ Auth::user()->name }}" class="rounded-circle">
            </a>
            <p>
                <a href="{{ action('UserController@edit', Auth::user()) }}">{{ Auth::user()->name }}</a>
                &nbsp;&nbsp;
                <i class="fa fa-sign-out fa-lg"></i> <a href="{{ route('desconectar') }}">Salir</a>
            </p>
        </div>

        <hr>
        <div class="menu-list">

            <!-- Menú Principal -->
            <ul id="menu-content" class="menu-content collapse out">
                <li id="administracion">
                    <a href="{{ action('WebController@admin') }}">
                        <i class="fa fa-dashboard fa-lg"></i> Panel de Control
                    </a>
                </li>
                <li id="actividades">
                    <a href="{{ action('ActivityController@index') }}">
                        <i class="fa fa-tasks fa-lg"></i> Actividad
                    </a>
                </li>
                <li id="usuarios">
                    <a href="{{ action('UserController@index') }}">
                        <i class="fa fa-users fa-lg"></i> Administradores
                    </a>
                </li>
                <li id="calendario">
                    <a href="{{ action('CalendarController@index') }}">
                        <i class="fa fa-calendar fa-lg"></i> Calendario
                    </a>
                </li>  
                <li id="clientes">
                    <a href="{{ action('CustomerController@index') }}">
                        <i class="fa fa-address-book fa-lg"></i> Clientes
                    </a>
                </li>
                <li id="mensajes">
                    <a href="{{ action('MessageController@index') }}">
                        <i class="fa fa-envelope fa-lg"></i> Mensajes
                    </a>
                    <span class="badge badge-pill badge-success" v-if="unansweredMessages.length > 0" v-html="unansweredMessages.length"></span>
                </li>
                <!-- Inmuebles -->
                <li id="inmuebles">
                    <a href="{{ action('PropertyController@index') }}">
                        <i class="fa fa-home fa-lg"></i> Inmuebles
                    </a>
                </li>
                <li id="comodidades">
                    <a href="{{ action('EnvironmentController@index') }}">
                        <i class="fa fa-cube fa-lg"></i> Comodidades
                    </a>
                </li>
                <li id="equipamiento">
                    <a href="{{ action('EquipmentController@index') }}">
                        <i class="fa fa-hotel fa-lg"></i> Equipamiento
                    </a>
                </li>
                <li id="exterior">
                    <a href="{{ action('ExteriorController@index') }}">
                        <i class="fa fa-sun-o fa-lg"></i> Exterior
                    </a>
                </li>
                <li id="servicios-comodidades">
                    <a href="{{ action('ServiceAmenityController@index') }}">
                        <i class="fa fa-snowflake-o fa-lg"></i> {!! __('properties.services_amenities_title') !!}
                    </a>
                </li>
                <!-- /Inmuebles -->
                <!-- Geografia -->
                <li id="paises">
                    <a href="{{ action('CountryController@index') }}">
                        <i class="fa fa-globe fa-lg"></i> Pa&iacute;ses
                    </a>
                </li>
                <li id="departamentos">
                    <a href="{{ action('DepartmentController@index') }}">
                        <i class="fa fa-building fa-lg"></i> Departamentos
                    </a>
                </li>
                <li id="localidades">
                    <a href="{{ action('LocationController@index') }}">
                        <i class="fa fa-location-arrow"></i> Localidades
                    </a>
                </li>
                <li id="zonas">
                    <a href="{{ action('ZoneController@index') }}">
                        <i class="fa fa-map-marker fa-lg"></i> Zonas
                    </a>
                </li>
                <!-- /Geografia -->
                <!-- Sitio -->
                <li id="sitio">
                    <a href="{{ action('SiteController@index') }}">
                        <i class="fa fa-keyboard-o fa-lg"></i> Administrar
                    </a>
                </li>
                <li id="oficinas">
                    <a href="{{ action('OfficeController@index') }}">
                        <i class="fa fa-building fa-lg"></i> Oficinas
                    </a>
                </li>
                <li id="servicios">
                    <a href="{{ action('ServiceController@index') }}">
                        <i class="fa fa-asterisk fa-lg"></i> Servicios
                    </a>
                </li>
                <!-- /Sitio -->
                <hr>
                <li id="configuracion">
                    <a href="{{ action('InstallationController@index') }}" style="color: #ff8888">
                        <i class="fa fa-database fa-lg"></i> Configuraci&oacute;n
                    </a>
                </li>
            </ul>
            <!-- /Menú Principal -->
     </div>
</div>

@push('js')
   $('#{{ $url }}').addClass('active');
@endpush