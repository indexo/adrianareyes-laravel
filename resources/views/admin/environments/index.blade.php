@extends('layouts.admin')

@section('title', 'Comodidades - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Comodidades</li>
@endsection

@section('content')
<p><a href="{{ action('EnvironmentController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Comodidad</a></p>
<table class="table table-bordered table-striped text-center" id="environments">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Tipo</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($environments as $e)
			<tr>
				<td>{!! $e->name !!}</td>
				<td>
					<?php
						switch($e->type) {
							case 'int':
								$type = 'N&uacute;mero';
								break;
							case 'yesno':
								$type = 'S&iacute; o No';
								break;
							default:
								$type = 'Personalizado';
						}
					?>
					{!! $type !!}
				</td>
				<td>
					<form method="POST" action="{{ action('EnvironmentController@destroy', $e->id, $e) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('EnvironmentController@edit', $e->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
						
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="3" class="text-center">No hay comodidades</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.css"/>
@endpush

@push('js_files')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-html5-1.5.1/datatables.min.js"></script>
@endpush

@push('js')
    $('#environments').DataTable({
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
        }
    });
@endpush