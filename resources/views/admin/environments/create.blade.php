@extends('layouts.admin')

@section('title', 'Comodidades - Nueva')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('EnvironmentController@index') }}">Comodidades</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nueva</li>
@endsection

@section('content')
<form method="POST" action="{{ action('EnvironmentController@store') }}" class="form col-md-6">
	{{ csrf_field() }}
	<fieldset class="form-group">
		<label for="name" class="control-label">Nombre</label>
		<input name="name" id="name" class="form-control">
	</fieldset>
	<fieldset class="form-group">
		<label for="placeholder" class="control-label">Texto Inicial</label>
		<input name="placeholder" id="placeholder" class="form-control">
	</fieldset>
	<fieldset class="form-group">
		<label for="type" class="control-label">Nombre</label>
		<select name="type" id="type" class="form-control">
			<option value="" disabled selected>Tipo de campo</option>
			<option value="int">N&uacute;mero</option>
			<option value="yesno">S&iacute; o No</option>
		</select>
	</fieldset>
	<fieldset class="form-group">
		<button class="btn btn-success"><i class="fa fa-floppy"></i> Guardar</button>
	</fieldset>
</form>
@endsection