@extends('layouts.admin')

@section('title', 'Comodadidades - Editar: ' . $environment->name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('EnvironmentController@index') }}">Comodidades</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('EnvironmentController@edit', $environment) }}">{!! $environment->name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
<form method="POST" action="{{ action('EnvironmentController@update', $environment) }}" class="form col-md-6">
	{{ csrf_field() }}
	{{ method_field('put') }}
	<fieldset class="form-group">
		<label for="name" class="control-label">Nombre</label>
		<input name="name" id="name" class="form-control" value="{{ $environment->name }}">
	</fieldset>
	<fieldset class="form-group">
		<label for="placeholder" class="control-label">Texto Inicial</label>
		<input name="placeholder" id="placeholder" class="form-control" value="{{ $environment->placeholder }}">
	</fieldset>
	<fieldset class="form-group">
		<label for="type" class="control-label">Nombre</label>
		<select name="type" id="type" class="form-control">
			<option value="" disabled>Tipo de campo</option>
			<option value="int" @if($environment->type == 'int') selected @endif>N&uacute;mero</option>
			<option value="yesno" @if($environment->type == 'yesno') selected @endif>S&iacute; o No</option>
		</select>
	</fieldset>
	<fieldset class="form-group">
		<button class="btn btn-success"><i class="fa fa-floppy"></i> Guardar</button>
	</fieldset>
</form>
@endsection