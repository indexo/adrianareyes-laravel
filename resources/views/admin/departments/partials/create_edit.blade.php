<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="name">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" @if(old('name')) value="{{ old('name') }}" @elseif(isset($department) && $department->name) value="{{ $department->name }}" @endif></input>
	</fieldset>
</div>
<div class="row">
	<fieldset class="form-group col-md-4">
		<label class="control-label" for="country_id">Pa&iacute;s</label>
		<select class="form-control" id="country_id" name="country_id">
			<option value="" @if(!old('country_id') || (isset($department) && $department->country_id)) selected @endif>Elija un pa&iacute;s</option>
			@forelse(App\Country::all() as $c)
				<option  value="{{ $c->id }}" @if(old('country_id') == $c->id) selected @elseif(isset($department) && $department->country_id == $c->id) selected @endif>{!! $c->name !!} ({!! $c->iso3 !!})</option>
			@empty
				<option value="" disabled="" selected="">No hay pa&iacute;ses disponibles</option>
			@endforelse
		</select>
	</fieldset>
</div>