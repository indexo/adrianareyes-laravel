@extends('layouts.admin')

@section('title', 'Departamentos - Editar: ' . $department->name)

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('DepartmentController@index') }}">Departamentos</a></li>
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('DepartmentController@edit', $department) }}">{!! $department->name !!}</a></li>
	<li class="breadcrumb-item active" aria-current="page">Editar</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('DepartmentController@update', $department) }}" class="form col-md-6">
		{{ csrf_field() }}
		{{ method_field('put') }}
		@include('admin.departments.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar Departamento</button>
	</form>
@endsection