@extends('layouts.admin')

@section('title', 'Departamentos - Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page">Departamentos</li>
@endsection

@section('content')
<a href="{{ action('DepartmentController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Departamento</a>
<hr>
<table class="table table-bordered text-center" id="departments">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Pa&iacute;s</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($departments as $d)
			<tr>
				<td>{{ $d->name }} </td>
				<td>{{ $d->country->name }}</td>
				<td>
					<form method="POST" action="{{ action('DepartmentController@destroy', $d) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('DepartmentController@edit', $d) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="4" class="text-center">No hay departamentos</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
	$('#departments').DataTable({
		order: [[ 2, "desc" ]],
		language: {
			url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
		}
	});
@endpush