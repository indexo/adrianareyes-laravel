@extends('layouts.admin')

@section('title', 'Departamentos - Nuevo')

@section('breadcrumb')
	<li class="breadcrumb-item" aria-current="page"><a href="{{ action('DepartmentController@index') }}">Departamentos</a></li>
	<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
@endsection

@section('content')
	<form method="POST" action="{{ action('DepartmentController@store') }}" class="form col-md-6">
		{{ csrf_field() }}
		@include('admin.departments.partials.create_edit')
		<button class="btn btn-success"><i class="fa fa-plus"></i> Crear Departamento</button>
	</form>
@endsection