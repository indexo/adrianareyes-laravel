@extends('layouts.admin')

@section('title', __('properties.services_amenities_title') .  '- Inicio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">{!! __('properties.services_amenities_title') !!}</li>
@endsection

@section('content')
<p><a href="{{ action('ServiceAmenityController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Servicio / Amenity</a></p>
<table class="table table-bordered table-striped text-center" id="services_amenities">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Tipo</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($services_amenities as $sa)
			<tr>
				<td>{!! $sa->name !!}</td>
				<td>
					<?php
						switch($sa->type) {
							case 'int':
								$type = 'N&uacute;mero';
								break;
							case 'yesno':
								$type = 'Sí o No';
								break;
							default:
								$type = 'Personalizado';
						}
					?>
					{!! $type !!}
				</td>
				<td>
					<form method="POST" action="{{ action('ServiceAmenityController@destroy', $sa->id, $sa) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<div class="btn-group" role="group" aria-label="Acciones">
							<a href="{{ action('ServiceAmenityController@edit', $sa->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
						</div>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="3" class="text-center">No hay servicios ni amenities</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection

@push('css_files')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
@endpush

@push('js_files')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
@endpush

@push('js')
    $('#services_amenities').DataTable({
    language: {
    url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
    }
    });
@endpush