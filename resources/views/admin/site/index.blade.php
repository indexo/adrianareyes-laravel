@extends('layouts.admin')

@section('title', 'Sitio')

@section('breadcrumb')
	<li class="breadcrumb-item active" aria-current="page">Sitio</li>
@endsection

@section('content')
<?php
	$site = App\Site::first();
?>
<div class="row">
	<div class="col-md-6 ">
		<div class="card">
			<div class="card-header text-center">
				<h4>Informaci&oacute;n de empresa</h4>
			</div>
			<div class="card-body ">
				<form class="form col-md-12" method="POST" action="{{ action('SiteController@company') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<fieldset class="form-group col-md">
							<label class="control-label" for="company_name">Nombre</label>
							<input class="form-control" id="company_name" name="company_name"@if(old('company_name')) value="{{ old('company_name') }}"@elseif(isset($site) && $site->company_name) value="{{ $site->company_name }}"@endif>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md">
							<label class="control-label" for="company_text">Texto</label>
							<textarea class="form-control" id="company_text" name="company_text">@if(old('company_text')){!! old('company_text') !!}@elseif(isset($site) && $site->company_text){!! $site->company_text !!}@endif</textarea>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md">
							<label class="control-label" for="logo">Logo</label>
							<input type="file" class="form-control" id="logo" name="logo">
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md text-center">
							<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar</button>
						</fieldset>
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="col text-center">
					@if(isset($site) && $site->logo)
						<img src="{{ action('SiteController@getLogo') }}" alt="Logo actual {{ $site->company_name }}">
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 ">
		<div class="card">
			<div class="card-header text-center">
				<h4>Acerca de nosotros</h4>
			</div>
			<div class="card-body ">
				<form class="form col-md-12" method="POST" action="{{ action('SiteController@about_us') }}">
					{{ csrf_field() }}
					<div class="row">
						<fieldset class="form-group col-md">
							<textarea class="form-control" id="about_us" name="about_us">@if(old('about_us')){!! old('about_us') !!}@elseif(isset($site) && $site->about_us){!! $site->about_us !!}@endif</textarea>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md text-center">
							<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar</button>
						</fieldset>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6 ">
		<div class="card">
			<div class="card-header text-center">
				<h4>Contacto</h4>
			</div>
			<div class="card-body ">
				<form class="form col-md-12" method="POST" action="{{ action('SiteController@contact_us') }}">
					{{ csrf_field() }}
					<div class="row">
						<fieldset class="form-group col-md">
							<textarea class="form-control" id="contact_us" name="contact_us">@if(old('contact_us')){!! old('contact_us') !!}@elseif(isset($site) && $site->contact_us){!! $site->contact_us !!}@endif</textarea>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md text-center">
							<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar</button>
						</fieldset>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6 ">
		<div class="card">
			<div class="card-header text-center">
				<h4>Nuevas Propiedades</h4>
			</div>
			<div class="card-body ">
				<form class="form col-md-12" method="POST" action="{{ action('SiteController@new_properties') }}">
					{{ csrf_field() }}
					<div class="row">
						<fieldset class="form-group col-md">
							<label for="new_properties_title" class="control-label">T&iacute;tulo</label>
							<input type="text" class="form-control" id="new_properties_title" name="new_properties_title"@if(old('new_properties_title')) value="{{ old('new_properties_title') }}"@elseif(isset($site) && $site->new_properties_title) value="{{ $site->new_properties_title }}"@endif>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md">
							<label for="new_properties_text" class="control-label">Descripci&oacute;n</label>
							<textarea class="form-control" id="new_properties_text" name="new_properties_text">@if(old('new_properties_text')){!! old('new_properties_text') !!}@elseif(isset($site) && $site->new_properties_text){!! $site->new_properties_text !!}@endif</textarea>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md text-center">
							<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar</button>
						</fieldset>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-header text-center">
				<h4>Imagen de Inicio</h4>
			</div>
			<div class="card-body ">
				<form class="form col-md-6" method="POST" action="{{ action('SiteController@index_image') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<fieldset class="form-group col-md">
							<label class="control-label" for="index_image">Imagen</label>
							<input type="file" class="form-control" id="index_image" name="index_image">
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="form-group col-md text-center">
							<button class="btn btn-success"><i class="fa fa-floppy-o"></i> Actualizar</button>
						</fieldset>
					</div>
				</form>
					@if(isset($site) && $site->index_image)
						<div class="col-md-6 text-center">
							<img src="{{ action('SiteController@getIndexImage') }}" alt="Logo actual {{ $site->company_name }}" width="500rem">
						</div>
					@endif
			</div>
		</div>
	</div>
</div>
@endsection


@push('js_files')
<script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
@endpush

@push('js')
    ClassicEditor
        .create( document.querySelector( '#about_us' ) )
        .catch( error => {
            console.error( error );
        } );
    ClassicEditor
        .create( document.querySelector( '#contact_us' ) )
        .catch( error => {
            console.error( error );
        } );
        ClassicEditor
        .create( document.querySelector( '#new_properties_text' ) )
        .catch( error => {
            console.error( error );
        } );
@endpush

@push('css')
	.card {
		margin-bottom: 2rem;
	}
@endpush
