<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .company-name {
        color: #0c5460;
    }
    .company-name:hover {
        color: #0c5460;
        text-decoration: none;
    }
</style>
<div class="container">
    @if(isset($site))
        <div class="row">
            <h1>
                <a href="{{ url('/') }}"><img src="{{ url('imagenes/empresa/logo') }}" alt="{{ $site->company_name }}"></a>
                &nbsp;
                <a class="company-name align-bottom" href="{{ url('/') }}">{!! $site->company_name !!}</a>
            </h1>
        </div>
        <hr>
    @endif
    <div class="row">
        <div class="col-12">
            <p class="lead">
                <strong>Estimado/a {!! $data->first_name . ' ' . $data->last_name !!}</strong>,
            </p>
            <p class="lead">
                Gracias por ponerse en contacto con nosotros. Su mensaje ha sido recibido correctamente.
            </p>
            <p class="lead">
                Tan pronto como nos sea posible, nos estaremos poniendo en contacto con usted por los medios <strong>{!! $data->phone !!}</strong> o <strong>{!! $data->email !!}</strong>.
            </p>
        </div>
    </div>
    <hr>
    @if($offices->count())
    <div class="row">
        <div class="col-12">
            <h3>Le recordamos que puede contactarse con nosotros por los siguientes medios:</h3>

            @foreach($offices as $office)
                <?php
                    $phones = explode(',', $office->phones);
                    $emails = explode(',', $office->emails);
                ?>
                <div class="row" style="margin-top: 30px">
                    <div class="col-12">
                        <p class="lead">
                            <i class="fa fa-home fa-1x"></i> {!! $office->address !!}, {!! $office->city !!}, {!! $office->country !!}
                        </p>
                        <p class="lead">
                        @forelse($phones as $phone)
                            <i class="fa fa-phone fa-1x"></i>
                            <a href="tel:{{ $phone }}">{{$phone}}</a>&nbsp;
                        @empty
                        @endforelse
                        </p>
                        <p class="lead">
                            @forelse($emails as $emal)
                                <i class="fa fa-envelope fa-1x"></i>
                                <a href="mailto:{{ $emal }}">{{$emal}}</a>&nbsp;
                            @empty
                            @endforelse
                        </p>
                        @if($office->google_map_code)
                            <p>
                                <iframe src="{{ $office->google_map_code }}" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </p>
                        @endif
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
    @endif
</div>
