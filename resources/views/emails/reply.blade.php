<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .container {
        margin-top: 30px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            {!! $data->message !!}
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <strong>Su mensaje:</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            {!! $data->parent_message->message !!}
        </div>
    </div>

    @if($data->user && $data->user->signature)
        {!! $data->user->signature !!}
    @endif
</div>
