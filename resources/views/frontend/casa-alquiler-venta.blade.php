@extends('layouts.frontend')

@section('title', $property->label())

@section('content')
    <main class="Propiedades">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    @if($property->cover_image)<a class="gallery__itemLink " href="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}" data-rel="aiLightbox"><img class="img-fluid" id="imagen-principal-galeria"  src="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}"/></a> @endif
                    <div class="gallery">
                        <div class="gallery__intro">
                            <ul>
                                @forelse($property->images as $image)
                                    <li class="gallery__item"><a class="gallery__itemLink" href="{{ url('imagenes/inmuebles/' . $image->name) }}" data-rel="aiLightbox"><img class="gallery__itemThumb" src="{{ url('imagenes/inmuebles/' . $image->name) }}"/></a></li>
                                @empty
                                    No hay im&aacute;genes
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div><!-- /col-sm-12 col-md-6   -->

                <div class="col-sm-12 col-md-6">
                    <div class="datos-propiedades">
                        <h3>{!! $property->label() !!} @include('frontend.partials.admin_button')</h3>
                        <p>{!! $property->description !!}</p>

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Datos Principales</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($property->ground_footage)
                                <tr>
                                    <th scope="row">Metraje Terreno</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->ground_footage }}"
                                                       suffix=" m2" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->built_footage)
                                <tr>
                                    <th scope="row">Metraje Construido</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->built_footage }}"
                                                       suffix=" m" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->distance_from_sea)
                                <tr>
                                    <th scope="row">Distancia al Mar</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->distance_from_sea }}"
                                                       suffix=" m" />
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <th scope="row">Codigo de Referencia</th>
                                <td><span class="badge badge-primary">#{{ $property->id }}</span></td>
                            </tr>
                            @if($property->notes)
                                <tr>
                                    <th scope="row">Observaciones</th>
                                    <td>{!! $property->notes !!}</td>
                                </tr>
                            @endif
                            </tbody>
                        </table><!-- /Datos Principales -->

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Precios</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Venta</th>
                                <td>
                                    <number-format class="badge badge-danger"
                                                   prefix="{{ $property->sale->country()->currency_symbol }}"
                                                   :value="{{ $property->sale->price }}"/>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Financiación:</th>
                                @if($property->sale->financing)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">Venta</th>
                                <td>
                                    <number-format class="badge badge-danger"
                                                   prefix="{{ $property->sale->country()->currency_symbol }}"
                                                   :value="{{ $property->sale->price }}"/>
                                </td>
                            </tr>
                            @if($property->rent->january)
                                <tr>
                                    <th scope="row">Alquiler Enero</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->january }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->january_1st_fortnight)
                                <tr>
                                    <td>1ª quincena</td>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->january_1st_fortnight }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->january_2nd_fortnight)
                                <tr>
                                    <td>2ª quincena</td>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->january_2nd_fortnight }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->february)
                                <tr>
                                    <th scope="row">Alquiler Febrero</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->february }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->february_1st_fortnight)
                                <tr>
                                    <td>1ª quincena</td>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->february_1st_fortnight }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->february_2nd_fortnight)
                                <tr>
                                    <td>2ª quincena</td>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->february_2nd_fortnight }}" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->season)
                                <tr>
                                    <th scope="row">Alquiler Temporada</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->season }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->easter)
                                <tr>
                                    <th scope="row">Alquiler Semana Santa</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->easter }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->annual)
                                <tr>
                                    <th scope="row">Alquiler Anual</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->annual }}"/>
                                    <th scope="row">Alquiler Invierno</th>
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->winter)
                                <tr>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->winter }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->high_season_daily)
                                <tr>
                                    <th scope="row">Alquiler Diario Temporada Alta</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->high_season_daily }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->rent->low_season_daily)
                                <tr>
                                    <th scope="row">Alquiler Diario Temporada Baja</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->low_season_daily }}"/>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table><!-- /Precios -->

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Gastos</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($property->expenses_property_tax)
                                <tr>
                                    <th scope="row">Contribución inmobiliaria</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->country()->currency_symbol }}"
                                                       :value="{{ $property->expenses_property_tax }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->expenses_property_tax)
                                <th scope="row">Impuesto de Primaria</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->country()->currency_symbol }}"
                                                   :value="{{ $property->expenses_property_tax }}"/>
                                </td>
                                </tr>
                            @endif
                            @if($property->expenses_gardener)
                                <th scope="row">Jardinero</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->country()->currency_symbol }}"
                                                   :value="{{ $property->expenses_gardener }}"/>
                                </td>
                                </tr>
                            @endif
                            @if($property->expenses_pool_maintenance)
                                <th scope="row">Mantinimiento Piscina</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->country()->currency_symbol }}"
                                                   :value="{{ $property->expenses_pool_maintenance }}"/>
                                </td>
                                </tr>
                            @endif
                            @if($property->expenses_alarm_vigilance)
                                <th scope="row">Alarma y Vigilancia</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->country()->currency_symbol }}"
                                                   :value="{{ $property->expenses_alarm_vigilance }}"/>
                                </td>
                                </tr>
                            @endif
                            <tr>
                            <th colspan="2">
                                <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalcontacto">Contactar</button>
                                        @include('frontend.partials.contacto-inmueble')
                            </th>
                            </tr>
                            </tbody>
                        </table><!-- /Gastos -->

                    </div><!-- /Datos-Propiedades   -->
                </div><!-- /col-sm-12 col-md-6   -->
            </div><!-- /row -->
            <div class="row">
                @if($property->environments)
                    <div class="col-sm-12 col-lg-3">
                        <div class="comodidades">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2"><h5>Comodidades</h5></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($property->environments as $name => $environment)
                                    <tr>
                                        <th scope="row">{{ $name }}</th>
                                        @if($environment->type == 'int')
                                            <td><number-format :value="{{ $environment->value }}" /></td>
                                        @elseif($environment->type == 'yesno')
                                            <td><i class="fa fa-check-circle"></i></td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr>
                                        <th scope="row" colspan="2">No hay comodidades disponibles</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /Comodidades col-sm-12 col-lg-3 -->
                @endif

                    @if($property->exteriors)
                        <div class="col-sm-12 col-lg-3">
                            <div class="exterior">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2"><h5>Exterior</h5></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($property->exteriors as $name => $exterior)
                                        <tr>
                                            <th scope="row">{{ $name }}</th>
                                            @if($exterior->type == 'int')
                                                <td><number-format :value="{{ $exterior->value }}" /></td>
                                            @elseif($exterior->type == 'yesno')
                                                <td><i class="fa fa-check-circle"></i></td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <th scope="row" colspan="2">No hay exteriores disponibles</th>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /Exterior col-sm-12 col-lg-3 -->
                    @endif

                    @if($property->equipments)
                        <div class="col-sm-12 col-lg-3">
                            <div class="equipamiento">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2"><h5>Equipamiento</h5></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($property->equipments as $name => $equipment)
                                        <tr>
                                            <th scope="row">{{ $name }}</th>
                                            @if($equipment->type == 'int')
                                                <td><number-format :value="{{ $equipment->value }}" /></td>
                                            @elseif($equipment->type == 'yesno')
                                                <td><i class="fa fa-check-circle"></i></td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <th scope="row" colspan="2">No hay equipamiento disponible</th>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /col-sm-12 col-lg-3 -->
                    @endif

                    @if($property->services_amenities)
                        <div class="col-sm-12 col-lg-3">
                            <div class="servicios-inmuebles">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="6"><h5>Servicios</h5></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($property->services_amenities as $name => $service_amenity)
                                        <tr>
                                            <th scope="row">{{ $name }}</th>
                                            @if($service_amenity->type == 'int')
                                                <td><number-format :value="{{ $service_amenity->value }}" /></td>
                                            @elseif($service_amenity->type == 'yesno')
                                                <td><i class="fa fa-check-circle"></i></td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <th scope="row" colspan="2">No hay servicios disponibles</th>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /col-sm-12 col-lg-3 -->
                    @endif
            </div>
        </div><!-- /Container   -->
    </main><!-- /Contacto -->
@endsection