@extends('layouts.frontend')

@section('title', $property->label())

@section('content')
<main class="Propiedades">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                @if($property->cover_image)<a class="gallery__itemLink " href="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}" data-rel="aiLightbox"><img class="img-fluid" id="imagen-principal-galeria"  src="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}"/></a> @endif
                <div class="gallery">
                    <div class="gallery__intro">
                        <ul>
                            @forelse($property->images as $image)
                                <li class="gallery__item"><a class="gallery__itemLink" href="{{ url('imagenes/inmuebles/' . $image->name) }}" data-rel="aiLightbox"><img class="gallery__itemThumb" src="{{ url('imagenes/inmuebles/' . $image->name) }}"/></a></li>
                            @empty
                                No hay im&aacute;genes
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div><!-- /col-sm-12 col-md-6   -->

            <div class="col-sm-12 col-md-6">
                <div class="datos-propiedades">
                    <h3>{!! $property->label() !!} @include('frontend.partials.admin_button')</h3>
                    <p>{!! $property->description !!}</p>

                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="2"><h5>Datos Principales</h5></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($property->area)
                            <tr>
                                <th scope="row">Superficie</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   :value="{{ $property->area }}"
                                                   suffix=" m" />
                                </td>
                            </tr>
                        @endif
                        @if($property->tdi->floors)
                            <tr>
                                <th scope="row">Plantas</th>
                                <td><number-format :value="{{ $property->tdi->floors }}"/></td>
                            </tr>
                        @endif
                        @if($property->tdi->low_level)
                            <tr>
                                <th scope="row">PB</th>
                                <td><number-format :value="{{ $property->tdi->low_level }}"/></td>
                            </tr>
                        @endif
                        @if($property->tdi->subsoil)
                            <tr>
                                <th scope="row">Subsuelo</th>
                                <td><number-format :value="{{ $property->tdi->subsoil }}"/></td>
                            </tr>
                        @endif
                        <tr>
                            <th scope="row">Cocina</th>
                            @if($property->tdi->kitchen)
                                <td><i class="fa fa-check-circle"></i></td>
                            @else
                                <td><i class="fa fa-times-circle"></i></td>
                            @endif
                        </tr>
                        <tr>
                            <th scope="row">Kitchenette</th>
                            @if($property->tdi->kitchenette)
                                <td><i class="fa fa-check-circle"></i></td>
                            @else
                                <td><i class="fa fa-times-circle"></i></td>
                            @endif
                        </tr>
                        @if($property->tdi->bathrooms)
                            <tr>
                                <th scope="row">Baños</th>
                                <td><number-format :value="{{ $property->tdi->bathrooms }}"/></td>
                            </tr>
                        @endif
                        <tr>
                            <th scope="row">Baño Privado</th>
                            @if($property->tdi->private)
                                <td><i class="fa fa-check-circle"></i></td>
                            @else
                                <td><i class="fa fa-times-circle"></i></td>
                            @endif
                        </tr>
                        <tr>
                            <th scope="row">Codigo de Referencia</th>
                            <td><span class="badge badge-primary">#{{ $property->reference() }}</span></td>
                        </tr>
                        @if($property->notes)
                            <tr>
                                <th scope="row">Observaciones</th>
                                <td>{!! $property->notes !!}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    <div class="table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Precios</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Venta</th>
                                <td>
                                    <number-format class="badge badge-danger"
                                                   prefix="{{ $property->sale->country()->currency_symbol }}"
                                                   :value="{{ $property->sale->price }}"/>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Financiación:</th>
                                @if($property->sale->financing)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            @if($property->rent->annual)
                            <tr>
                                <th scope="row">Alquiler Anual</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->rent->country()->currency_symbol }}"
                                                   :value="{{ $property->rent->annual }}"/>
                                </td>
                            </tr>
                            @endif

                            @if($property->rent->temporary)
                                <tr>
                                    <th scope="row">Alquiler Temporal</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->rent->country()->currency_symbol }}"
                                                       :value="{{ $property->rent->temporary }}"/>
                                    </td>
                                </tr>
                            @endif

                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="3"><h5>Gastos</h5></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($property->expenses)
                                <tr>
                                    <th scope="row">Expensas ({!! title_case($property->expenses_range) !!})</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->country()->currency_symbol }}"
                                                       :value="{{ $property->expenses }}"/>
                                    </td>
                                    <td>{{ title_case($property->expenses_range) }}</td>
                                </tr>
                                @endif
                                @if($property->expenses_property_tax)
                                    <tr>
                                        <th scope="row">Contribución inmobiliaria</th>
                                        <td>
                                            <number-format class="badge badge-primary"
                                                           prefix="{{ $property->country()->currency_symbol }}"
                                                           :value="{{ $property->expenses_property_tax }}"/>
                                        </td>
                                    </tr>
                                @endif
                                @if($property->expenses_property_tax)
                                    <th scope="row">Impuesto de Primaria</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->country()->currency_symbol }}"
                                                       :value="{{ $property->expenses_property_tax }}"/>
                                    </td>
                                    </tr>
                                @endif


                                <th colspan="3">
                                    <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalcontacto">Contactar</button>
                                    @include('frontend.partials.contacto-inmueble')
                                </th>
                                </tr>
                                </tbody>
                            </table><!-- /Gastos -->
                            </tbody>
                        </table>
                    </div>
                </div><!-- /Datos-Propiedades   -->
            </div><!-- /col-sm-12 col-md-6 -->

        </div><!-- /Container   -->
</main><!-- /Contacto -->
@endsection