<div class="portada parallax-window"
     data-parallax="scroll"
     data-image-src="{{ action('SiteController@getIndexImage') }}">
 <div class="container-fluid">
  <div class="row d-flex align-items-start">
  	<!-- Buscador -->
	@include('frontend.partials.buscador')
	<!-- /Buscador -->
	<!-- Introducción -->
	<div id="introduccion" class="d-none d-lg-block col-lg-6 text-right">
	 <h4>El asesoramiento que necesitas, personalizado y a tu medida a tan solo unos clics de distancia </h4>
		<a class="btn btn-danger"
           href="{{ action('WebController@contact_us') }}"
        >Contactanos!</a>
	</div><!-- Introducción -->	
  </div><!-- /row -->
</div><!-- /container-fluid	 -->
</div><!-- /Portada -->
