<div class="ultimos-inmuebles">
    <div class="container">
        <div class="row d-flex justify-content-around">
            <div class="d-flex justify-content-center">
                <h3>Inmuebles Destacados</h3>
            </div>
            <div class="card-deck">
                @forelse($properties as $property)
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card">

                                <img class="card-img-top"
                                 src="@if($property->cover_image){{ url('/imagenes/inmuebles/' . $property->cover_image->name) }}@else{{ url('imagenes/empresa/logo') }}@endif"
                                 alt="{{ $property->label() }}"
                                 style="height: 200px;">
                                 
                                <div class="card-img-overlay">

                                  <div class="badge badge-primary" id="status-card">{!! $property->status !!}</div><!-- /status-card -->

                                  <div class="badge badge-primary" id="price-card">{!! $property->price !!}</div><!-- /price-card -->

                                </div><!-- /card-img-overlay   --> 

                            <div class="card-body">
                                <h4 class="card-title">{!! $property->label() !!}</h4>
                                <ul class="list-inline">
                                    <li class="list-inline-item">Dormitorios<span class="badge badge-primary">{{ $property->rooms }}</span></li>
                                    <li class="list-inline-item">Baños<span class="badge badge-primary">{{ $property->bathrooms }}</span></li>
                                    <li class="list-inline-item">Metraje<span class="badge badge-primary">{{ $property->built_footage }} m2</span></li>
                                </ul>
                            </div>
                            <div class="card-footer">
                                <div class="float-right"><a class="btn btn-info btn-sm" href="{{ action('WebController@showProperty', $property->reference()) }}">Ver Detalles</a>
                                </div>
                            </div><!-- /card-footer -->
                        </div><!-- /Card -->
                    </div>
                @empty
                    <h1>No hay inmuebles destacados</h1>
                @endforelse
            </div><!-- /card-deck -->
        </div><!-- /Row -->

        <div class="row d-flex justify-content-center pt-4">
            <a href="{{ action('WebController@properties') }}"
               class="btn btn-info"
            >Ver Mas...</a>
        </div><!-- /row d-flex justify-content-center -->

    </div><!-- /Container -->
</div><!-- /Ultimos Inmuebles -->