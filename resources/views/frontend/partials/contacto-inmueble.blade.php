<!-- Modal Contacto -->
<div class="modal fade" id="modalcontacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <form method="POST"
                      action="{{ action('MessageController@store', [ 'property_id' => $property ]) }}"
                      id="contact-us">
                {{ csrf_field() }}
                    <input type="hidden" id="g-recaptcha-response" name="_recaptcha">

                <!-- Numero de Referencia -->
                    <div class="form-group row">
                        <label for="codigodereferencia" class="col-sm-3 col-form-label col-form-label-sm">Referencia</label>
                        <div class="col-sm-4">
                            <input type="email" class="form-control form-control-sm" id="codigodereferencia" placeholder="#{{ $property->reference() }}" disabled>
                        </div>
                    </div><!-- /form-group row -->

                    <!-- Nombre -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">person</i></span>
                            <input type="text"
                                   class="form-control form-control-sm"
                                   id="nombre"
                                   name="first_name"
                                   aria-describedby="Nombre"
                                   value="{{ old('first_name') }}"
                                   placeholder="Nombre">
                        </div><!-- /form-group -->
                    </div><!-- /Nombre -->

                    <!-- Apellido -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">person</i></span>
                            <input type="text"
                                   class="form-control form-control-sm"
                                   id="apellido"
                                   name="last_name"
                                   aria-describedby="Apellido"
                                   value="{{ old('last_name') }}"
                                   placeholder="Apellido">
                        </div><!-- /form-group -->
                    </div><!-- /Apellido -->

                    <!-- Email -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">email</i></span>
                            <input type="email"
                                   class="form-control form-control-sm"
                                   id="email"
                                   name="email"
                                   aria-describedby="Email"
                                   value="{{ old('email') }}"
                                   placeholder="Email">
                        </div><!-- /form-group -->
                    </div><!-- /Email -->

                    <!-- Telefono -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">phone</i></span>
                            <input type="tel"
                                   class="form-control form-control-sm"
                                   id="telefono"
                                   name="phone"
                                   aria-describedby="Telefono"
                                   value="{{ old('phone') }}"
                                   placeholder="Telefono">
                        </div><!-- /form-group -->
                    </div><!-- /Telefono -->

                    <!-- Mensaje -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">create</i></span>
                            <textarea class="form-control form-control-sm"
                                      id="mensaje"
                                      name="message"
                                      rows="3"
                            >{{ old('message') }}</textarea>
                        </div><!-- /form-group -->
                    </div><!-- /Mensaje -->
                </form><!-- /Formulario   -->

            </div><!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg btn-block" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-success btn-lg btn-block" value="Enviar">
            </div><!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div><!-- /Modal Contacto -->

@push('js')
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6Ld-rl4UAAAAAHtf7wZazlNpl4bnd4k54GGcdhko', {action: 'contact'})
                .then(function(token) {
                    $('#g-recaptcha-response').val(token);
                });
        });
        $('input[type=submit]').on('click',function() {
            var error = false,
                mensaje = '<ul>';

            if($('#nombre').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El nombre es obligatorio.</li>'
            }

            if($('#apellido').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El apellido es obligatorio.</li>'
            }

            if($('#email').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El email es obligatorio.</li>'
            }

            if($('#mensaje').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El mensaje es obligatorio.</li>'
            }

            mensaje += '</ul>';

            if(error) {
                swal(
                    'Error',
                    mensaje,
                    'error'
                );
            } else {
                $('#contact-us').submit();
            }
        });
    </script>
@endpush