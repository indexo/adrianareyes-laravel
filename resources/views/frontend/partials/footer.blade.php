   <?php
    $site = App\Site::first();
    $office = App\Office::where('is_main', 1)->first();
   ?>
   <footer>
     <div class="container-fluid">
        <div class="row">
          <div class="d-none d-sm-block col-md-6 col-lg-4">
            <div class="footer-informacion">
              <h5>Inmobiliaria Registrada en:</h5>
             <img src="{{ asset('public/themes/default/img/banner_mintur.jpg') }}"
                  class="img-fluid"
                  alt="MINTUR">
            </div><!-- /footer-informacion  -->   
          </div><!-- /d-none d-sm-block col-md-6 col-lg-4  -->

          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="footer-informacion">
             <h5>Miembros de:</h5>
             <img src="{{ asset('public/themes/default/img/camara-inmobiliaria.jpg') }}"
                  class="img-fluid"
                  alt="Camara Inmobiliaria">
            </div><!-- /footer-informacion  -->   
          </div><!-- /col-sm-12 col-md-6 col-lg-4 -->
        @if(isset($office))
          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="footer-contacto">
             <h5>Contacto</h5>
             <ul>
               <li>
                  <strong><i class="material-icons">call</i> Telefonos:</strong> &nbsp; {!! $office->phonify() !!}
                </li>
               <li>
                <strong><i class="material-icons">mail_outline</i> Emails:</strong> &nbsp; {!! $office->emailify() !!}
              </li>
               <li><strong><i class="material-icons">location_on</i> Dirección:</strong> &nbsp; {{ $office->address }} {{ @$office->city }} - {{ $office->country }}</li>
               <li><strong><i class="material-icons">query_builder</i> Horario:</strong> &nbsp; {!! $office->schedules !!}</li>
             </ul><!-- /lista Contacto -->
            </div><!-- /footer-informacion  -->   
          </div><!-- /col-sm-12 col-md-6 col-lg-4 -->
        @endif

        </div><!-- /Row -->
            <div class="footer-copyright text-center">
              <p>Copyright © Adriana Reyes Propiedades 2007 - 2018 - <a href="{{ action('WebController@admin') }}">Administraci&oacute;n</a></p>
            </div><!-- /footer-copyright -->
     </div><!-- /container-fluid  -->  
  

    <!-- Script`s -->
        <!-- JQuery -->
        <!-- Popper -->
        <!-- Bootstrap -->
        <!-- Font Awesome -->
        <script src="{{ asset('public/js/app.js') }}"></script>
        <!-- Main -->
        <script src="{{ asset('public/themes/default/js/main.js') }}"
                type="text/javascript"></script><!-- /Main -->
        <!-- Parallax -->
        <script src="{{ asset('public/themes/default/js/parallax.min.js') }}"
                type="text/javascript"></script><!-- /Parallax -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.js"></script>

        @stack('js_files')
        @stack('js')
    <!-- /Script`s -->
    </footer><!-- /Footer  -->
  </body>
</html>