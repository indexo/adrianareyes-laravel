<?php
$office = App\Office::where('is_main', true)->first();
if($office) {
    $address = $office->google_map_code;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="formulario-contacto">
                <h3>Formulario de Contacto</h3>
                <p>
                    Ingrese sus datos y nos comunicaremos con usted a la brevedad. Gracias.
                </p>
                <form method="POST"
                      action="{{ action('MessageController@store') }}"
                      id="contact-us">
                {{ csrf_field() }}
                    <input type="hidden" id="g-recaptcha-response" name="_recaptcha">
                <!-- Nombre -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">person</i></span>
                            <input type="text"
                                   class="form-control"
                                   id="nombre"
                                   name="first_name"
                                   aria-describedby="Nombre"
                                   value="{{ old('first_name') }}"
                                   placeholder="Nombre">
                        </div><!-- /form-group -->
                    </div><!-- /Nombre -->

                    <!-- Apellido -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">person</i></span>
                            <input type="text"
                                   class="form-control"
                                   id="apellido"
                                   name="last_name"
                                   aria-describedby="Apellido"
                                   value="{{ old('last_name') }}"
                                   placeholder="Apellido">
                        </div><!-- /form-group -->
                    </div><!-- /Apellido -->

                    <!-- Email -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">email</i></span>
                            <input type="email"
                                   class="form-control"
                                   id="email"
                                   name="email"
                                   aria-describedby="Email"
                                   value="{{ old('email') }}"
                                   placeholder="Email">
                        </div><!-- /form-group -->
                    </div><!-- /Email -->

                    <!-- Telefono -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">phone</i></span>
                            <input type="tel"
                                   class="form-control"
                                   id="telefono"
                                   name="phone"
                                   aria-describedby="Telefono"
                                   value="{{ old('phone') }}"
                                   placeholder="Telefono">
                        </div><!-- /form-group -->
                    </div><!-- /Telefono -->

                    <!-- Mensaje -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="material-icons">create</i></span>
                            <textarea class="form-control"
                                      id="mensaje"
                                      name="message"
                                      rows="3"
                            >{{ old('message') }}</textarea>
                        </div><!-- /form-group -->
                    </div><!-- /Mensaje -->

                    <!-- Boton Enviar -->
                    <div class="form-group">
                        <input type="submit" class="btn btn-info btn-lg btn-block" value="Enviar">
                    </div><!-- /form-group -->

                </form><!-- /Formulario	 -->
            </div><!-- /Formulario Contacto -->
        </div><!-- /col-sm-12 col-md-6 -->

        @if(isset($office))
            <div class="col-sm-12 col-md-6">
                <div class="ubicacion">
                    <h3>Ubicación</h3>
                    <p><strong>Dirección:</strong> {!! $office->address !!}</p>
                    <iframe src="{{ $office->google_map_code }}"
                            width="500"
                            height="400"
                            frameborder="0"
                            style="border:0"
                            allowfullscreen
                    ></iframe>
                </div><!-- /Ubicacion -->
            </div><!-- /col-sm-12 col-md-6	 -->
        @endif
    </div><!-- /Row -->
</div><!-- /Container Fluid -->

@push('js')
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6Ld-rl4UAAAAAHtf7wZazlNpl4bnd4k54GGcdhko', {action: 'contact'})
                .then(function(token) {
                    $('#g-recaptcha-response').val(token);
                });
        });
        $('input[type=submit]').on('click',function() {
            var error = false,
                mensaje = '<ul>';

            if($('#nombre').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El nombre es obligatorio.</li>'
            }

            if($('#apellido').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El apellido es obligatorio.</li>'
            }

            if($('#email').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El email es obligatorio.</li>'
            }

            if($('#mensaje').val().trim().length < 1) {
                error = true;
                mensaje += '<li>El mensaje es obligatorio.</li>'
            }

            mensaje += '</ul>';

            if(error) {
                swal(
                    'Error',
                    mensaje,
                    'error'
                );
            } else {
                $('#contact-us').submit();
            }
        });
    </script>
@endpush