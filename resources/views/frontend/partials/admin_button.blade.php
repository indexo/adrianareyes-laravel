@if(Auth::check())
    <!-- Botón visible sólo para administradores -->
    <a href="{{ action('PropertyController@edit', $property) }}"
       class="btn btn-primary btn-sm">
        <i class="fa fa-pencil"></i> Editar Inmueble
    </a>
@endif