<?php
$site = App\Site::first();
?>
<!doctype html>
<html class="no-js" lang="es">
<head>
    <title>{!! isset($site) ? $site->company_name : env('APP_NAME') !!}</title>
    <meta charset="utf-8">
    <meta name="author"
          content="Indexo">
    <meta name="description"
          content="{!! @$site->company_text !!}">
    <!-- Links -->
    <link href="{{ asset('public/css/app.css') }}"
          rel="stylesheet">
    <link rel="stylesheet"
          type="text/css"
          href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ url('https://fonts.googleapis.com/icon?family=Material+Icons') }}"
          rel="stylesheet">
    <link rel="stylesheet"
          type="text/css"
          href="{{ asset('public/themes/default/css/style.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.css"
          rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/favicon.ico/') }}" type="image/x-icon">
    <script src='https://www.google.com/recaptcha/api.js?render=6Ld-rl4UAAAAAHtf7wZazlNpl4bnd4k54GGcdhko'></script>
@stack('css_files')
<!-- /Links -->
@stack('css')
@stack('head_js')


</head>
<body>
<!-- Header -->
<header>
    <!-- Menu -->
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar sticky-top navbar-dark bg-dark">
        <!-- Logo -->
        <div class="logo">
            <img src="{{ action('SiteController@getLogo') }}" alt="{{ @$site->company_name }}">
        </div><!-- /Logo -->
        <!-- Toggle Button -->
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button><!-- /Toggle Button -->
        <div class="collapse navbar-collapse justify-content-end"
             id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ action('WebController@index') }}"
                    >Inicio</a>
                </li><!-- /inicio -->
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ action('WebController@about_us') }}"
                    >Nosotros</a>
                </li><!-- /nosotros -->
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ action('WebController@properties') }}"
                    >Inmuebles</a>
                </li><!-- /inmuebles-->
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ action('WebController@contact_us') }}"
                    >Contacto</a>
                </li><!-- /contacto -->

            </ul>
        </div>
    </nav><!-- /navbar -->
</header><!-- /header -->
