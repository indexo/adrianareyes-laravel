<?php
$action = explode("/", request()->url());
$action = $action[(count($action) - 1)];
?>
<!-- Formulario de Busqueda -->
<div class="formulario col-sm-12 col-lg-6">
	<!-- Formulario -->
	<form method="GET"
		  action="{{ action('WebController@properties') }}">
		<div class="form-row">
			<!-- Tipo de Operación -->
			<div class="form-group col-12 col-xl-6">
				<select class="form-control"
						name="tdo"
						v-model="defaultTDO">
					<option value="">Tipo de Operación</option>
					<option value="rent"
							v-if="totalRents > 0"
					>Alquiler</option>
					<option value="sale"
							v-if="totalSales > 0"
					>Venta</option>
				</select>
			</div><!-- /Tipo de Operación -->
			<!-- Tipo de Inmueble -->
			<div class="form-group col-md-12 col-xl-6">
				<select class="form-control"
						name="tdi"
						v-model="defaultTDI">
					<option value="">Tipo de Inmueble</option>
					<option value="apartment"
							v-if="totalApartments > 0"
					>Apartamento</option>
					<option value="house"
							v-if="totalHouses > 0"
					>Casa</option>
					<option value="countryside"
							v-if="totalCountrysides > 0"
					>Chacra / Campo</option>
					<option value="business_premise"
							v-if="totalBusinessPremises > 0"
					>Local Comercial</option>
					<option value="land"
							v-if="totalLands > 0"
					>Terreno</option>
				</select>
			</div>
			<!-- /Tipo de Inmueble -->
		</div><!-- /form-row -->

		<div class="form-row">
			<!-- Localidad -->
			<div class="form-group col-md-12 col-xl-6">
				<select 
					class="form-control" 
					name="location"
					v-model="defaultLocation">
					<option value="">Localidad</option>
					<option v-if="localities.length > 0"
							v-for="location in localities"
							:value="location.id"
					>@{{ location.name }}</option>
					<option v-else
							value=""
							disabled
					>No hay localidades disponibles</option>
				</select>
			</div><!-- /Localidad -->
			<!-- Desde -->
			<div class="form-group col-12 col-sm-6 col-xl-3">
                <input type="number"
                       class="form-control"
                       name="from"
                       min="0"
                       step="100"
                       placeholder="Desde"
					   v-model="defaultFrom">
			</div><!-- /Desde -->
			<!-- Hasta -->
			<div class="form-group col-12 col-sm-6 col-xl-3">
                <input type="number"
                       class="form-control"
                       name="to"
                       min="0"
                       step="100"
                       placeholder="Hasta"
					   v-model="defaultTo">
			</div><!-- /Hasta -->
		</div><!-- /form-row -->

		<div class="form-row">
			<!-- Dormitorios -->
			<div class="form-group col-sm-12 col-md-4">
				<select class="form-control"
						name="rooms"
						v-model="defaultRooms">
					<option value="">Dormitorios</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>6</option>
					<option>7</option>
					<option>8</option>
				</select>
			</div><!-- /Dormitorios -->
			<!-- Baños -->
			<div class="form-group col-12 col-md-4">
				<select class="form-control"
						name="bathrooms"
						v-model="defaultBathrooms">
					<option value="">Baños</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
			</div><!-- /Baños -->
			<!-- Moneda -->
			<div class="form-group col-12 col-md-4">
				<select class="form-control"
						value="currency"
						name="currency"
						v-model="defaultCurrency">
					<option value="">Moneda</option>
					@forelse(App\Country::all() as $country)
                    <option value="{{ strtolower($country->id) }}">{!! $country->currency !!}</option>
                    @empty
                        <option value="" disabled>No hay monedas disponibles</option>
                    @endforelse
				</select>
			</div><!-- /Moneda -->
		</div><!-- /form-row -->

		<div class="form-row">
			<button class="btn btn-success btn-lg btn-block">Buscar</button>
		</div>
	</form><!-- /Formulario -->
</div><!-- /Formulario de Busqueda -->