<?php 
	$site = App\Site::first(); 
	$services = App\Service::all();
?>
<div class="servicios">
	@if(isset($site) && isset($services))
	<div class="container">
	  <div class="row">
	  	<!-- Nuestros Servicios -->
		<div class="col-sm-12">
		 <h2>Nuestros Servicios</h2>	
		 <p>{!! $site->services !!}</p>
		</div><!-- /Nuestros Servicios -->

		<!--  Servicios -->
		<div class="col-sm-12 col-lg-6">
			@foreach($services as $service)
			<h3><i class="fa {{ $service->icon }}"></i> {!! $service->title !!}</h3>
			<p>{!! $service->description !!}</p>
			@endforeach
		</div><!--  /Servicios -->
		<!-- Imagen Servicios -->
		<div class="col-sm-12 col-lg-6">
			<img src="/public/themes/default/img/servicios.png" alt="Servicios">
		</div>
		<!-- /Imagen Servicios -->

		<div class="col-sm-12">
			<div class="multiservicios">
				<h4>¡Nuevo!</h4>
				<h3><i class="fas fa-info-circle"></i>Multiservicios para alquileres directos</h3>
				<p>Con la tecnología al servicio tanto de propietarios como inquilinos, hoy en día es muy fácil llegar acuerdos, pero nada es tan simple ni tan sencillo, la experiencia lo ha demostrado. A la hora de hacer un alquiler con tranquilidad y seguridad para ambas partes y en todos los aspectos, son necesarios algunos detalles, como el pago correcto de los impuestos correspondientes, el pre chequeo de que todo funcione y esté en condiciones, la solución de cualquier inconveniente durante la ocupación, el proveer de artículos necesarios para el bienestar de los ocupantes, el posterior chequeo para constatar que todo esté en orden, el cobro y pago de consumos, limpieza de la propiedad, lavado de blanquería, etc… Nosotros cubrimos parcial o totalmente las necesidades surgidas de este tipo de transacciones, según las necesidades de cada uno.</p>
			</div>
		</div>
		<!-- Multiservicios -->
	  </div><!-- /Row -->
	</div><!-- /Container -->
        @else
            <h3>No hay servicios</h3>
        @endif
</div><!-- /Servicios -->