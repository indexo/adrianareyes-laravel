@extends('layouts.frontend')

@section('title', 'Inmuebles')

@section('content')
    <main class="contenido inmuebles">
        <div class="container-fluid">
            <div class="portada parallax-window" data-parallax="scroll" data-image-src="https://adrianareyespropiedades.com/public/themes/default/img/inmuebles.jpg">
                <div class="row">
                    <!-- Buscador -->
                    @include('frontend.partials.buscador')
                    <!-- /Buscador -->
                    <!-- Introducción -->
                    <div id="introduccion" class="d-none d-xl-block col-lg-6 text-center">
                        <h4>El asesoramiento que necesitas, personalizado y a tu medida a tan solo unos clics de distancia</4>
                        <a class="btn btn-danger" href="{{ action('WebController@contact_us') }}">Contactanos!</a>
                    </div><!-- /Introducción -->
                </div><!-- /.row -->
            </div><!-- /.portada parallax-window -->
            <!-- Últimos inmuebles -->
            <div class="ultimos-inmuebles">
                <div class="container">
                    <div class="row d-flex justify-content-around">
                        <div class="d-flex justify-content-center">
                            <h3>Inmuebles Destacados</h3>
                        </div>
                        <div class="card-deck">
                            @forelse($properties as $property)
                                <div class="col-sm-12 col-md-6 col-lg-3">
                                    <div class="card">
                                        <img class="card-img-top" src="@if($property->cover_image){{ url('/imagenes/inmuebles/' . $property->cover_image->name) }}@else{{ url('imagenes/empresa/logo') }}@endif" alt="{{ $property->label() }}" style="height: 200px;">
                                        <div class="card-img-overlay">
                                         <div class="badge badge-primary" id="status-card">{!! $property->status !!}</div><!-- /status-card -->
                                         <div class="badge badge-primary" id="price-card">{!! $property->price !!}</div><!-- /price-card -->
                                        </div><!-- /card-img-overlay   --> 
                                        <div class="card-body">
                                            <h4 class="card-title">{!! $property->label() !!}</h4>
                                            <ul>
                                                @if($property->env && isset($property->env['Dormitorios']))
                                                <li class="list-inline-item">Dormitorios<span class="badge badge-primary">{{ $property->env['Dormitorios'] }}</span></li>
                                                @endif

                                                @if($property->env && isset($property->env['Baños']))
                                                <li class="list-inline-item">Baños<span class="badge badge-primary">{{ $property->env['Baños'] }}</span></li>
                                                @endif 

                                                <li class="list-inline-item">Metraje<span class="badge badge-primary">{{ $property->built_footage }} m2</span></li>
                                                
                                            </ul>
                                        </div>
                                        <div class="card-footer">
                                         <div class="float-right"><a class="btn btn-info btn-sm" href="{{ action('WebController@showProperty', $property->reference()) }}">Ver Detalles</a>
                                       </div> 
                            </div><!-- /card-footer -->
                                    </div><!-- /Card -->
                                </div>
                            @empty
                                <h1>No se encontraron inmuebles</h1>
                            @endforelse
                        </div><!-- /card-deck -->
                    </div>
                    <!-- /Row -->
                    @if($properties->total() > $properties->perPage())
                    <!-- Paginación -->
                    <?php
                        $from = $properties->firstItem() > 0
                            ? 1
                            : $properties->currentPage() - 2;
                        $to = $properties->currentPage()+2 > $properties->lastPage()
                            ? $properties->lastPage()
                            : $properties->currentPage() + 2;

                        $links = '';
                        
                        $params = array_filter(request()->except('page'));
                        $url = url()->current();
                        foreach(array_keys($params) as $i => $k)
                        {
                            if($i == 0) {
                                $url .= '?' . $k . '=' . $params[$k];
                            } else {
                                $url .= '&' . $k . '=' . $params[$k];
                            }
                        }
                        if(count($params) > 0) {
                            $url .= '&page=';
                        } else {
                            $url .= '?page=';
                        }

                        $page = function($number, $active, $url) {
                            $class = "page-item";
                            if($active) {
                                $class .= " active";
                            }
                            return "<li class=\"{$class}\">\n<a class=\"page-link\" href=\"".$url.$number."\">{$number}</a>\n</li>\n";
                        };

                        for($i = $from; $i <= $to; $i++) {
                            if($properties->currentPage() === $i) {
                                $links .= $page($i, true, $url);
                            } else {
                                $links .= $page($i, false, $url);
                            }
                        }
                    ?>
                    <div class="row d-flex justify-content-center pt-4">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item @if($properties->currentPage() === $from || $properties->currentPage() === $properties->firstItem())disabled @endif">
                                    <a class="page-link"
                                       href="{{ $url . ($properties->currentPage()-1) }}"
                                    >Anterior</a></li>
                                {!! $links !!}
                                <li class="page-item @if($properties->currentPage() == $properties->total() || $properties->currentPage() === $properties->lastPage())disabled @endif">
                                    <a class="page-link"
                                       href="{{ $url . ($properties->currentPage()+1) }}"
                                    >Siguiente</a></li>
                            </ul>
                        </nav>
                        <!-- /Paginacion -->
                        @endif
                    </div>
                    <!-- /row d-flex justify-content-center -->
                </div>
                <!-- /Container -->
            </div>
            <!-- /Ultimos Inmuebles -->
        </div><!-- /.container-fluid -->
    </main><!-- /.main -->
@endsection