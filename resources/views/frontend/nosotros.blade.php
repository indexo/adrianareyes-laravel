@extends('layouts.frontend')

@section('title', 'Nosotros')

<?php
	$site = App\Site::first();
?>

@section('content')
<main class="contenido servicios">
 <div class="container">
 	{!! @$site->about_us !!}
 </div><!-- /container -->	
</main><!-- /main -->
@endsection