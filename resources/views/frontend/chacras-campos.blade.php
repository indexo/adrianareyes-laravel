@extends('layouts.frontend')

@section('title', $property->label())

@section('content')
    <main class="Propiedades">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    @if($property->cover_image)<a class="gallery__itemLink " href="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}" data-rel="aiLightbox"><img class="img-fluid" id="imagen-principal-galeria"  src="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}"/></a> @endif
                    <div class="gallery">
                        <div class="gallery__intro">
                            <ul>
                                @forelse($property->images as $image)
                                    <li class="gallery__item"><a class="gallery__itemLink" href="{{ url('imagenes/inmuebles/' . $image->name) }}" data-rel="aiLightbox"><img class="gallery__itemThumb" src="{{ url('imagenes/inmuebles/' . $image->name) }}"/></a></li>
                                @empty
                                    No hay im&aacute;genes
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div><!-- /col-sm-12 col-md-6   -->

                <div class="col-sm-12 col-md-6">
                    <div class="datos-propiedades">
                        <h3>{!! $property->label() !!} @include('frontend.partials.admin_button')</h3>
                        <p>{!! $property->description !!}</p>

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Datos Principales</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($property->area)
                            <tr>
                                <th scope="row">Superficie</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   :value="{{ $property->area }}" suffix=" m2"/>
                                </td>
                            </tr>
                            @endif
                            @if($property->countryside->location)
                            <tr>
                                <th scope="row">Ubicación</th>
                                <td>{!! $property->countryside->location !!}</td>
                            </tr>
                            @endif
                            @if($property->countryside->access)
                            <tr>
                                <th scope="row">Accesos</th>
                                <td>{!! $property->countryside->access !!}</td>
                            </tr>
                            @endif
                            @if($property->countryside->topography)
                            <tr>
                                <th scope="row">Topografia</th>
                                <td>{!! $property->countryside->topography !!}</td>
                            </tr>
                            @endif
                            @if($property->countryside->productivity)
                            <tr>
                                <th scope="row">Productividad</th>
                                <td>{!! $property->countryside->productivity !!}</td>
                            </tr>
                            @endif
                            <tr>
                                <th scope="row">Tajamar</th>
                                @if($property->countryside->spec_cutwater)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">Alambrados</th>
                                @if($property->countryside->spec_wiring)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">R&iacute;os - Arroyos</th>
                                @if($property->countryside->spec_rivers_streams)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">Montes</th>
                                @if($property->countryside->hills)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">Luz</th>
                                @if($property->countryside->light)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">C&oacute;digo de Referencia</th>
                                <td><span class="badge badge-primary">#{{ $property->reference() }}</span></td>
                            </tr>
                            @if($property->notes)
                                <tr>
                                    <th scope="row">Observaciones</th>
                                    <td>{!! $property->notes !!}</td>
                                </tr>
                            @endif
                            </tbody>
                        </table><!-- /Datos Principales -->

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Precios</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Venta</th>
                                <td>
                                    <number-format class="badge badge-danger"
                                                   prefix="{{ $property->sale->country()->currency_symbol }}"
                                                   :value="{{ $property->sale->price }}"/>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Financiaci&oacute;n:</th>
                                @if($property->sale->financing)
                                    <td><i class="fa fa-check-circle"></i></td>
                                @else
                                    <td><i class="fa fa-times-circle"></i></td>
                                @endif
                            </tr>
                            </tbody>
                        </table><!-- /Precios -->

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Gastos</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($property->expenses_property_tax)
                                <tr>
                                    <th scope="row">Contribución inmobiliaria</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       prefix="{{ $property->country()->currency_symbol }}"
                                                       :value="{{ $property->expenses_property_tax }}"/>
                                    </td>
                                </tr>
                            @endif
                            @if($property->expenses_property_tax)
                                <th scope="row">Impuesto de Primaria</th>
                                <td>
                                    <number-format class="badge badge-primary"
                                                   prefix="{{ $property->country()->currency_symbol }}"
                                                   :value="{{ $property->expenses_property_tax }}"/>
                                </td>
                                </tr>
                            @endif
                            <th colspan="2">
                                <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalcontacto">Contactar</button>
                                        @include('frontend.partials.contacto-inmueble')
                            </th>
                            </tr>
                            </tbody>
                        </table><!-- /Gastos -->

                    </div><!-- /Datos-Propiedades   -->
                </div><!-- /col-sm-12 col-md-6   -->
            </div><!-- /row -->

            <div class="construcciones">
                <div class="row titulo">
                    <div class="col-sm-12 text-center">
                        <h5>Construcciones</h5>
                    </div>
                </div>

                @if($property->countryside->house)
                <div class="row">
                    <!-- Casa -->
                    <div class="col-sm-12 col-lg-6">
                        <h5 class="text-center" style="background-color: #3b4957; padding: .50rem;
color: #fff;">Casa</h5>
                        <div class="row">
                            @if($property->environments)
                                <div class="col-sm-12 col-lg-3">
                                    <div class="comodidades">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2"><h5>Comodidades</h5></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($property->environments as $name => $environment)
                                                <tr>
                                                    <th scope="row">{{ $name }}</th>
                                                    @if($environment->type == 'int')
                                                        <td><number-format :value="{{ $environment->value }}" /></td>
                                                    @elseif($environment->type == 'yesno')
                                                        <td><i class="fa fa-check-circle"></i></td>
                                                    @endif
                                                </tr>
                                            @empty
                                                <tr>
                                                    <th scope="row" colspan="2">No hay comodidades disponibles</th>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- /Comodidades col-sm-12 col-lg-3 -->
                            @endif
                            @if($property->exteriors)
                                <div class="col-sm-12 col-lg-3">
                                    <div class="exterior">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2"><h5>Exterior</h5></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($property->exteriors as $name => $exterior)
                                                <tr>
                                                    <th scope="row">{{ $name }}</th>
                                                    @if($exterior->type == 'int')
                                                        <td><number-format :value="{{ $exterior->value }}" /></td>
                                                    @elseif($exterior->type == 'yesno')
                                                        <td><i class="fa fa-check-circle"></i></td>
                                                    @endif
                                                </tr>
                                            @empty
                                                <tr>
                                                    <th scope="row" colspan="2">No hay exteriores disponibles</th>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- /Exterior col-sm-12 col-lg-3 -->
                        @endif

                    <!-- /Casa -->

                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <!-- Galpones -->
                            <div class="col-sm-12">
                                <h5 class="text-center" style="background-color: #3b4957; padding: .50rem;
color: #fff;">Galpones</h5>
                                @if($property->countryside->sheds)
                                {!! $property->countryside->sheds_description !!}
                                @else
                                    <p>No tiene galpones</p>
                                @endif
                            </div><!-- /col-sm-12-->
                            <!-- /Galpones -->
                        </div><!-- /Row -->

                        <div class="row">
                            <!-- Caballerizas -->
                            <div class="col-sm-12">
                                <h5 class="text-center" style="background-color: #3b4957; padding: .50rem;
color: #fff;">Caballerizas</h5>
                                @if($property->countryside->stables)
                                    {!! $property->countryside->stables_description !!}
                                @else
                                    <p>No tiene cabellerizas</p>
                                @endif
                            </div><!-- /col-sm-12 -->
                            <!-- /Caballerizas -->
                        </div><!-- /Row -->

                        <div class="row">
                            <!-- Otros -->
                            <div class="col-sm-12">
                                <h5 class="text-center" style="background-color: #3b4957; padding: .50rem;
color: #fff;">Otros</h5>
                                @if($property->countryside->others)
                                    {!! $property->countryside->others_description !!}
                                @else
                                    <p>No hay otros</p>
                                @endif
                            </div><!-- /col-sm-12-->
                            <!-- /Otros -->

                        </div><!-- /Row -->
                    </div><!-- /col-sm-12 col-md-6 -->
                </div><!-- /Row Titulo -->
                @endif
            </div><!-- /Construcciones -->
        </div><!-- /Container   -->
    </main><!-- /Contacto -->
@endsection