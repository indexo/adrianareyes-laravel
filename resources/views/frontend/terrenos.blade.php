@extends('layouts.frontend')

@section('title', $property->label())

@section('content')

    <main class="Propiedades">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    @if($property->cover_image)<a class="gallery__itemLink " href="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}" data-rel="aiLightbox"><img class="img-fluid" id="imagen-principal-galeria"  src="{{ url('imagenes/inmuebles/' . $property->cover_image->name) }}"/></a> @endif
                    <div class="gallery">
                        <div class="gallery__intro">
                            <ul>
                                @forelse($property->images as $image)
                                    <li class="gallery__item"><a class="gallery__itemLink" href="{{ url('imagenes/inmuebles/' . $image->name) }}" data-rel="aiLightbox"><img class="gallery__itemThumb" src="{{ url('imagenes/inmuebles/' . $image->name) }}"/></a></li>
                                @empty
                                    No hay im&aacute;genes
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div><!-- /col-sm-12 col-md-6   -->

                <div class="col-sm-12 col-md-6">
                    <div class="datos-propiedades">
                        <h3>{!! $property->label() !!} @include('frontend.partials.admin_button')</h3>
                        <p>{!! $property->description !!}</p>

                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2"><h5>Datos Principales</h5></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($property->area)
                                <tr>
                                    <th scope="row">Superficie</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->area }}"
                                                       suffix=" m2" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->distance_from_sea)
                                <tr>
                                    <th scope="row">Distancia al Mar</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->distance_from_sea }}"
                                                       suffix=" m" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->frontyard)
                                <tr>
                                    <th scope="row">Frente</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->frontyard }}"
                                                       suffix=" m2" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->backyard)
                                <tr>
                                    <th scope="row">Fondo</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->backyard }}"
                                                       suffix=" m2" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->sides)
                                <tr>
                                    <th scope="row">Laterales</th>
                                    <td>
                                        <number-format class="badge badge-primary"
                                                       :value="{{ $property->sides }}"
                                                       suffix=" m2" />
                                    </td>
                                </tr>
                            @endif
                            @if($property->FOT)
                                <tr>
                                    <th scope="row">FOT</th>
                                    <td><number-format :value="{{ $property->FOT }}" /></td>
                                </tr>
                            @endif
                            @if($property->FOS)
                                <tr>
                                    <th scope="row">FOS</th>
                                    <td><number-format :value="{{ $property->FOS }}" /></td>
                                </tr>
                            @endif
                            <tr>
                                <th scope="row">Codigo de Referencia</th>
                                <td><span class="badge badge-primary">{{ $property->reference() }}</span></td>
                            </tr>
                            @if($property->notes)
                                <tr>
                                    <th scope="row">Observaciones</th>
                                    <td>{!! $property->notes !!}.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        @if($property->ret_frontyard || $property->ret_backyard || $property->ret_bilateral)
                        <div class="table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2"><h5>Retiros</h5></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($property->ret_frontyard)
                                    <tr>
                                        <th scope="row">Frente</th>
                                        <td>
                                            <number-format class="badge badge-primary"
                                                           :value="{{ $property->ret_frontyard }}"
                                                           suffix=" m" />
                                        </td>
                                    </tr>
                                @endif
                                @if($property->ret_backyard)
                                    <tr>
                                        <th scope="row">Fondo</th>
                                        <td>
                                            <number-format class="badge badge-primary"
                                                           :value="{{ $property->ret_backyard }}"
                                                           suffix=" m" />
                                        </td>
                                    </tr>
                                @endif
                                @if($property->ret_bilateral)
                                    <tr>
                                        <th scope="row">Bilateral</th>
                                        <td>
                                            <number-format class="badge badge-primary"
                                                           :value="{{ $property->ret_bilateral }}"
                                                           suffix=" m" />
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @endif

                            <div class="table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2"><h5>Precios</h5></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Venta</th>
                                        <td>
                                            <number-format class="badge badge-danger"
                                                           prefix="{{ $property->sale->country()->currency_symbol }}"
                                                           :value="{{ $property->sale->price }}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Financiacón</th>
                                        @if($property->sale->financing)
                                            <td><i class="fa fa-check-circle"></i></td>
                                        @else
                                            <td><i class="fa fa-times-circle"></i></td>
                                        @endif
                                    </tr>
                                    @if($property->property_expenses && $property->school_expenses)
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th colspan="3"><h5>Gastos</h5></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($property->property_expenses)
                                        <tr>
                                            <th scope="row">Contribución inmobiliaria</th>
                                            <td>
                                                <number-format class="badge badge-danger"
                                                               prefix="{{ $property->country()->currency_symbol }}"
                                                               :value="{{ $property->expenses_property_tax }}"/>
                                            </td>
                                        </tr>
                                        @endif
                                        @if($property->school_expenses)
                                        <tr>
                                            <th scope="row">Impuesto Primaria</th>
                                            <td>
                                                <number-format class="badge badge-danger"
                                                               prefix="{{ $property->country()->currency_symbol }}"
                                                               :value="{{ $property->school_expenses }}"/>
                                            </td>
                                        </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    @endif
                                    <table class="table">
                                        <tr>
                                            <th colspan="3">
                                                <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalcontacto">Contactar</button>
                                                @include('frontend.partials.contacto-inmueble')
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table><!-- /Gastos -->
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /Datos-Propiedades   -->
                    </div><!-- /col-sm-12 col-md-6 -->

                </div><!-- /Container   -->
    </main><!-- /Contacto -->
@endsection