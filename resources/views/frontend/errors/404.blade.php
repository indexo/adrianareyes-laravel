@extends('layouts.frontend')

@section('title', 'Error')

@section('content')
    <div class="jumbotron">
        <h1 class="display-4">No se encontr&oacute; lo que busca</h1>
        <p class="lead">Si cree que esto er un error contacte un administrador</p>
        <hr class="my-4">
        <p>Puede intentar nuevamente.</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="{{ action('WebController@index') }}" role="button">Volver a Inicio</a>
        </p>
    </div>
@endsection