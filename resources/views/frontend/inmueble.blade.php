<!-- Header -->
<?php include("includes/header.php"); ?><!-- /Header -->

<main class="contacto">
 <div class="container">
 	<div class="row">
  <div class="col-sm-12 col-md-6">
	<div class="gallery">
  <div class="gallery__intro">
  <ul>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
    <li class="gallery__item"><a class="gallery__itemLink" href="https://unsplash.it/1920/1080?image=772" data-rel="aiLightbox"><img class="gallery__itemThumb" src="https://unsplash.it/150/150?image=772"/></a></li>
  </ul>
</div>
  </div> 
  </div><!-- /col-sm-12 col-md-6   --> 

  <div class="col-sm-12 col-md-6">
  	<h3>Nombre Propiedad</h3>
  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A nam illo, ducimus eligendi esse, cumque exercitationem.</p>
  </div><!-- /col-sm-12 col-md-6	 -->
  </div><!-- /row -->
 </div><!-- /Container	 -->
</main><!-- /Contacto -->

<!-- Footer -->
<?php include("includes/footer.php"); ?><!-- /Footer