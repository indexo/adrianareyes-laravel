@extends('layouts.frontend')

@section('title', 'Inicio')

@section('content')
	<div class="contenido">
		<!-- Portada -->
		@include('frontend.partials.portada')

		<!-- Servicios -->
		@include('frontend.partials.servicios')

		<!-- Destacados -->
		@include('frontend.partials.destacados-index')

		<!-- Contacto Portada -->
		<div class="contacto-portada">
			@include('frontend.partials.contacto')
		</div>
	</div>
@endsection