@extends('layouts.frontend')

@section('title', 'Contacto')

@section('content')
<main class="contacto">
 <div class="container">
 	<!-- Contacto Portada -->
    @include('frontend.partials.contacto')
    <!-- /Contacto Portada -->
 </div>	
</main>
@endsection