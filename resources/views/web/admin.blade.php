@extends('layouts.admin')

@section('title', 'Administración - Panel de Control')

@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Panel de Control</li>
@endsection

@section('content')
<h1 class="text-center">Administraci&oacute;n <button type="button" class="btn btn-secondary" @click="refreshData"><i class="fa fa-refresh"></i> Actualizar datos</button> <a href="{{ action('InstallationController@index') }}" class="btn btn-danger"><i class="fa fa-wrench"></i></a></h1>
<hr>
<!-- Actividad -->
<div class="row">
	<div class="col-sm">
		<div class="card">
			<div class="card-header text-center">
				<h3>Actividad <a href="{{ action('ActivityController@index')}}" class="btn btn-info"><i class="fa fa-calendar"></i></a></h3>
			</div>
			<div class="card-body">
				<div id="regions_div" style="width: 900px; height: 500px;"></div>
			</div>
		</div>
	</div>
</div><!-- /Actividad -->
<hr>
<!-- Eventos -->
<div class="row">
    <div class="col-sm">
        <div class="card">
            <div class="card-header text-center">
                <h3>Eventos pr&oacute;ximos <a href="{{ action('CalendarController@index')}}" class="btn btn-info"><i class="fa fa-calendar"></i></a></h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered text-center" id="events">
                    <thead>
                        <tr>
                            <th>T&iacute;tulo</th>
                            <th>Inicio</th>
                            <th>Duraci&oacute;n</th>
                            <th>Creado por</th>
                        </tr>
                    </thead>
                    <tbody v-if="events.length > 0">
                        <tr v-for="event in events">
                            <td>@{{ event.title }}</td>
                            <td>@{{ event.start }}</td>
                            <td>@{{ event.duration }}</td>
                            <td>@{{ event.user.name }}</td>
                        </tr>
                    </tbody>
                    <tbody v-else>
                        <tr>
                            <td colspan="4" class="text-center">No hay datos disponibles</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!-- /Eventos -->
<hr>
<!-- Mensajes -->
<div class="row">
    <div class="col-md">
        <div class="card">
            <div class="card-header text-center">
                <h3>Mensajes sin responder <a href="{{ action('MessageController@index') }}" class="btn btn-info"><i class="fa fa-envelope"></i></a> </h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped text-center" v-if="unansweredMessages.length > 0">
                    <thead>
                        <tr>
                            <th>Remitente</th>
                            <th>Correo electr&oacute;nico</th>
                            <th>Direcci&oacute;n IP</th>
                            <th>Fecha</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(row, i) in unansweredMessages" v-if="i <= 5">
                            <td>@{{ row.first_name + ' ' + row.last_name }}</td>
                            <td>@{{ row.email }}</td>
                            <td>@{{ row.ip }}</td>
                            <td>@{{ row.created_at }}</td>
                            <td><a :href="'{{ url('administracion/mensajes') }}/' + row.id" class="btn btn-primary"><i class="fa fa-reply"></i> Responder</a></td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-center" v-else><i class="fa fa-check"></i> No hay mensajes nuevos</p>
            </div>
        </div>
    </div>
</div><!-- /Mensajes -->
<!-- Inmuebles -->
<hr>
<h2 class="text-center">Inmuebles <a href="{{ action('PropertyController@index')}}" class="btn btn-info"><i class="fa fa-home"></i></a></h2>
<div class="row">
    <div class="col-sm">
        <div class="card">
            <div class="card-header text-center">
                Totales
            </div>
            <div class="card-body">
                <p class="card-text"><strong>Total:</strong> @{{ totalProperties }}</p>
                <hr>
                <p class="card-text"><strong>Apartamentos:</strong> @{{ totalApartments }}</p>
                <p class="card-text"><strong>Casas:</strong> @{{ totalHouses }}</p>
                <p class="card-text"><strong>Chacras / Campos:</strong> @{{ totalCountrysides }}</p>
                <p class="card-text"><strong>Locales comerciales:</strong> @{{ totalBusinessPremises }}</p>
                <p class="card-text"><strong>Terrenos:</strong> @{{ totalLands }}</p>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-header text-center">
                En venta
            </div>
            <div class="card-body">
                <p class="card-text"><strong>Total:</strong> @{{ totalSales }} propiedades</p>
                <p class="card-text"><strong>Vendidos:</strong> @{{ totalSold }} propiedades</p>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-header text-center">
                En alquiler
            </div>
            <div class="card-body">
                <p class="card-text"><strong>Total:</strong> @{{ totalRents }} propiedades</p>
                <p class="card-text"><strong>Alquilados:</strong> @{{ totalRented }} propiedades</p>
            </div>
        </div>
    </div>
</div><!-- /Inmuebles -->
<hr>
<!-- Clientes -->
<div class="row">
    <div class="col-md">
        <div class="card">
            <div class="card-header">
                <h4 class="text-center">
                    &Uacute;ltimos clientes <a class="btn btn-info" href="{{ action('CustomerController@index') }}"> <i class="fa fa-building"></i></a>
                </h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered text-center" id="users">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Notas</th>
                        </tr>
                    </thead>
                    <tbody v-if="customers.length > 0">
                        <tr v-for="customer in customers">
                            <td>@{{ customer.last_name + ', ' + customer.first_name }}</td>
                            <td>@{{ customer.type == 'owner' ? 'Propietario' : 'Inquilino' }}</td>
                            <td>@{{ customer.notes }}</td>
                        </tr>
                    </tbody>
                    <tbody v-else>
                        <tr>
                            <td colspan="3" class="text-center">No hay clientes</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!-- /Clientes -->
@endsection

@push('js_files')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endpush

@push('js')
var datos = [];
$.ajax({
	url: '{{ url('/api/admin/uso') }}',
	method: 'GET',
	dataType: 'json',
	success: function(response) {
		google.charts.load('current', {
			'packages':['geochart'],
			'mapsApiKey': 'AIzaSyD9pfblIGwYIdG6no3LWo6MZTGh4XIdudM'
		});
		google.charts.setOnLoadCallback(drawRegionsMap);

		function drawRegionsMap() {
			var data = google.visualization.arrayToDataTable(response);

			var options = {};

			var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

			chart.draw(data, options);
		}
    }
});
@endpush

