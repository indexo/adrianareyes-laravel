/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 *
 * Variables y funciones NO ADMINISTRATIVAS llevan prefijo _ (guión bajo).
 */

global.jQuery = require('jquery');
window.Popper = require('popper.js').default;
window.Vue = require('vue');
window.axios = require('axios');

import Vuetify from 'vuetify';
Vue.use(Vuetify)

var $ = global.jQuery;
window.$ = $;

// https://www.npmjs.com/package/vue-currency-filter
import VueCurrencyFilter from 'vue-currency-filter';
Vue.use(VueCurrencyFilter,
{
  symbol : '$',
  thousandsSeparator: '.',
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
});

import Vue from 'vue'
import money from 'v-money'
import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)

// register directive v-money and component <money>
Vue.use(money, {precision: 4})

// Vue Select
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

// Registro de componentes
Vue.component('expenses-form', require('./components/FormExpensesComponent.vue'));
Vue.component('geography-field', require('./components/GeographyFieldComponent.vue'));
Vue.component('property-specification', require('./components/PropertySpecificationComponent.vue'));
Vue.component('environment', require('./components/EnvironmentComponent.vue'));
Vue.component('exterior', require('./components/ExteriorComponent.vue'));
Vue.component('equipment', require('./components/EquipmentComponent.vue'));
Vue.component('service-amenity', require('./components/ServiceAmenityComponent.vue'));
Vue.component('property-house', require('./components/PropertyHouseComponent.vue'));
Vue.component('guest-room', require('./components/GuestRoomComponent.vue'));
Vue.component('form-currency', require('./components/FormCurrencyComponent.vue'));
/*Vue.component('table-pagination', require('./components/TablePaginationComponent.vue'));*/
Vue.component('currency', require('./components/Currency.vue'));
/*Vue.component('upload', require('./components/Upload.vue'));*/
Vue.component('number-format', require('./components/NumberFormat.vue'));

// / Registro de componentes

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#v-app',
    components: {
        
    },
    // Se declaran las variables
    data: {
    	propertyType: null,
    	operationType: null,
    	operations: [
    		{ value: 'rent', text: 'Alquiler' }, 
    		{ value: 'sale', text: 'Venta' }, 
    		{ value: 'rent-sale', text: 'Alquiler y Venta' }
    	],
    	ops: [],
    	guestRoom: false,
    	expenses: 0,
    	specCutwater: false,
    	specWiring: false,
    	specStreams: false,
    	specHills: false,
    	specLight: false,
    	csHouse: false,
    	csSheds: false,
    	csStables: false,
    	csOthers: false,

    	totalEnvironments: 0,
    	totalEquipment: 0,
    	totalExterior: 0,
    	totalServicesAmenities: 0,
    	totalProperties: 0,
    	totalApartments: 0,
    	totalBusinessPremises: 0,
    	totalCountrysides: 0,
    	totalHouses: 0,
    	totalLands: 0,
    	totalRents: 0,
    	totalSales: 0,
    	totalRented: 0,
    	totalSold: 0,
        users: [],
        totalUsers: 0,
        totalActiveUsers: 0,
        totalInactiveUsers: 0,

    	activities: [],
    	startDate: '',
    	startTime: '',
    	allDay: false,
    	events: [],
    	users: [],
        servicesIntroduction: '',
        services: [],
        offices: [],
        customers: [],

        countries: [],
        departments: [],
        localities: [],
        zones: [],
        selectedCountry: 0,
        selectedDepartment: 0,
        selectedLocation: 0,
        selectedZone: 0,

        adminCountries: [],
        adminDepartments: [],
        adminLocalities: [],
        adminZones: [],

        iso3: '',
        currency: '',
        currencySale: 0,
        currencyRent: 0,
        currencyExpenses: 0,
        pagination: {
    	    total: 0,
            currentPage: 0,
            perPage: 0,
            lastPage: 0,
            from: 0,
            to: 0
        },
        unansweredMessages: 0,

        defaultTDO: '',
        defaultTDI: '',
        defaultLocation: '',
        defaultFrom: '',
        defaultTo: '',
        defaultRooms: '',
        defaultBathrooms: '',
        defaultCurrency: '',
    },
    mounted: function() {
    	// Cargamos los datos en la administración
        this.getTotalProperties();
        this.loadActivities();
    	this.getTotalApartments();
    	this.getTotalBusinessPremises();
    	this.getTotalCountrysides();
    	this.getTotalHouses();
    	this.getTotalLands();
    	this.getTotalRents();
    	this.getTotalSales();
    	this.getTotalRented();
    	this.getTotalSold();
    	this.loadEvents(5);
        this.loadCustomers(5);

        // Cargamos los datos en la página web
        this._loadServicesIntroduction();
        this._loadServices();
        this._loadCountries();
        this._loadDepartments();
        this._loadLocalities();
        this._loadZones();

        this.loadCountries();
        this.loadDepartments();
        this.loadLocalities();
        this.loadZones();

        this.loadUnansweredMessages();

        this.setSearchDefaultValues();
    },
    // Se establecen los métodos
    methods: {
        // Devuelve una full qualified URL con el parametro pasado
        getURL: (path) => {
            let url = window.location.origin;
            if(window.location.host === 'localhost') {
                let splittedUrl = window.location.pathname.split('/');
                splittedUrl = splittedUrl.filter(str => str.length > 0);
                url = `${url}/${splittedUrl.shift()}`;
            }
            return `${url}/${path}`;
        },
    	// Obtiene las opciones del tipo de operación según el tipo de inmueble
    	loadOps: function() {
    		this.ops = [];
    		switch(this.propertyType) {
				case 'countryside':
				case 'land':
    				this.ops.push(this.operations[1]);
    				break;
    			default:
    				this.ops = this.operations;
    		}
    	},
    	// Carga el formulario según el tipo de inmueble y tipo de operación en la URL para que sea diferenciado en Laravel
    	redirectToForm: function() {
    		var addToURL = `?tdi=${this.propertyType}&tdo=${this.operationType}`;

    		window.location.href = addToURL;
    	},
        // Obtiene la cantidad de ambientes
    	getTotalEnvironments: function() {
    		axios.get(this.getURL('api/admin/ambientes/total'))
    			.then(function (response) {
    				app.totalEnvironments = response.data;
    			});
    	},
        // Obtiene la cantidad de equipamientos
    	getTotalEquipment: function() {
    		axios.get(this.getURL('api/admin/equipamientos/total'))
    			.then(function (response) {
    				app.totalEquipment = response.data;
    			});
    	},
        // Obtiene la cantidad de exteriores
    	getTotalExterior: function() {
    		axios.get(this.getURL('api/admin/exterior/total'))
    			.then(function (response) {
    				app.totalExterior = response.data;
    			});
    	},
        // Obtiene la cantidad de servicios y comodidades
    	getTotalServiceAmenity: function() {
    		axios.get(this.getURL('api/admin/servicios-comodidades/total'))
    			.then(function (response) {
    				app.totalServicesAmenities = response.data;
    			});
    	},
        // Obtiene la cantidad de propiedades
    	getTotalProperties: function() {
    		axios.get(this.getURL('api/admin/inmuebles/total'))
    			.then(function (response) {
    				app.totalProperties = response.data;
    			});
    	},
        // Obtiene la cantidad de apartamentos
    	getTotalApartments: function() {
    		axios.get(this.getURL('api/admin/inmuebles/apartamentos/total'))
    			.then(function (response) {
    				app.totalApartments = response.data;
    			});
    	},
        // Obtiene la cantidad de locales comerciales
    	getTotalBusinessPremises: function() {
    		axios.get(this.getURL('api/admin/inmuebles/locales-comerciales/total'))
    			.then(function (response) {
    				app.totalBusinessPremises = response.data;
    			});
    	},
        // Obtiene la cantidad de chacras y campos
    	getTotalCountrysides: function() {
    		axios.get(this.getURL('api/admin/inmuebles/chacras-campos/total'))
    			.then(function (response) {
    				app.totalCountrysides = response.data;
    			});
    	},
        // Obtiene la cantidad de casas
    	getTotalHouses: function() {
    		axios.get(this.getURL('api/admin/inmuebles/casas/total'))
    			.then(function (response) {
    				app.totalHouses = response.data;
    			});
    	},
        // Obtiene la cantidad de terrenos
    	getTotalLands: function() {
    		axios.get(this.getURL('api/admin/inmuebles/terrenos/total'))
    			.then(function (response) {
    				app.totalLands = response.data;
    			});
    	},
        // Obtiene la cantidad de inmuebles en alquiler
    	getTotalRents: function() {
    		axios.get(this.getURL('api/admin/inmuebles/alquileres/total'))
    			.then(function (response) {
    				app.totalRents = response.data;
    			});
    	},
        // Obtiene la cantidad de inmuebles en venta
    	getTotalSales: function() {
    		axios.get(this.getURL('api/admin/inmuebles/ventas/total'))
    			.then(function (response) {
    				app.totalSales = response.data;
    			});
    	},
        // Obtiene la cantidad de inmuebles alquilados
    	getTotalRented: function() {
    		axios.get(this.getURL('api/admin/inmuebles/alquilados/total'))
    			.then(function (response) {
    				app.totalRented = response.data;
    			});
    	},
        // Obtiene la cantidad de inmuebles vendidos
    	getTotalSold: function() {
    		axios.get(this.getURL('api/admin/inmuebles/vendidos/total'))
    			.then(function (response) {
    				app.totalSold = response.data;
    			});
    	},
        // Obtiene las actividades
        loadActivities: function(page = 1) {
            axios.get(this.getURL(`api/admin/actividades?page=${page}`))
                .then(function (response) {
                    app.activities = response.data;
                });
        },
        // Obtiene la casa de invitados del inmueble pasado por parámetro
    	getGuestRoom: function(propertyId) {
    		axios.get(this.getURL(`api/admin/inmuebles/${propertyId}/invitados`))
    			.then(function (response) {
    				app.guestRoom = response.data;
    			});
    	},
        // Obtiene la introducción de los servicios
        loadServicesIntroduction: function() {
            axios.get(this.getURL('api/admin/sitio/servicios/introduccion'))
                .then(function (response) {
                    app.servicesIntroduction = response.data;
                });
        },
        // Obtiene los servicios
        loadServices: function() {
             axios.get(this.getURL('api/admin/sitio/servicios'))
                .then(function (response) {
                    app.services = response.data;
                });
        },
        // Obtiene las oficinas
        loadOffices: function(total) {
	    let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/admin/oficinas${param}`))
                .then(function (response) {
                    app.offices = response.data;
                });
        },
        // Obtiene los clientes
        loadCustomers: function(total) {
	    let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/admin/clientes${param}`))
                .then(function (response) {
                    app.customers = response.data;
                });
        },
    	// Volvemos a cargar los datos al presionar el boton de "Refrescar datos" en la administracion
    	refreshData: function() {
	    	this.getTotalProperties();
	    	this.getTotalApartments();
	    	this.getTotalBusinessPremises();
	    	this.getTotalCountrysides();
	    	this.getTotalHouses();
	    	this.getTotalLands();
	    	this.getTotalRents();
	    	this.getTotalSales();
	    	this.getTotalRented();
	    	this.getTotalSold();
    		this.loadEvents(5);
            this.loadUnansweredMessages();
    	},
        // Obtiene el ID del inmueble desde la URL
    	getPropertyIdFromURL: function() {
    		var baseuri = this.$el.baseURI;
    		var arr = baseuri.split('/');
    		for(var a in arr) {
    			if(arr[a] === 'inmuebles') {
    				var property = arr[parseInt(a)+1];
    				if($.isNumeric(property)) {
    					return property;
    				}
    			}
    		}
    		return false;
    	},
        // Obtiene la cantidad de usuarios
    	getTotalUsers: function () {
    		axios.get(this.getURL('api/admin/usuarios/total'))
    			.then(function (response) {
    				app.totalUsers = response.data;
    			});
    	},
        //Obtiene la cantidad de usuarios activos
        getTotalActiveUsers: function () {
            axios.get(this.getURL('api/admin/usuarios/activos/total'))
                .then(function (response) {
                    app.totalActiveUsers = response.data;
                });
        },
        //Obtiene la cantidad de usuarios inactivos
        getTotalInactiveUsers: function () {
            axios.get(this.getURL('api/admin/usuarios/inactivos/total'))
                .then(function (response) {
                    app.totalInactiveUsers = response.data;
                });
        },
        // Obtiene los usuarios
        getUsers: function (total) {
		let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/admin/usuarios${param}`))
                .then(function (response) {
                    app.users = response.data;
                });
        },
        // Obtiene los usuarios activos
    	loadActiveUsers: function() {
    		axios.get(this.getURL('api/admin/usuarios/activos'))
    			.then(function (response) {
    				app.activeUsers = response.data;
    			});
    	},
        // Obtiene los usuarios inactivos
    	loadInactiveUsers: function() {
    		axios.get(this.getURL('api/admin/usuarios/inactivos'))
    			.then(function (response) {
    				app.inactiveUsers = response.data;
    			});
    	},
        // Obtiene los eventos
    	loadEvents: function (total) {
		let param = total ? `/${total}` : '';
    		axios.get(this.getURL(`api/admin/eventos${param}`))
    			.then(function(response) {
    				app.events = response.data;
    			});
    	},
        // APIController@getDepartments(): Obtiene todos los países o una cantidad específica
        loadCountries: function(total) {
		let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/inmuebles/paises${param}`))
                .then(function (response) {
                    app.countries = response.data;
                });

            total = (total <= this.countries.length) 
                ? total 
                : this.countries.length;

            if(total) {
                var newArr = [];
                for(i=0; i<total; i++) {
                    newArr.push(this.countries[i]);
                }
                this.countries = newArr;
                console.log('paises cargados')
            }            
        },
        // APIController@getDepartments(): Obtiene todos los departamentos o una cantidad específica
        loadDepartments: function(total) {
		let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/inmuebles/departamentos${param}`))
                .then(function (response) {
                    app.departments = response.data;
                });

            total = (total <= this.departments.length) 
                ? total 
                : this.departments.length;

            if(total) {
                var newArr = [];
                for(i=0; i<total; i++) {
                    newArr.push(this.departments[i]);
                }
                this.departments = newArr;
                console.log('departamentos cargados');
            }        
        },
        // APIController@getLocalities(): Obtiene todas las localidades o una cantidad específica
        loadLocalities: function(total) {
		let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/inmuebles/localidades${param}`))
                .then(function (response) {
                    app.localities = response.data;
                });
            
            total = (total <= this.localities.length) 
                ? total 
                : this.localities.length;

            if(total) {
                var newArr = [];
                for(i=0; i<total; i++) {
                    newArr.push(this.localities[i]);
                }
                this.localities = newArr;
                console.log('localidades cargadas');
            }        
        },
        // ApiController@getZones(): Obtiene todas las zonas o una cantidad específica
        loadZones: function(total) {
		let param = total ? `/${total}` : '';
            axios.get(this.getURL(`api/inmuebles/zonas${param}`))
                .then(function (response) {
                    app.zones = response.data;
                });

            total = (total <= this.zones.length) 
                ? total 
                : this.zones.length;
            
            if(total) {
                var newArr = [];
                for(i=0; i<total; i++) {
                    newArr.push(this.zones[i]);
                }
                this.zones = newArr;
                console.log('zonas cargadas');
            }        
        },
        selectCountry: function(id) {
            this.selectedCountry = id;
    	    if(id < 1) {
                this.departments = [];
                this.localities = [];
                this.zones = [];
    	        return;
            }
		let param = id ? `/${id}` : '';
            axios.get(this.getURL(`api/inmuebles/pais${param}`))
                .then(function (response) {
                    var data = response.data;
                    if(data.departments.length > 0) {
                        var departments = [];
                        $.each(data.departments, function (index, value) {
                            departments.push(value);
                        });
                        app.departments = departments;
                    }
                });
        },
        selectDepartment: function(id) {
            this.selectedDepartment = id;
            if(id < 1) {
                this.localities = [];
                this.zones = [];
                return;
            }
		let param = id ? `/${id}` : '';
            axios.get(this.getURL(`api/inmuebles/departamento${param}`))
                .then(function (response) {
                    var data = response.data;
                    if(data.localities.length > 0) {
                        var localities = [];
                        $.each(data.localities, function (index, value) {
                            localities.push(value);
                        });
                        app.localities = localities;
                    }
                });
        },
        selectLocation: function(id) {
            this.selectedLocation = id;
            if(id < 1) {
                this.zones = [];
                return;
            }
		    let param = id ? `/${id}` : '';
            axios.get(this.getURL(`api/inmuebles/localidad${param}`))
             .then(function (response) {
                var data = response.data;
                if(data.zones.length > 0) {
                    var zones = [];
                    $.each(data.zones, function (index, value) {
                        zones.push(value);
                    });
                    app.zones = zones;
                }
            });
        },
        // ** Funciones NO ADMINISTRATIVAS **
        _loadServicesIntroduction: function() {
            axios.get(this.getURL('api/sitio/servicios/introduccion'))
                .then(function (response) {
                    app.servicesIntroduction = response.data;
                });
        },
        // Obtiene todos los servicios
        _loadServices: function() {
             axios.get(this.getURL('api/sitio/servicios'))
                .then(function (response) {
                    app.services = response.data;
                });
        },
        // APIController@getCountries($id): Obtiene los países o un país específico
        _loadCountries: function(id) {
            if(id) {
		    let param = id ? `/${id}` : '';
                axios.get(this.getURL(`api/inmuebles/paises${param}`))
                    .then(function (response) {
                        app.countries = response.data;
                    });
            }
        },
        // APIController@getDepartments($id): Obtiene los departamentos o un departamento específico
        _loadDepartments: function(id) {
            if(id) {
		    let param = id ? `/${id}` : '';
                axios.get(this.getURL(`api/inmuebles/departamentos${param}`))
                    .then(function (response) {
                        app.departments = response.data;
                    });
            }
        },
        // APIController@getLocalities($id): Obtiene las localidades o una localidad específica
        _loadLocalities: function(id) {
            if(id) {
		    let param = id ? `/${id}` : '';
                axios.get(this.getURL(`api/inmuebles/localidades${param}`))
                    .then(function (response) {
                        app.localities = response.data;
                    });
            }
        },
        // ApiController@getZones($id): Obtiene las zonas o una zona específica
        _loadZones: function(id) {
            if(id) {
		    let param = id ? `/${id}` : '';
                axios.get(this.getURL(`api/inmuebles/zonas${param}`))
                    .then(function (response) {
                        app.zones = response.data;
                    });
            }
        },
        // Obtiene el cambio de moneda para el país seleccionado
        getCurrencyExchange: function(iso3) {
            if(!iso3) { return; }
            axios.get(this.getURL(`https://restcountries.eu/rest/v2/currency/${iso3}`))
                .then(function (response) {

                });
        },
        // Cambia el estado de la casa
        toggleHouse: function(value) {
            this.csHouse = value;
        },
        // Cambia el estado del apartamento de invitados
        toggleGuestRoom: function(value) {
            this.guestRoom = value;
        },
        // Ordena las columnas de la tabla de actividades
        orderActivity: function(column) {
    	    axios.get(this.getURL(`api/admin/actividades/order_by/${column}`))
                .then(function(response) {
                    app.activities = response.data;
                });
        },
        loadUnansweredMessages: function() {
    	    axios.get(this.getURL('api/admin/mensajes/sin-responder'))
                .then(function(response) {
                    app.unansweredMessages = response.data;
                });
        },
        setCurrency: function (currency, value) {
    	    switch (currency) {
                case 'currency_sale':
                    this.currencySale = value;
                    break;
                case 'currency_rent':
                    this.currencyRent = value;
                    break;
            }
        },
        setSearchDefaultValues: function() {
            let queryString = window.location.search.substr(1);
            queryString = queryString.split('&');
            let params = {};
            queryString.forEach(param => {
                let p = param.split('=');
                params[p[0]] = p[1];
            });
            
            if (params.tdo) {
                this.defaultTDO = params.tdo;
            }

            if (params.tdi) {
                this.defaultTDI = params.tdi;
            }

            if (params.location) {
                this.defaultLocation = params.location;
            }
            
            if (params.from) {
                this.defaultFrom = params.from;
            }
            
            if (params.to) {
                this.defaultTo = params.to;
            }

            if (params.rooms) {
                this.defaultRooms = params.rooms;
            }

            if (params.bathrooms) {
                this.defaultBathrooms = params.bathrooms;
            }

            if (params.currency) {
                this.defaultCurrency = params.currency;
            }
        }
    },
    // Se declaran los Watchers para observar los cambios en el DOM
    watch: {
    	selectedCountry: function(value) {
            if(value < 1) {
                this.selectedDepartment = 0;
                this.selectedLocation = 0;
                this.selectedZone = 0;
            }
        },
        selectedDepartment: function(value) {
            if(value < 1) {
                this.selectedLocation = 0;
                this.selectedZone = 0;
            }
        },
        selectedLocation: function(value) {
            if(value < 1) {
                this.selectedZone = 0;
            }
        },
        activities: function(value) {
    	    if(value) {
    	        this.pagination.currentPage = this.activities.current_page;
    	        this.pagination.perPage = this.activities.per_page;
    	        this.pagination.lastPage = this.activities.last_page;
    	        this.pagination.from = this.activities.from;
    	        this.pagination.to = this.activities.to;
    	        this.pagination.total = this.activities.total;
            }
        }
    }
});
