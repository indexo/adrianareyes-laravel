## Sistema Inmobiliario

El Sistema Inmobiliario es una plataforma cuyo objetivo es facilitar e integrar las herramientas para administrar, organizar y mostrar inmuebles en un sitio.

### Prerequisitos

#### Servidor Web

* ![Apache / NGINX](https://img.shields.io/badge/Servidor-Apache/NGINX-blue.svg)
* ![MySQL / MariaDB >= 5.6](https://img.shields.io/badge/MySQL/MariaDB-%3E=5.6-blue.svg)
* ![PHP >= 7.0.0](https://img.shields.io/badge/PHP-%3E=7.0.0-blue.svg)

#### PHP (Extensiones)

* OpenSSL
* PDO
* Mbstring
* Tokenizer
* XML

### Características

* Sitio web
* Ingreso y Edición de Propiedades
* Calendario con notificaciones
* Registro de Actividades
* Registro de Usuarios

### Licencia

**EULA** (End User License Agreement) o **ALUF** (Acuerdo de Licencia de Usuario Final) prohibe la total o parcial redistribución, modificación o clonación del Sistema de Inmobiliarias a cualquier individuo o empresa que no sea ![INDEXO](https://www.indexo.uy) o integrante de.
El Sistema de Inmobiliarias es de uso comercial y su redistribución, modificación o clonación ilegal está legislada por el gobierno nacional (República Oriental del Uruguay).

### Migración

#### 1. Archivos

1. Ejecutar `git clone git@gitlab.com:indexo/adrianareyes-laravel.git` en la ruta principal del servidor web.
2. Pararse sobre la carpeta descargada con `cd adrianareyes-laravel`
3. Instalar las librerías de Node con `npm install`
4. Instalar las librerías de Composer con `composer install`
5. Migrar el directorio **storage** y reemplazar los archivos para migrar las imágenes
6. Dar todos los permisos a la carpeta **storage** con `chmod 777 -R storage`
7. Migrar el archivo **.env** manualmente ya que se encuentra en .gitignore por contener información confidencial

#### 2. Base de datos

Importar la base de datos mediante un dump creado en phpMyAdmin