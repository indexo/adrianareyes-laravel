<?php

namespace App\Http\Middleware;

use Closure;
use App\Country;

class CurrentCurrency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->hasCookie('currency'))
        $country = Country::find($request->cookie('currency'));
        return $next($request);
    }
}
