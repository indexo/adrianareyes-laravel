<?php

namespace App\Http\Middleware;

use Closure;
use App\Activity;

class LogActivity
{
    private $connection = true;


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        // Si el método HTTP es GET
        if($request->isMethod('get') && $this->connection) {
            // Creamos la instancia de Actividad
            $activity = new Activity;

            // Guardamos la IP del usuario - $_SERVER['remote_addr']
            $activity->remote_addr = $request->ip();
            // Guardamos el cliente del usuario - $_SERVER['HTTP_USER_AGENT']
            $activity->http_user_agent = $request->server('HTTP_USER_AGENT');
            // Guardamos la URI del usuario (URL completa)
            $activity->request_uri = $request->path();
            // Gurdamos la URL de referencia (anterior)
            $activity->referer = $request->headers->get('referer');
            // Guradamos el ID del usuario logueado (si existe)
            @$activity->user_id = \Auth::id();

            // Con cURL obtenemos el país según la IP
            @$json = file_get_contents('http://ip-api.com/json/' . $activity->remote_addr);

            if($json) {
                $result = json_decode($json);

                if($result->status != 'fail') {
                    $activity->country = $result->country;
                }
            }

            // Guardamos la actividad
            @$activity->save();
        }

        return $next($request);
    }
}
