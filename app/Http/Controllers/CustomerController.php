<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();

        foreach($customers as $k => $c)
        {
            $phone_array = explode("\r\n", $c->phones);
            $phone_links = '';

            foreach($phone_array as $phone)
            {
                $phone_links .= "<a href=\"tel:$phone\">$phone</a>\r\n";
            }

            $customers[$k]['phone_links'] = $phone_links;
        }

        return view('admin.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name'          => 'required',
            'email'               => 'email',
            'type'                => 'required',
            'notes'               => 'max:600',
        ];

        $messages = [
            'first_name.required' => 'El nombre es obligatorio',
            'email.email'         => 'El correo electr&oacute;nico es incorrecto',
            'type.required'       => 'El tipo de cliente es obligatorio',
            'notes.max'           => 'Las notas deben tener como m&aacute;ximo 600 caracteres',

        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $customer = new Customer;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phones = $request->phones;
        $customer->type = $request->type;
        $customer->notes = $request->notes;

        if(!$customer->save()) {
            parent::message(
                'Hubo un error al ingresar el cliente. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El cliente ha sido creado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CustomerController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Customer $customer)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Customer $customer)
    {
        return view('admin.customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Customer $customer)
    {
        $rules = [
            'first_name'          => 'required',
            'email'               => 'email',
            'type'                => 'required',
            'notes'               => 'max:600',
        ];

        $messages = [
            'first_name.required' => 'El nombre es obligatorio',
            'email.email'         => 'El correo electr&oacute;nico es incorrecto',
            'type.required'       => 'El tipo de cliente es obligatorio',
            'notes.max'           => 'Las notas deben tener como m&aacute;ximo 600 caracteres',

        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phones = $request->phones;
        $customer->type = $request->type;
        $customer->notes = $request->notes;  

        if(!$customer->save()) {
            parent::message(
                'Hubo un error al actualizar el cliente. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El cliente ha sido actualizado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CustomerController@edit', $customer));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Customer $customer)
    {
        if(!$customer->delete()) {
            parent::message(
                'Hubo un problema al eliminar el cliente. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'El cliente ha sido eliminado correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('CustomerController@index'));
    }
}
