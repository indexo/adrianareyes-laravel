<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'                  => 'required|max:45',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required|min:6|confirmed',
            'avatar'                => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'name.max'              => 'El nombre debe contener como m&aacute;ximo 45 caracteres',
            'email.required'        => 'El correo electr&oacute;nico es obligatorio',
            'email.email'           => 'El correo electr&oacute;nico es incorrecto',
            'email.unique'          => 'El correo electr&oacute;nico ya existe',
            'password.required'     => 'La contrase&ntilde;a es obligatoria',
            'password.confirmed'    => 'Las contrase&ntilde;as no coinciden',
            'password.min'          => 'La contrase&ntilde;a debe tener al menos seis caracteres',
            'avatar.mimes'          => 'La foto debe tener formato JPEG, PNG, JPG, GIF o SVG',
            'avatar.max'            => 'La foto debe pesar menos de 2 MB'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->signature = $request->signature;

        if(!$request->avatar) {
            $user->avatar = null;
        } else {
            $img = $request->avatar->store('images/users');
            $filename = explode('/', $img);
            $filename = $filename[2]; 
            $user->avatar = $filename;
        }            

        if(!$user->save()) {
            parent::message(
                'Hubo un error al ingresar el usuario. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El usuario ha sido creado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('UserController@edit', $user));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\User $user)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\User $user)
    {
        $rules = [
            'name'                  => 'required|max:45',
            'avatar'                => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        if($request->password) {
            $rules['password'] = 'required|min:6|confirmed';
        }

        if($request->email !== $user->email) {
            $rules['email'] = 'required|email|unique:users';
        }

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'name.max'              => 'El nombre debe contener como m&aacute;ximo 45 caracteres',
            'email.required'        => 'El correo electr&oacute;nico es obligatorio',
            'email.email'           => 'El correo electr&oacute;nico es incorrecto',
            'email.unique'          => 'El correo electr&oacute;nico ya existe',
            'password.required'     => 'La contrase&ntilde;a es obligatoria',
            'password.confirmed'    => 'Las contrase&ntilde;as no coinciden',
            'password.min'          => 'La contrase&ntilde;a debe tener al menos seis caracteres',
            'avatar.mimes'          => 'La foto debe tener formato JPEG, PNG, JPG, GIF o SVG',
            'avatar.max'            => 'La foto debe pesar menos de 2 MB'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->signature = $request->signature;
        if($request->password) {
            $user->password = Hash::make($request->password);
        }

        if($request->avatar) {
            @\Storage::delete('images/users/'.$user->avatar);

            $img = $request->avatar->store('images/users');
            $filename = explode('/', $img);
            $filename = $filename[2]; 
            $user->avatar = $filename;
        }            

        if(!$user->save()) {
            parent::message(
                'Hubo un error al actualizar el usuario. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El usuario ha sido actualizado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('UserController@edit', $user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\User $user)
    {
        @\Storage::delete('images/users/'.$user->avatar);

        if(!$user->delete()) {
            parent::message(
                'Hubo un problema al eliminar el usuario. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'El usuario ha sido eliminado correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('UserController@index'));
    }

    public function getAvatar(\App\User $user)
    {
        $path = 'images/users/' . $user->avatar;

        if(Storage::exists($path)) {
            $file = Storage::get($path);
            $response = Response::make($file, 200);
            $response->header('Content-Type', 'image/jpeg');

            return $response;
        } else {
            return view('admin.errors.404');
        }
    } 
}
