<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();

        return view('admin.departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departments.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'                  => 'required',
            'country_id'            => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'country_id.required'   => 'El pa&iacute;s es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $department = new Department;
        $department->name = $request->name;
        $department->country_id = $request->country_id;

        if(!$department->save()) {
            parent::message(
                'Hubo un error al ingresar el departamento. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El departamento ha sido creado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('DepartmentController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Department $department)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Department $department)
    {
        return view('admin.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Department $department)
    {
        $rules = [
            'name'                  => 'required',
            'country_id'            => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'country_id.required'   => 'El pa&iacute;s es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $department->name = $request->name;
        $department->country_id = $request->country_id;

        if(!$department->save()) {
            parent::message(
                'Hubo un error al actualizar el departamento. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El departamento ha sido actualizado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('DepartmentController@edit', $department));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Department $department)
    {
        if(!$department->delete()) {
            parent::message(
                'Hubo un problema al eliminar el departamento. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'El departamento ha sido eliminado correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('DepartmentController@index'));
    }
}
