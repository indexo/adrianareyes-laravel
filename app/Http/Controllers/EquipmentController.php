<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Http\Request;
use Validator;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipment = Equipment::all();

        return view('admin.equipment.index', compact('equipment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.equipment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:equipment',
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $equipment = new Equipment;

        $equipment->name = $request->name;
        $equipment->type = $request->type;
        $equipment->placeholder = $request->placeholder;

        if(!$equipment->save()) {
            parent::message(
                'Hubo un problema al guardar el equipamiento. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El equipamiento se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EquipmentController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function show(Equipment $equipment)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipment $equipment)
    {
        return view('admin.equipment.edit', compact('equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipment $equipment)
    {

        if($request->name != $equipment->name) {
            $name = 'required|unique:equipment';
        } else {
            $name = 'required';
        }
        $rules = [
            'name' => $name,
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $equipment->name = $request->name;
        $equipment->type = $request->type;
        $equipment->placeholder = $request->placeholder;

        if(!$equipment->save()) {
            parent::message(
                'Hubo un problema al guardar el equipamiento. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El equipamiento se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EquipmentController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipment $equipment)
    {
        if(!$equipment->delete()) {
            parent::message(
                'Hubo un problema al eliminar el equipamiento. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El equipamiento se elimin&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EquipmentController@index'));
    }
}
