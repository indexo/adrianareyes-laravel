<?php

namespace App\Http\Controllers;

use App\Site;
use Illuminate\Http\Request;
use Validator;
use Storage;
use Response;

class SiteController extends Controller
{
    public function index()
    {
        return view('admin.site.index');
    }

    public function about_us(Request $request)
    {
        $rules = [
            'about_us' => 'required',
        ];

        $messages = [
            'about_us.required' => 'El texto es obligatorio'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $site = Site::first();
        if(!$site) {
            $site = new Site;
        }
        $site->about_us = $request->about_us;

        if(!$site->save()) {
            parent::message(
                'Hubo un error al guardar la secci&oacute;n "Nosotros". Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La secci&oacute;n "Nosotros" ha sido guardada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('SiteController@index'));
    }


    public function company(Request $request)
    {
        $rules = [
            'company_name' => 'required|max:50',
            'logo' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $messages = [
            'company_name.required' => 'El nombre es obligatorio',
            'company_name.max'      => 'El nombre debe contener como m&aacute;ximo 50 caracteres',
            'logo.mimes'    => 'El logo debe tener formato JPEG, PNG, JPG, GIF o SVG',
            'logo.max'      => 'El logo debe pesar menos de 2 MB'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $site = Site::first();
        if(!$site) {
            $site = new Site;
        }
        $site->company_name = $request->company_name;
        $site->company_text = $request->company_text;

        if($request->logo) {
            $site->logo = $request->logo->store('images/company');
        }

        if(!$site->save()) {
            parent::message(
                'Hubo un error al guardar la informaci&oacute;n de la empresa. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La informaci&oacute;n de la empresa ha sido guardada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('SiteController@index'));
    }

    public function contact_us(Request $request)
    {
        $rules = [
            'contact_us' => 'required',
        ];

        $messages = [
            'contact_us.required' => 'El texto es obligatorio'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $site = Site::first();
        if(!$site) {
            $site = new Site;
        }
        $site->contact_us = $request->contact_us;

        if(!$site->save()) {
            parent::message(
                'Hubo un error al guardar la secci&oacute;n "Contacto". Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La secci&oacute;n "Contacto" ha sido guardada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('SiteController@index'));
    }

    public function services(Request $request)
    {
        $site = Site::first();

        if(!$site) {
            $site = new Site;
        }

        $site->services = $request->services_description;

        if(!$site->save()) {
            parent::message(
                'La introducci&oacute;n no se pudo guardar. Intente de nuevo o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La introducci&oacute;n ha sido guardada correctamente',
            '¡Excelente!',
            'success'
        );

        return redirect(action('ServiceController@index'));
   }

   public function getLogo()
    {
        $site = Site::first();

        if($site && Storage::exists($site->logo)) {
            $file = Storage::get($site->logo);
            $response = Response::make($file, 200);
            $response->header('Content-Type', 'image/jpeg');

            return $response;
        } else {
            return view('admin.errors.404');
        }
    }

    public function new_properties(Request $request)
    {
        $rules = [
            'new_properties_title' => 'required',
        ];

        $messages = [
            'new_properties_title.required' => 'El t&iacute;tulo es obligatorio'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $site = Site::first();
        if(!$site) {
            $site = new Site;
        }
        $site->new_properties_title = $request->new_properties_title;
        $site->new_properties_text = $request->new_properties_text;

        if(!$site->save()) {
            parent::message(
                'Hubo un error al guardar la secci&oacute;n "Nuevas Propiedades". Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La secci&oacute;n "Nuevas Propiedades" ha sido guardada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('SiteController@index'));
    }

    public function index_image(Request $request)
    {
        $rules = [
            'index_image' => 'mimes:jpeg,png,jpg,gif,svg|max:4096',
        ];

        $messages = [
            'index_image.mimes'    => 'El index_image debe tener formato JPEG, PNG, JPG, GIF o SVG',
            'index_image.max'      => 'El index_image debe pesar menos de 4 MB'
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $site = Site::first();
        if(!$site) {
            $site = new Site;
        }

        if($request->index_image) {
            $site->index_image = $request->index_image->store('images/home');
        }

        if(!$site->save()) {
            parent::message(
                'Hubo un error al guardar la imagen de inicio. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La imagen de inicio ha sido guardada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('SiteController@index'));
    }

    public function getIndexImage()
    {
        $site = Site::first();

        if($site && Storage::exists($site->index_image)) {
            $file = Storage::get($site->index_image);
            $response = Response::make($file, 200);
            $response->header('Content-Type', 'image/jpeg');

            return $response;
        } else {
            return view('errors.404');
        }
    }
}
