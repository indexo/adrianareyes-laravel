<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();

        return view('admin.countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'                      => 'required|max:45|unique:countries',
            'iso3'                      => 'required|max:3',
            'currency_symbol'           => 'required',
        ];

        $messages = [
            'name.required'             => 'El nombre es obligatorio',
            'name.max'                  => 'El nombre debe contener como m&aacute;ximo 45 caracteres',
            'name.unique'               => 'El nombre ya existe',
            'iso3.required'             => 'El nombre corto (ISO 3) es obligatorio',
            'iso3.max'                  => 'El nombre corto (ISO 3) debe contener como m&aacute;ximo 3 caracteres',
            'currency_symbol.required'  => 'El s&iacute;mbolo de moneda es obligatoria',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $country = new Country;
        $country->name = $request->name;
        $country->iso3 = $request->iso3;
        $country->phone_code = $request->phone_code;
        $country->currency = title_case($request->currency);
        $country->currency_symbol = $request->currency_symbol;
        $country->currency_exchange = (float) $request->currency_exchange;

        if(!$country->save()) {
            parent::message(
                'Hubo un error al ingresar el pa&iacute;s. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El pa&iacute;s ha sido creado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CountryController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Country $country)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Country $country)
    {
        return view('admin.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Country $country)
    {
        $rules = [
            'name'                      => 'required|max:45',
            'iso3'                      => 'required|max:3',
            'currency_symbol'           => 'required',
        ];

        if($request->name !== $country->name) {
            $rules['name'] = 'required|max:45|unique:countries';
        }

        $messages = [
            'name.required'             => 'El nombre es obligatorio',
            'name.max'                  => 'El nombre debe contener como m&aacute;ximo 45 caracteres',
            'name.unique'               => 'El nombre ya existe',
            'iso3.required'             => 'El nombre corto (ISO 3) es obligatorio',
            'iso3.max'                  => 'El nombre corto (ISO 3) debe contener como m&aacute;ximo 3 caracteres',
            'currency_symbol.required'  => 'El s&iacute;mbolo de moneda es obligatoria',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $country->name = $request->name;
        $country->iso3 = $request->iso3;
        $country->phone_code = $request->phone_code;
        $country->currency = title_case($request->currency);
        $country->currency_symbol = $request->currency_symbol;
        $country->currency_exchange = (float) $request->currency_exchange;

        if(!$country->save()) {
            parent::message(
                'Hubo un error al actualizar el pa&iacute;s. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El pa&iacute;s ha sido actualizado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CountryController@edit', $country));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Country $country)
    {
        if(!$country->delete()) {
            parent::message(
                'Hubo un problema al eliminar el pa&iacute;s. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'El pa&iacute;s ha sido eliminado correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('CountryController@index'));
    }
}
