<?php

namespace App\Http\Controllers;

use App\Environment;
use Illuminate\Http\Request;
use Validator;

class EnvironmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $environments = Environment::all();

        return view('admin.environments.index', compact('environments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.environments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:environments',
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $environment = new Environment;

        $environment->name = $request->name;
        $environment->type = $request->type;
        $environment->placeholder = $request->placeholder;

        if(!$environment->save()) {
            parent::message(
                'Hubo un problema al guardar el ambiente. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El ambiente se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EnvironmentController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Environment  $environment
     * @return \Illuminate\Http\Response
     */
    public function show(Environment $environment)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Environment  $environment
     * @return \Illuminate\Http\Response
     */
    public function edit(Environment $environment)
    {
        return view('admin.environments.edit', compact('environment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Environment  $environment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Environment $environment)
    {

        if($request->name != $environment->name) {
            $name = 'required|unique:environments';
        } else {
            $name = 'required';
        }
        $rules = [
            'name' => $name,
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $environment->name = $request->name;
        $environment->type = $request->type;
        $environment->placeholder = $request->placeholder;

        if(!$environment->save()) {
            parent::message(
                'Hubo un problema al guardar el ambiente. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El ambiente se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EnvironmentController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Environment  $environment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Environment $environment)
    {
        if(!$environment->delete()) {
            parent::message(
                'Hubo un problema al eliminar el ambiente. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El ambiente se elimin&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('EnvironmentController@index'));
    }
}
