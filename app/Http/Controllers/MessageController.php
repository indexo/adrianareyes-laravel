<?php

namespace App\Http\Controllers;

use App\Message;
use App\Mail\Message as MailMessage;
use App\Mail\Reply;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;
use Illuminate\View\View;
use Validator;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $messages = Message::where('parent_message_id', null)->get();

        return view('admin.messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $secret = env('RECAPTCHA_SECRET');
        $captcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$request->_recaptcha}");
        $json = \GuzzleHttp\json_decode($captcha);
        if(!$json->success) {
            parent::message(
                'No estamos seguros de su humanidad. En caso de persistir este mensaje contacte con un administrador.',
                'Error',
                'error'
            );
            return back()->withInput();
        } else {
            if($json->score < 0.5) {
                parent::message(
                    'No estamos seguros de su humanidad. En caso de persistir este mensaje contacte con un administrador.',
                    'Error',
                    'error'
                );
                return back()->withInput();
            }
        }
        $rules = [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email',
            'message'       => 'required'
        ];

        $messages = [
            'first_name.required'   => 'El nombre es obligatorio',
            'last_name.required'    => 'El apellido es obligatorio',
            'email.required'        => 'Un correo electrónico es obligatorio',
            'email.email'           => 'El correo electrónico no parece ser válido',
            'message.required'      => 'El mensaje es obligatorio'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            // Enviamos atras con los inputs y un mensaje de error
            return back()->withInput();
        }

        $message = new Message($request->all());
        $message->ip = $request->ip();
        if(\Auth::check()) {
            $message->user_id = \Auth::id();
        }

        try {
            Mail::to($message->email)->send(new MailMessage($message));
            $message->save();
        } catch(\Exception $e) {
            parent::message(
                'Hubo un error al enviar su mensaje. Intente nuevamente o contacte un administrador.',
                '¡Error!',
                'error'
            );

            return redirect()->back()->withInput();
        }

        parent::message(
            'Su mensaje ha sido enviado correctamente. Lo estaremos contactando a la brevedad.',
            '¡Excelente!',
            'success'
        );

        return redirect()->back();
    }

    /**
     * Store a reply from a message in storage.
     *
     * @param \App\Message $message
     * @param  \Illuminate\Http\Request  $request
     * @return View
     */
    public function reply(Message $message, Request $request)
    {
        $rules = [
            'message'       => 'required'
        ];

        $messages = [
            'message.required'      => 'El mensaje es obligatorio'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            // Enviamos atras con los inputs y un mensaje de error
            return back()->withInput();
        }

        $reply = new Message($request->all());

        $reply->ip                  = $request->ip();
        $reply->user_id             = \Auth::id();
        $reply->parent_message_id   = $message->id;

        try {
            Mail::to($message->email)->send(new Reply($reply));
            $reply->save();
        } catch(\Exception $e) {
            parent::message(
                'Hubo un error al responder el mensaje. Intente nuevamente o contacte un administrador.',
                '¡Error!',
                'error'
            );

            return redirect()->back()->withInput();
        }

        parent::message(
            'Su respuesta ha sido enviada correctamente.',
            '¡Excelente!',
            'success'
        );

        return redirect()->action('MessageController@show', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        if(!$message->childMessages) {
            return view('admin.errors.404');
        }

        return view('admin.messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        try {
            $message->delete();
        } catch(\Exception $e) {
            parent::message(
                'Hubo un error al eliminar el mensaje. Intente nuevamente o contacte un administrador.',
                '¡Error!',
                'error'
            );
        }

        parent::message(
            'Su respuesta ha sido eliminada correctamente.',
            '¡Excelente!',
            'success'
        );

        return redirect()->action('MessageController@index');
    }
}
