<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Carbon\Carbon;

class ActivityController extends Controller
{
	public function __construct()
	{
		Carbon::setLocale('es');
	}
    public function index()
    {
    	$activities = Activity::orderBy('created_at', 'desc')->get();

    	foreach($activities as $k => $v)
    	{

    		$activities[$k]->date = Carbon::parse($v->created_at);
    		$activities[$k]->timeago = $v->date->diffForHumans();
    	}

    	return view('admin.activities.index', compact('activities'));
    }
}
