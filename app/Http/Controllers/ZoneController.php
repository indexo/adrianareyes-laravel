<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Zone;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zones = Zone::all();

        return view('admin.zones.index', compact('zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.zones.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'                  => 'required',
            'location_id'         => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'location_id.required'=> 'El departamento es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $zone = new Zone;
        $zone->name = $request->name;
        $zone->location_id = $request->location_id;

        if(!$zone->save()) {
            parent::message(
                'Hubo un error al ingresar la zona. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La zona ha sido creada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ZoneController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Zone $zone)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Zone $zone)
    {
        return view('admin.zones.edit', compact('zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Zone $zone)
    {
        $rules = [
            'name'                  => 'required',
            'location_id'         => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'location_id.required'=> 'El departamento es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $zone->name = $request->name;
        $zone->location_id = $request->location_id;

        if(!$zone->save()) {
            parent::message(
                'Hubo un error al actualizar la zona. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La zona ha sido actualizada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ZoneController@edit', $zone));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Zone $zone)
    {
        if(!$zone->delete()) {
            parent::message(
                'Hubo un problema al eliminar la zona. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'La zona ha sido eliminada correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('ZoneController@index'));
    }
}
