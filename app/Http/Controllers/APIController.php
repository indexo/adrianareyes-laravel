<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Environment;
use App\Equipment;
use App\Exterior;
use App\ServiceAmenity;
use App\Property;
use App\Apartment;
use App\BusinessPremise;
use App\Countryside;
use App\House;
use App\Land;
use App\Rent;
use App\Sale;
use App\Activity;
use App\User;
use App\Calendar;
use App\Site;
use App\Service;
use App\Office;
use App\Customer;
use App\Country;
use App\Department;
use App\Location;
use App\Zone;
use App\Message;

class APIController extends Controller
{
    // Constructor
	public function __construct()
	{
        \Carbon\Carbon::setLocale('es');

        // Comprobar que la IP de la solicitud sea local o no devolver nada
        if(request()->ip() !== '127.0.0.1' || request()->ip() !== 'localhost' || request()->ip() !== '::1') { return; }
	}

    // Contadores - inicio
    // Ambientes
    public function totalEnvironments()
    {
        return Environment::count();
    }

    // Equipamientos
    public function totalEquipment()
    {
        return Equipment::count();
    }

    // Exteriores
    public function totalExterior()
    {
        return Exterior::count();
    }

    // Servicios y Amenities
    public function totalServicesAmenities()
    {
        return ServiceAmenity::count();
    }

    // Inmuebles
    public function totalProperties()
    {
        return Property::count();
    }

    // Apartamentos
    public function totalApartments()
    {
        return Apartment::count();
    }

    // Locales comerciales
    public function totalBusiness_premises()
    {
        return BusinessPremise::count();
    }

    // Chacras y Campos
    public function totalCountrysides()
    {
        return Countryside::count();
    }

    // Casas
    public function totalHouses()
    {
        return House::count();
    }

    // Terrenos
    public function totalLands()
    {
        return Land::count();
    }

    // Alquileres
    public function totalRents()
    {
        return Rent::where('rented', false)->count();
    }

    // Ventas
    public function totalSales()
    {
        return Sale::where('sold', false)->count();
    }

    // Inmuebles Alquilados
    public function totalRented()
    {
        return Rent::where('rented', true)->count();
    }

    // Inmuebles Vendidos
    public function totalSold()
    {
        return Sale::where('sold', true)->count();
    }

    // Administradores
    public function totalUsers()
    {
        return User::count();
    }

    // Administradores Activos
    public function totalActiveUsers()
    {
        $users = 0;

        foreach(User::all() as $u)
        {
            if($u->status()) {
                $users++;
            }
        }

        return $users;
    }

    // Administradores Inactivos
    public function totalInactiveUsers()
    {
        $users = 0;

        foreach(User::all() as $u)
        {
            if(!$u->status()) {
                $users++;
            }
        }

        return $users;
    }

    // Clientes
    public function totalCustomers()
    {
        return Customer::count();
    }
    // Contadores - final

    // getters ORM - inicio
    // Ambientes
    public function getEnvironments($total = null)
    {
    	return ($total)
			? Environment::take($total)->get()
			: Environment::all(); 
    }

    // Equipamientos
    public function getEquipment($total = null)
    {
    	return ($total)
			? Equipment::take($total)->get()
			: Equipment::all();
    }

    // Exteriores
    public function getExterior($total = null)
    {
    	return ($total)
			? Exterior::take($total)->get()
			: Exterior::all();
    }

    // Servicios y Amenities
    public function getServicesAmenities($total = null)
    {
    	return ($total)
			? ServiceAmenity::take($total)->get()
			: ServiceAmenity::all();
    }

    // Inmuebles
    public function getProperties($total = null)
    {
    	return ($total)
			? Property::take($total)->get()
			: Property::all();
    }

    // Apartamentos
    public function getApartments($total = null)
    {
    	return ($total)
			? Apartment::take($total)->get()
			: Apartment::all();
    }

    // Locales comerciales
    public function getBusiness_premises($total = null)
    {
    	return ($total)
			? BusinessPremise::take($total)->get()
			: BusinessPremise::all();
    }

    // Chacras y Campos
    public function getCountrysides($total = null)
    {
    	return ($total)
			? Countryside::take($total)->get()
			: Countryside::all();
    }

    // Casas
    public function getHouses($total = null)
    {
    	return ($total)
			? House::take($total)->get()
			: House::all();
    }

    // Terrenos
    public function getLands($total = null)
    {
    	return ($total)
			? Land::take($total)->get()
			: Land::all();
    }

    // Alquileres
    public function getRents($total = null)
    {
    	return ($total)
			? Rent::where('rented', false)->take($total)->get()
			: Rent::where('rented', false)->get();
    }

    // Ventas
    public function getSales($total = null)
    {
    	return ($total)
			? Sale::where('sold', false)->take($total)->get()
			: Sale::where('sold', false)->get();
    }

    // Inmuebles Alquilados
    public function getRented($total = null)
    {
    	return ($total)
			? Rent::where('rented', true)->take($total)->get()
			: Rent::where('rented', true)->get();
    }

    /* Inmuebles Vendidos
    public function getSold($total = null)
    {
    	return $total
            ? Sale::where('sold', true)->take($total)->get()
            : Sale::where('sold', true)->get();
    }*/

    // Actividades
    public function getActivities(Request $request)
    {
        $value = 'desc';
        switch($request->order_by)
        {
            case 'user':
                $order_by = 'user_id';
                break;
            case 'ip':
                $order_by = 'remote_addr';
                break;
            case 'country':
                $order_by = 'country';
                break;
            case 'action':
                $order_by = 'request_uri';
                break;
            default:
                $order_by = 'created_at';
        }

        $activities = Activity::orderBy($order_by, $value)->paginate(10);

	    foreach($activities as $k => $activity)
    	{
            $activities[$k]->timeago = $activity->created_at->diffForHumans();
    	}

    	return $activities;
    }

    // Casa de Invitados
    public function getGuestroom($id)
    {
        $data = null;
        $property = Property::find($id);

        if($property) {
            $house = $property->house;
            if($house) {
                $data = ($house->guest_room > 0);
            }
        }

        return $data;
    }

    // Administradores
    public function getUsers($total = null)
    {
        return $total
            ? User::orderBy('created_at', 'desc')->get()
            : User::orderBy('created_at', 'desc')->take($total)->get();
    }

    // Administradores Activos
    public function getActiveUsers($total = null)
    {
        $users = $total
            ? User::take($total)->get()
            : User::all();

        foreach($users as $k => $u)
        {
            if(!$u->status()) {
                unset($users[$k]);
            }
        }

        return $users;
    }

    // Administradores Inactivos
    public function getInactiveUsers($total = null)
    {
        $users = $total
            ? User::take($total)->get()
            : User::all();

        foreach($users as $k => $u)
        {
            if($u->status()) {
                unset($users[$k]);
            }
        }

        return $users;
    }

    // Eventos (total)
    public function getEvents($total = null) 
    {
        $events = $total 
            ? Calendar::orderBy('start_date', 'desc')->take($total)->get()
            : Calendar::orderBy('start_date', 'desc')->get();

        $new_events = [];
        foreach($events as $event)
        {
            $carbon_start_date = Carbon::parse($event->start_date . ' ' . $event->start_time);
            $end_time = $event->end_time ?? '00:00:00';
            $carbon_end_date = $event->end_date ?? Carbon::parse($event->start_date . ' ' . $end_time)->addDays(1);

            if(Carbon::now('America/Montevideo')->diffInSeconds($carbon_start_date, false) > 0) {
                $event->start = $carbon_start_date->diffForHumans();
                $event->duration = $carbon_start_date->diffInHours($carbon_end_date) . ' horas';
                $event->user = $event->user;

                $new_events[] = $event;
            }

        }

        return $new_events;
    }

    // Servicios Introducción
    public function getServicesIntroduction()
    {
        $site = Site::first();

        if($site) {
            return $site->services;
        }
        return null;
    }

    // Servicios
    public function getServices()
    {
        return Service::orderBy('created_at', 'desc')->take(5)->get();
    }

    // Oficinas
    public function getOffices($total = null)
    {
        $offices = $total
            ? Office::orderBy('created_at', 'desc')->take($total)->get()
            : Office::all();

        foreach($offices as $k => $o)
        {
            $offices[$k]->phonesHTML = $o->phonify();
        }

        return $offices;
    }

    // Clientes
    public function getCustomers($total = null)
    {
        return $total
            ? Customer::orderBy('created_at', 'desc')->take($total)->get()
            : Customer::all();
            
    }

    // Inmuebles para mostrar
    public function loadProperties($total = null)
    {
        // Listamos las propiedades
        $properties = $total
            ? Property::take($total)->get()
            : Property::all();

        if($properties) {
            // Iteración de propiedades - inicio
            foreach($properties as $k => $property)
            {
                // Ambientes - inicio
                // Obtenemos los ambientes
                $environments = $property->environments;
                if($environments) {
                    $env = [];
                    // Iteramos los ambientes
                    foreach($environments as $environment)
                    {
                        // Guardamos los datos fundamentales de cada ambiente
                        $env[] = [
                            'environment' => $environment->environment->name,
                            'value' => $environment->value
                        ];
                    }
                    // Asignamos dichos datos a la propiedad respectiva
                    $properties[$k]->environments = $env;
                }
                // Ambientes - final

                // Operación - inicio
                switch($property->tdo()) {
                    case 'rent-sale':
                        $rent = $property->rent;
                        $sale = $property->sale;
                        break;
                    case 'rent':
                        $rent = $property->rent;
                        break;
                    case 'sale':
                        $sale = $property->sale;
                        break;
                }
            }
            // Iteración de propiedades - final
        }
    }

    public function loadProperty($id)
    {
        $property = Property::find($id);

        // Comodidades
        $environments = $property->environments;
        if($environments) {
            $env = [];
            foreach($environments as $environment)
            {
                $env[] = [
                    'environment' => $environment->environment->name,
                    'value' => $environment->value == 'on'
                        ? true
                        : $environment->value
                ];
            }
            $property->environments = $env;
        } // /Comodidades

        // Exteriores
        $exteriors = $property->exteriors;
        if($exteriors) {
            $ext = [];
            foreach($exteriors as $exterior)
            {
                $ext[] = [
                    'exterior' => $exterior->exterior->name,
                    'value' => $exterior->value == 'on'
                        ? true
                        : $exterior->value
                ];
            }
            $property->exteriors = $ext;
        } // /Exteriores

        // Equipamientos
        $equipments = $property->equipments;
        if($equipments) {
            $eq = [];
            foreach($equipments as $equipment)
            {
                $eq[] = [
                    'equipment' => $equipment->equipment->name,
                    'value' => $equipment->value == 'on'? true
                        : $equipment->value
                ];
            }
            $property->equipments = $eq;
        } // /Equipamientos

        // Servicios y comodidades
        $services_amenities = $property->services_amenities;
        if($services_amenities) {
            $sa = [];
            foreach($services_amenities as $service_amenity)
            {
                $sa[] = [
                    'service_amenity' => $service_amenity->service_amenity->name,
                    'value' => $service_amenity->value == 'on'? true
                        : $service_amenity->value
                ];
            }
            $property->services_amenities = $sa;
        } // /Servicios y comodidades

        // Imágenes
        $images = $property->images;
        if($images) {
            $property->images = $property->images;
        } // /Imágenes

        // Tipo de operacion
        $property->tdo = $property->tdo();
        switch($property->tdo()) {
            case 'rent-sale':
                $property->rent = $property->rent;
                $property->sale = $property->sale;
                break;
            case 'rent':
                $property->rent = $property->rent;
                break;
            case 'sale':
                $property->sale = $property->sale;
                break;
        } // /Tipo de Operacion

        $property->tdi = $property->tdi();

        return [ 'property' => $property];
    }

    public function lastProperties($total = null)
    {
        // Listamos las propiedades
        $properties = $total
            ? Property::take($total)->orderBy('created_at', 'desc')->get()
            : Property::orderBy('created_at', 'desc')->get();

        if(!$properties) {
            return;
        }
            
        // Iteración de propiedades - inicio
        foreach($properties as $k => $property)
        {
            // Sólo aplica a apartamentos y casas
            if($property->tdi() == 'apartment' || $property->tdi() == 'house') {
                // Ambientes - inicio
                // Obtenemos los ambientes
                $environments = $property->environments;
                if($environments) {
                    $env = [];
                    // Iteramos los ambientes
                    foreach($environments as $environment)
                    {
                        // Guardamos los datos fundamentales de cada ambiente
                        $env[] = [
                            'environment' => $environment->environment->name,
                            'value' => $environment->value
                        ];
                    }
                    // Asignamos dichos datos a la propiedad respectiva
                    $properties[$k]->environments = $env;
                }
                // Ambientes - final

                // Operación - inicio
                $properties[$k]->tdo = $property->tdo();

                switch($property->tdo()) {
                    case 'rent-sale':
                        $properties[$k]->rent = $property->rent;
                        $properties[$k]->sale = $property->sale;
                        break;
                    case 'rent':
                        $properties[$k]->rent = $property->rent;
                        break;
                    case 'sale':
                        $properties[$k]->sale = $property->sale;
                        break;
                }
                // Operación - final

                // Imágenes
                $properties[$k]->images = $property->images;




            } else {
                unset($properties[$k]);
            }
        }
        // Iteración de propiedades - final
dd($properties);
        return $properties;
    }

    // Países
    public function getCountries($id = null)
    {
        $countries = $id
            ? Country::findOrFail($id)
            : Country::all();

        if($id) {
            $countries->departments = $countries->departments;            
        } else {
            foreach($countries as $k => $country)
            {
                $countries[$k]->departments = $country->departments;
            }
        }

        return $countries;

    }

    // País
    public function getCountry($id)
    {
        $country = Country::findOrFail($id);
        $country->departments = $country->departments;
        
        return $country;
    }

    // Departamentos
    public function getDepartments($id = null)
    {
        $departments = $id
            ? Department::findOrFail($id)
            : Department::all();

        if($id) { 
            $departments->localities = $departments->localities;            
        } else {
            foreach($departments as $k => $department)
            {
                $departments[$k]->localities = $department->localities;
            }
        }

        return $departments;
    }

    // Departamento
    public function getDepartment($id)
    {
        $department = Department::findOrFail($id);
        $department->localities = $department->localities;
        
        return $department;
    }

    // Localidades
    public function getLocalities($id = null)
    {
        $localities = $id
            ? Location::findOrFail($id)
            : Location::all();

         if($id) {
            $localities->zones = $localities->zones;
        } else {
            foreach($localities as $k => $location)
            {
                $localities[$k]->zones = $location->zones;
            }
        }

        return $localities;
    }

    // Localidades
    public function getLocation($id)
    {
        $location = Location::findOrFail($id);
        $location->zones = $location->zones;
        
        return $location;
    }

    // Zonas
    public function getZones($id = null)
    {
        return $id
            ? Zone::find($id)
            : Zone::all();
    }

    // Mensajes
    public function getMessages($id = null)
    {
        return $id
            ? Message::find($id)
            : Message::all();
    }

    public function getUnansweredMessages()
    {
        $messages = Message::all();

        foreach($messages as $k => $message)
        {
            if((boolean) $message->parent_message || $message->replies()->count()) {
                unset($messages[$k]);
                continue;
            }
        }

        return $messages;
    }

    // getters ORM - final
	
	public function getUsageLocation()
	{
		$activities = \DB::table('activities')
			->select(\DB::raw('country, count(*) as activities'))
			->whereNotNull('country')
			->groupBy('country')->get();
		$data[0] = [ 'País', 'Ingresos' ];
		foreach($activities as $activity)
		{
			$data[] = [  $activity->country, $activity->activities ];
		}
		
		return $data;
	}

}


