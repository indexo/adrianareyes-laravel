<?php

namespace App\Http\Controllers;

use App\Office;
use Illuminate\Http\Request;
use Validator;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offices = Office::all();

        return view('admin.offices.index', compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.offices.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'address'          => 'required|unique:offices',
        ];

        $messages = [
            'address.required' => 'La direcci&oacute;n es obligatoria',
            'address.unique'   => 'Una oficina con esa direcci&oacute;n ya existe',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $office = new Office;
        $office->address            = $request->address;
        $office->city               = $request->city;
        $office->country            = $request->country;
        $office->phones             = $request->phones;
        $office->emails             = $request->emails;
        $office->google_map_code    = $request->google_map_code;
        $office->open_mon           = $request->open_mon;
        $office->close_mon          = $request->close_mon;
        $office->open_tue           = $request->open_tue;
        $office->close_tue          = $request->close_tue;
        $office->open_wed           = $request->open_wed;
        $office->close_wed          = $request->close_wed;
        $office->open_thu           = $request->open_thu;
        $office->close_thu          = $request->close_thu;
        $office->open_fri           = $request->open_fri;
        $office->close_fri          = $request->close_fri;
        $office->open_sat           = $request->open_sat;
        $office->close_sat          = $request->close_sat;
        $office->open_sun           = $request->open_sun;
        $office->close_sun          = $request->close_sun;
        $office->schedules          = $request->schedules;
        $office->is_main            = (boolean) $request->is_main;     

        foreach(Office::all() as $o)
        {
            $o->is_main = null;
            $o->save();
        }  

        if(!$office->save()) {
            parent::message(
                'Hubo un error al ingresar la oficina. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La oficina ha sido creada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('OfficeController@edit', $office));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Office $office)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Office $office)
    {
        return view('admin.offices.edit', compact('office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Office $office)
    {
        $rules = [
            'address'          => 'required',
        ];

        if($request->address !== $office->address) {
            $rules['address'] = 'required|unique:offices';
        }

        $messages = [
            'address.required' => 'La direcci&oacute;n es obligatoria',
            'address.unique'   => 'Una oficina con esa direcci&oacute;n ya existe',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $office->address            = $request->address;
        $office->city               = $request->city;
        $office->country            = $request->country;
        $office->phones             = $request->phones;
        $office->emails             = $request->emails;
        $office->google_map_code    = $request->google_map_code;
        $office->open_mon           = $request->open_mon;
        $office->close_mon          = $request->close_mon;
        $office->open_tue           = $request->open_tue;
        $office->close_tue          = $request->close_tue;
        $office->open_wed           = $request->open_wed;
        $office->close_wed          = $request->close_wed;
        $office->open_thu           = $request->open_thu;
        $office->close_thu          = $request->close_thu;
        $office->open_fri           = $request->open_fri;
        $office->close_fri          = $request->close_fri;
        $office->open_sat           = $request->open_sat;
        $office->close_sat          = $request->close_sat;
        $office->open_sun           = $request->open_sun;
        $office->close_sun          = $request->close_sun;
        $office->schedules          = $request->schedules;
        $office->is_main            = (boolean) $request->is_main;

        foreach(Office::all() as $o)
        {
            $o->is_main = null;
            $o->save();
        } 

        if(!$office->save()) {
            parent::message(
                'Hubo un error al actualizar la oficina. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La oficina ha sido actualizada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('OfficeController@edit', $office));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Office $office)
    {
        if(!$office->delete()) {
            parent::message(
                'Hubo un problema al eliminar la oficina. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'La oficina ha sido eliminada correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('OfficeController@index'));
    }

    public function phonify($phones)
    {
        
    }
}
