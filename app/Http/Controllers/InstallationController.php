<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use Storage;
use Validator;
use App\Activity;

class InstallationController extends Controller
{
    public function index()
    {
    	return view('admin.installation.index');
    }

    public function createDB(Request $request)
    {
    	// Reglas de validación del formulario
    	$rules = [
    		'database_host' => 'required',
    		'database_name' => 'required',
    		'database_user' => 'required',
    	];

    	// Mensajes para las reglas definidas
    	$messages = [
    		'database_host.required' => 'El host es requerido',
    		'database_name.required' => 'El nombre es requerido',
    		'database_user.required' => 'El usuario es requerido',
    	]; 

    	// Validación de la información ingresada ($request) con las reglas y mensajes
    	$validator = Validator::make(
    		$request->all(), 
    		$rules, 
    		$messages
    	);

    	// Si la validación falla volvemos al formulario y mostramos error
    	if($validator->fails()) {
    		parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );
    		return back()->withInput();
    	}

    	// Llamamos a la función que escribe en el archivo .env pasando la configuración necesaria
    	$env = $this->write_env([
    		'DB_HOST' 	  => $request->database_host,
    		'DB_DATABASE' => $request->database_name,
    		'DB_USERNAME' => $request->database_user,
    		'DB_PASSWORD' => $request->database_password
    	]);

    	// Si falla volvemos con error
    	if(!$env) {
    		parent::message(
	            'Hubo un problema al actualizar la configuraci&oacute;n',
	            '¡Error!',
	            'error'
	        );
    		return back()->withInput();
    	}
    	
    	// Intentamos crear instancia de la conexión con la BD o volvemos con error
    	try {
    		$mysqli = new \mysqli(
	    		$request->database_host, 
	    		$request->database_user, 
	    		$request->database_password
	    	);
    	} catch (\Exception $e) {
    		parent::message(
	            'No se pudo conectar a la base de datos. Compruebe la informaci&oacute;n de la misma y vuelva a intentarlo',
	            '¡Error!',
	            'error'
	        );
    		return back()->withInput();
    	}

    	// Creamos la query que creará la Base de datos si la BD no existe
    	if(!$this->Db()) {
    		$query = 'CREATE DATABASE ' . $request->database_name;
    	} else {
    		parent::message(
	            'La configuraci&oacute;n se ha actualizado',
	            '¡Excelente!',
	            'success'
	        );
	    	return redirect(action('InstallationController@index'));
    	}

    	if(!$mysqli->query($query)) {
    		parent::message(
	            'La configuraci&oacute;n no se pudo actualizar',
	            '¡Error!',
	            'error'
	        );
    		return back()->withInput();
    	}

    	// Cerramos la conexión a la BD
    	$mysqli->close();

    	if(!$this->Db()) {
    		parent::message(
	            'La configuraci&oacute;n se ha actualizado, pero la conexi&oacute;n a la base de datos no se realiz&oacute; como se esperaba',
	            '¡Error!',
	            'error'
	        );
    		return back()->withInput();
    	}

    	parent::message(
            'La configuraci&oacute;n se ha actualizado y la conexi&oacute;n a la base de datos se realiz&oacute; correctamente',
            '¡Excelente!',
            'success'
        );
    	return redirect(action('InstallationController@index'));

    }

    // Migra las tablas de la base de datos
    public function migrateTables(Request $request)
    {
    	if($this->Db()) {
    		\Artisan::call('migrate');

    		parent::message(
	            'Se ha migrado la base de datos correctamente',
	            '¡Excelente!',
	            'success'
	        );
    		return redirect(action('InstallationController@index'));
    	}

    	parent::message(
            'Error al migrar la base de datos.',
            '¡Error!',
            'error'
        );
    	return back();
    }

    public function resetConf(Request $request)
    {
    	$env = $this->write_env();

    	if(!$env) {

    		parent::message(
	            'Hubo un problema al reiniciar la configuraci&oacute;n',
	            '¡Error!',
	            'error'
	        );
    		return back()->withInput();
    	}

    	parent::message(
            'La configuraci&oacute;n se ha reiniciado',
            '¡Excelente!',
            'success'
        );
    	return redirect(action('InstallationController@index'));
    }

    public function resetTables(Request $request)
    {
    	if($this->Db()) {
    		\Artisan::call('migrate:reset');

    		parent::message(
	            'Se ha reiniciado la base de datos correctamente',
	            '¡Excelente!',
	            'success'
	        );
    		return redirect(action('InstallationController@index'));
    	}

    	parent::message(
            'Error al reiniciar la base de datos.',
            '¡Error!',
            'error'
        );
    	return back();
    }

    public function exportDB()
    {
        $database_host = env('DB_HOST');
        $database_name = env('DB_DATABASE');
        $database_user = env('DB_USERNAME');
        $database_password = env('DB_PASSWORD');

        $filename = 'respaldos/' . Carbon::now('America/Montevideo')->format('his-dmY') . '.sql';
        $cmd = 'mysqldump -u ' . $database_user . ' -p' . $database_password . ' ' . $database_name . ' ../> ' . $filename;
        exec($cmd);

        parent::message(
            "El comando de exportaci&oacute;n de la base de datos se ha ejecutado. Verifique la ruta <strong>$filename</strong>",
            '¡&Eacute;xito!',
            'success'
        );

        return back();
    }

    public function erase_activities()
    {
        try {
            $activities = Activity::where('created_at', '<', Carbon::now()->subDays(3)->format('Y-m-d h:i:s'))->delete();
        } catch(\Exception $e) {

            parent::message(
                "Las actividades no pudieron ser eliminadas. Intente nuevamente o contacte un administrador",
                'Error!',
                'error'
            );

            return redirect()->back();
        }

        parent::message(
            "Las actividades fueron eliminadas. Se conservaron las &uacute;ltimas 72 horas",
            '¡&Eacute;xito!',
            'success'
        );

        return redirect()->back();

    }

    public function configEmail(Request $request)
    {
        // Reglas de validación del formulario
        $rules = [
            'domain' => 'required',
            'password' => 'required',
        ];

        // Mensajes para las reglas definidas
        $messages = [
            'domain.required' => 'El dominio es requerido',
            'password.required' => 'La contrase&ntilde;a es requerida'
        ];

        // Validación de la información ingresada ($request) con las reglas y mensajes
        $validator = Validator::make(
            $request->all(),
            $rules,
            $messages
        );

        // Si la validación falla volvemos al formulario y mostramos error
        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );
            return back()->withInput();
        }

        // Llamamos a la función que escribe en el archivo .env pasando la configuración necesaria
        $env = $this->write_env([
            'MAILGUN_DOMAIN' 	  => $request->domain,
            'MAILGUN_SECRET' => $request->password,
            'MAIL_USERNAME' => $request->email_from,
            'MAIL_PASSWORD' => $request->email_password
        ]);

        // Si falla volvemos con error
        if(!$env) {
            parent::message(
                'Hubo un problema al actualizar la configuraci&oacute;n',
                '¡Error!',
                'error'
            );
            return back()->withInput();
        }

        parent::message(
            'La configuraci&oacute;n se ha actualizado correctamente.',
            '¡Excelente!',
            'success'
        );
        return redirect(action('InstallationController@index'));

    }









    protected function Db()
	{
		try {
	    	\DB::connection()->getPdo();
	    	return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	private function write_env($conf = null)
	{
		$env_string = '';
		$env_path = base_path() . DIRECTORY_SEPARATOR;

		if(isset($conf)) {
	    	$env = file_get_contents($env_path . '.env');

            $line_separator = DIRECTORY_SEPARATOR == "\\" ? "\n" : "\r\n";

	    	$env_array = explode($line_separator, $env);

	    	$env = [];

	    	foreach($env_array as $k => $line)
	    	{
	    	    if(strlen($line) > 0) {
	    			$array = explode("=", $line);

                    if(array_key_exists(0, $array) && array_key_exists(1, $array)) {
                        foreach($conf as $ck => $input)
                        {
                            if(strstr($input, " ")) {
                                $input = '"'. $input .'"';
                            }
                            if($array[0] == $ck) {
                                $array[1] = $input;
                            }
                        }

	    			    $env[$k] = $array[0] . '=' . trim($array[1]) . $line_separator;
                    }
	    		} else {
                    $env[$k] = $line_separator;
                }
	    	}

	    	foreach($env as $line)
	    	{
	    		$env_string .= $line;
	    	}
	    } else {
	    	$env_string = file_get_contents($env_path .'.env.original');
	    }

    	if(!file_put_contents($env_path . '.env', $env_string)) {
    		return false;
    	}

    	return true;
	}
}
