<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'          => 'required|max:45|unique:services',
        ];

        $messages = [
            'title.required' => 'El t&iacute;tulo es obligatorio',
            'title.max'      => 'El t&iacute;tulo debe contener como m&aacute;ximo 45 caracteres',
            'title.unique'   => 'Un servicio con ese nombre ya existe',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $service = new Service;
        $service->title = $request->title;
        $service->description = $request->description;
        $service->icon = $request->icon;           

        if(!$service->save()) {
            parent::message(
                'Hubo un error al ingresar el servicio. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El servicio ha sido creado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ServiceController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Service $service)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Service $service)
    {
        $rules = [
            'title'          => 'required|max:45',
        ];

        if($request->title !== $service->title) {
            $rules['title'] = 'required|max:45|unique:services';
        }

        $messages = [
            'title.required' => 'El t&iacute;tulo es obligatorio',
            'title.max'      => 'El t&iacute;tulo debe contener como m&aacute;ximo 45 caracteres',
            'title.unique'   => 'Un servicio con ese nombre ya existe',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $service->title = $request->title;
        $service->description = $request->description;
        $service->icon = $request->icon;

        if(!$service->save()) {
            parent::message(
                'Hubo un error al actualizar el servicio. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El servicio ha sido actualizado correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ServiceController@edit', $service));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Service $service)
    {
        if(!$service->delete()) {
            parent::message(
                'Hubo un problema al eliminar el servicio. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'El servicio ha sido eliminado correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('ServiceController@index'));
    }
}
