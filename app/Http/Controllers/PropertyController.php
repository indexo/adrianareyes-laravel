<?php

namespace App\Http\Controllers;

use App\Property;
use App\Apartment;
use App\BusinessPremise;
use App\Countryside;
use App\House;
use App\Land;
use App\Rent;
use App\Sale;
use App\Environment;
use App\PropertyEnvironment;
use App\Equipment;
use App\PropertyEquipment;
use App\Exterior;
use App\PropertyExterior;
use App\ServiceAmenity;
use App\PropertyServiceAmenity;
use App\User;
use App\Image as Img;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Storage;
use Response;
use Intervention\Image\ImageManagerStatic as Image;

class PropertyController extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('es');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $properties = Property::all();

        return view('admin.properties.index', compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.properties.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // Se definen las reglas para los formularios
        $rules = [
            'currency_prop' => 'required',
            /*'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10000',*/
            'images' => 'image|max:10000',
            'zone' => 'required',
        ];
        // Se definen los mensajes para los errores de los formularios
        $messages = [
            'currency_sale.required' => 'El tipo de moneda para alquiler es obligatorio',
            'currency_rent.required' => 'El tipo de moneda para venta es obligatorio',
            'currency_prop.required' => 'El tipo de moneda para gastos es obligatorio',
            'images.*.image' => 'Uno de los archivos cargados no es una imagen',
            'images.*.max' => 'Las im&aacute;genes deben pesar menos de 2 MB cada una',
            'images.*' => 'Hubo un error al subir una imagen',
            'zone.required' => 'La zona es obligatoria',
        ];

        if($request->tdo == 'sale' || $request->tdo == 'rent-sale') {
            $rules['price'] = 'required';
            $messages['price.required'] = 'El precio es obligatorio';
        }

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            // Enviamos atras con los inputs y un mensaje de error
            return back()->withInput();
        }

        // Creamos una instancia de la propiedad (App\Property)
        $property = new Property;
        
        // Asignamos la información que tienen todas los inmuebles
        $property->currency_country_id  = $request->currency_prop;
        $property->zone_id              = $request->zone;
        $property->address              = $request->address;
        $property->description          = $request->description;
        $property->notes                = $request->notes;
        $property->featured             = $request->featured === 'on' ? true : false;

        // Creamos instancias de los respectivos tipos de operación
        if($request->tdo === 'rent') {
            // Alquiler
            $rent = new Rent;

            // Si se marcó el inmueble como alquilado
            $rent->rented = (boolean) $request->rented;
        } elseif($request->tdo === 'rent-sale') {
            // Alquiler y Venta
            $rent = new Rent;
            $sale = new Sale;

            // Si se marcó el inmuebe como alquilado o vendido
            $rent->rented = (boolean) $request->rented;
            $sale->sold = (boolean) $request->sold;
        } elseif($request->tdo === 'sale') {
            // Venta
            $sale = new Sale;

            // Si se marcó el inmueble como vendido
            $sale->sold = (boolean) $request->sold;
        }

        // Si existe la instancia de Renta App\Rent
        if(isset($rent)) {
            // Si el tipo de inmueble es un local comercial se asignan distintos datos que si es un apartamento o casa

            // Asignamos el tipo de moneda a TODOS
            $rent->currency_country_id = $request->currency_rent;

            if($request->tdi == 'business_premise') {
                // Aplica SÓLO a locales comerciales

                $rent->annual    = $this->get_int($request->annual);
                $rent->temporary = $this->get_int($request->temporary);
            } else { 
                // Aplica a casas y apartamentos

                $rent->january                      = $this->get_int($request->january);
                $rent->january_1st_fortnight        = $this->get_int($request->january_1st_fortnight);
                $rent->january_2nd_fortnight        = $this->get_int($request->january_2nd_fortnight);
                $rent->february                     = $this->get_int($request->february);
                $rent->february_1st_fortnight       = $this->get_int($request->february_1st_fortnight);
                $rent->february_2nd_fortnight       = $this->get_int($request->february_2nd_fortnight);
                $rent->season                       = $this->get_int($request->season);
                $rent->easter                       = $this->get_int($request->easter);
                $rent->annual                       = $this->get_int($request->annual);
                $rent->winter                       = $this->get_int($request->winter);
                $rent->high_season_daily            = $this->get_int($request->high_season_daily);
                $rent->high_season_daily_min_days   = $this->get_int($request->high_season_daily_min_days);
                $rent->low_season_daily             = $this->get_int($request->low_season_daily);
                $rent->low_season_daily_min_days    = $this->get_int($request->low_season_daily_min_days);
            }
        }

        // Información compartida por todas las propiedades en venta
        if(isset($sale)) {
            $sale->currency_country_id = $request->currency_sale;
            $sale->price               = $this->get_int($request->price);
            $sale->financing           = (boolean) $request->financing;
        }

        // Guardamos la propiedad o enviamos atrás con un mensaje de error
        if(!$property->save()) {
            parent::message(
                'La propiedad no se pudo guardar. Intente nuevamente o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        // Guardamos el objeto App\Rent o enviamos atrás con un mensaje de error
        if(isset($rent)) {
            // Asignamos el ID del inmueble
            $rent->property_id = $property->id;

            // Si no se puede guardar borramos el inmueble y volvemos con input y mensaje de error
            if(!$rent->save()) {
                parent::message(
                    'El inmueble se pudo guardar. Intente nuevamente o contacte con un administrador',
                    '¡Error!',
                    'error'
                );

                $property->delete();

                return back()->withInput();
            }
        }

        // Guardamos el objeto App\Sale o enviamos atrás con un mensaje de error
        if(isset($sale)) {
            // Asignamos el ID del inmueble
            $sale->property_id = $property->id;

            // Si no se puede guardar borramos el inmueble y volvemos con input y mensaje de error
            if(!$sale->save()) {
                parent::message(
                    'El inmueble no se pudo guardar. Intente nuevamente o contacte con un administrador',
                    '¡Error!',
                    'error'
                );

                $property->delete();

                return back()->withInput();
            }
        }

        // Creamos las instancias de los respectivos tipos de inmueble y asignamos sus datos
        switch($request->tdi) {
            // Apartamento
            case 'apartment':

                // Asignamos información que tienen todos los inmuebles que son apartamentos
                $property->built_footage             = (integer) $request->built_footage;
                $property->distance_from_sea         = (integer) $request->distance_from_sea;
                $property->expenses                  = $this->get_int($request->expenses);
                $property->expenses_range            = $request->expenses_range;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Creamos la instancia App\Apartment
                $property_type = new Apartment;

                break;

            // Local comercial
            case 'business_premise':

                // Asignamos información que tienen todos los inmuebles que son locales comerciales
                $property->area                      = (integer) $request->area;
                $property->expenses                  = $this->get_int($request->expenses);
                $property->expenses_range            = $request->expenses_range;

                // Creamos la instancia de App\BusinessPremise
                $property_type = new BusinessPremise;
                $property_type->floors      = (integer) $request->floors;
                $property_type->low_level   = (integer) $request->low_level;
                $property_type->mezzanine   = (integer) $request->mezzanine;
                $property_type->subsoil     = (integer) $request->subsoil;
                $property_type->bathrooms   = (integer) $request->bathrooms;
                $property_type->kitchenette = (boolean) $request->kitchenette;
                $property_type->private     = (boolean) $request->private;
                $property_type->kitchen     = (boolean) $request->kitchen;

                break;

            // Chacra / Campo
            case 'countryside':

                // Asignamos información que tienen todos los inmuebles que son chacras y campos
                $property->area                      = (integer) $request->area;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Creamos la instancia de App\Countryside
                $property_type = new Countryside;

                // Asignamos información que tienen SÓLO las chacras y campos
                $property_type->location        = $request->property_location;
                $property_type->access          = $request->access;
                $property_type->topography      = $request->topography;
                $property_type->activity        = $request->activity;
                $property_type->productivity    = $request->productivity;

                // Si se marcó la opción de "cuarto de invitados" asignamos su información
                if($request->guest_room) {
                    $property_type->guest_room              = (boolean) $request->guest_room;
                    $property_type->guest_room_rooms        = (integer) $request->guest_room_rooms;
                    $property_type->guest_room_bathrooms    = (integer) $request->guest_room_bathrooms;
                    $property_type->guest_room_kitchen      = (boolean) $request->guest_room_kitchen;
                    $property_type->guest_room_kitchenette  = (boolean) $request->guest_room_kitchenette;
                    $property_type->guest_room_living_room  = (boolean) $request->guest_room_living_room;
                }

                // Especificaciones

                // Si se marcó "Tajamar"
                if($request->spec_cutwater) {
                    $property_type->spec_cutwater = (boolean) $request->spec_cutwater;
                    $property_type->spec_cutwater_description = $request->spec_cutwater_description;
                }

                // Si se marcó "Alambrado"
                if($request->spec_wiring) {
                    $property_type->spec_wiring = (boolean) $request->spec_wiring;
                    $property_type->spec_wiring_description = $request->spec_wiring_description;
                }

                // Si se marcó "Ríos y arroyos"
                if($request->spec_streams) {
                    $property_type->spec_rivers_streams = (boolean) $request->spec_streams;
                    $property_type->spec_rivers_streams_description = $request->spec_streams_description;
                }

                // Si se marcó "Montes"
                if($request->spec_hills) {
                    $property_type->spec_hills = (boolean) $request->spec_hills;
                    $property_type->spec_hills_description = $request->spec_hills_description;
                }

                // Si se marcó "Luz"
                if($request->spec_light) {
                    $property_type->spec_light = (boolean) $request->spec_light;
                    $property_type->spec_light_description = $request->spec_light_description;
                }

                // Si se marcó la opción de "casa" asignamos su información
                if($request->house) {
                    $property_type->house = true;

                    // Iteramos los ambientes
                    foreach($request->environments as $k => $env)
                    {
                        // Creamos una nueva instancia por cada ambiente iterado
                        $environment = new PropertyEnvironment;

                        // Obtenemos la propiedad para la cual se va a ingresar la información
                        $get_env = Environment::find($k);

                        // Asignamos el ID del ambiente, el ID de la propiedad, y su valor
                        $environment->environment_id = $k;
                        $environment->property_id = $property->id;

                        // Asignamos un valor booleano o entero dependiendo del tipo de dato de dicha propiedad
                        $environment->value = $get_env->type == 'yesno'
                            ? (boolean) $env
                            : (integer) $env;

                        // Guardamos el ambiente
                        $environment->save();
                    }

                    // Iteramos los exteriores
                    foreach($request->exterior as $k => $ext)
                    {
                        // Creamos una nueva instancia por cada exterior iterado
                        $exterior = new PropertyExterior;

                        // Obtenemos la propiedad para la cual se va a ingresar la información
                        $get_ext = Environment::find($k);

                        // Asignamos el ID del exterior, el ID de la propiedad, y su valor
                        $exterior->exterior_id = $k;
                        $exterior->property_id = $property->id;
                        $exterior->value = $get_ext->type == 'yesno'
                            ? (boolean) $ext
                            : (integer) $ext;

                        // Guardamos el exterior
                        $exterior->save();
                    }
                }

                // Si se marcó la opción de "galpones"
                if($request->sheds) {
                    $property_type->sheds = (boolean) $request->sheds;
                    $property_type->sheds_description = $request->sheds_description;
                }

                // Si se marcó la opción de "establos"
                if($request->stables) {
                    $property_type->stables = (boolean) $request->stables;
                    $property_type->stables_description = $request->stables_description;
                }

                // Si se marcó la opción de "otros"
                if($request->others) {
                    $property_type->others = (boolean) $request->others;
                    $property_type->others_description = $request->others_description;
                }

                break;

            // Casa
            case 'house':

                // Asignamos información que tienen todos los inmuebles que son casas
                $property->ground_footage               = (integer) $request->ground_footage;
                $property->built_footage                = (integer) $request->built_footage;
                $property->distance_from_sea            = (integer) $request->distance_from_sea;
                $property->distance_from_beach          = (integer) $request->distance_from_beach;
                $property->expenses_property_tax        = $this->get_int($request->expenses_property_tax);
                $property->expenses_school_tax          = $this->get_int($request->expenses_school_tax);
                $property->expenses_gardener            = $this->get_int($request->expenses_gardener);
                $property->expenses_pool_maintenance    = $this->get_int($request->expenses_pool_maintenance);
                $property->expenses_alarm_vigilance     = $this->get_int($request->expenses_alarm_vigilance);

                // Creamos la instancia de App\House
                $property_type = new House;

                // Si se marcó la opción de "cuarto de invitados" asignamos su información
                if($request->guest_room) {
                    $property_type->guest_room               = (boolean) $request->guest_room;
                    $property_type->guest_room_rooms         = (integer) $request->guest_room_rooms;
                    $property_type->guest_room_bathrooms     = (integer) $request->guest_room_bathrooms;
                    $property_type->guest_room_kitchen       = (boolean) $request->guest_room_kitchen;
                    $property_type->guest_room_kitchenette   = (boolean) $request->guest_room_kitchenette;
                    $property_type->guest_room_living_room   = (boolean) $request->guest_room_living_room;
                }

                break;

            // Terreno
            case 'land':

                // Asignamos información que tienen todos los inmuebles que son terrenos
                $property->distance_from_sea         = (integer) $request->distance_from_sea;
                $property->area                      = (integer) $request->area;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Creamos la instancia de App\Land
                $property_type = new Land;
                $property_type->frontyard       = (integer) $request->frontyard;
                $property_type->backyard        = (integer) $request->backyard;
                $property_type->sides           = (integer) $request->sides;
                $property_type->FOT             = (integer) $request->FOT;
                $property_type->FOS             = (integer) $request->FOS;
                $property_type->ret_frontyard   = (integer) $request->ret_frontyard;
                $property_type->ret_backyard    = (integer) $request->ret_backyard;
                $property_type->ret_bilateral   = (integer) $request->ret_bilateral;

                break;
        }

        // Actualizamos la propiedad o enviamos atrás con un mensaje de error
        if(!$property->save()) {
            parent::message(
                'El inmueble no se pudo guardar. Intente nuevamente o cont&aacute',
                '¡Error!',
                'error'
            );

            $property->delete();

            return back()->withInput();
        }

        // Sólo si el inmueble NO es una chacra o campo
        if(gettype($property_type) !== "App\Countryside")
        {
            // Ambientes
            // Si existen ambientes
            if($request->environments) {
                // Iteramos los ambientes
                foreach($request->environments as $k => $env)
                {
                    // Si el ambiente está definido
                    if($env) {

                        $environment = new PropertyEnvironment;

                        $environment->environment_id = $k;
                        $environment->property_id = $property->id;
                        $environment->value = $env;

                        $environment->save();
                    }
                }

            }

            // Equipamientos
            if($request->equipment) {
                foreach($request->equipment as $k => $eq)
                {
                    if($eq) {
                        $equipment = new PropertyEquipment;

                        $equipment->equipment_id = $k;
                        $equipment->property_id = $property->id;
                        $equipment->value = $eq;

                        $equipment->save();
                    }
                }
            }

            // Exteriores
            if($request->exterior) {
                foreach($request->exterior as $k => $ext)
                {
                    if($ext) {
                        $exterior = new PropertyExterior;

                        $exterior->exterior_id = $k;
                        $exterior->property_id = $property->id;
                        $exterior->value = $ext;

                        $exterior->save();
                    }
                }
            }

            // Servicios y Amenities
            if($request->services_amenities) {
                foreach($request->services_amenities as $k => $sa)
                {
                    if($sa) {
                        $service_amenity = new PropertyServiceAmenity;

                        $service_amenity->service_amenity_id = $k;
                        $service_amenity->property_id = $property->id;
                        $service_amenity->value = $sa;

                        $service_amenity->save();
                    }
                }
            }
        }


        $property_type->property_id = $property->id;

        if(!$property_type->save()) {
            parent::message(
                'Hubo un problema al subir una imagen. Comprueba que sean correctas o contacta con un administrador',
                '¡Error!',
                'error'
            );

            $property->delete();

            return back()->withInput();
        }

        if($request->images) {
            $saved_images = $this->saveImages($request->images, $property->id);

            if($saved_images !== true) {
                parent::message(
                    $saved_images,
                    '¡Error!',
                    'error'
                );

                // Si las imágenes no fueron ingresadas correctamente borramos la propiedad para que no hayan duplicados o información incorrecta
                $imgs = $property->images;
                foreach($imgs as $img)
                {
                    @\Storage::delete('images/properties/'.$img->name);
                }

                $property->delete();

                return back()->withInput();
            }            
        }

        parent::message(
            'La propiedad fue ingresada correctamente',
            '¡Excelente!',
            'success'
        );

        return redirect(action('PropertyController@edit', $property));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     *
    public function show($id, Property $property)
    {
        $property = Property::findOrFail($id);
        return view('admin.properties.show', compact('property'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Property $property, Request $request)
    {
        // Si existe la zona asignamos el rastreo desde zona a país
        if($property->zone_id) {
            $property->location_id = $property->zone->location->id;
            $property->department_id = $property->zone->location->department->id; 
            $property->country_id = $property->zone->location->department->country->id;
        }

        // Obtenemos el tipo de Inmueble desde la propiedad o desde la URL
        $tdi = $property->{$request->tdi} ?? $property->{$property->tdi()};

        // Obtenemos el tipo de operación desde la propiedad o desde la URL
        $tdo = $request->tdo ?? $property->tdo();

        // Dependiendo del tipo de operación obtenido agregamos a la propiedad los datos compartidos por el antiguo y el nuevo TDO
        if($tdo === 'rent-sale') {
            if($property->rent && $property->sale) {
                $property->_tdo = array_merge($property->rent->toArray(), $property->sale->toArray());
                $property->_tdo = (object) $property->_tdo;
            }
        } else if($tdo === 'rent') {
            if($property->rent) {
                $property->_tdo = $property->rent->toArray();
                $property->_tdo = (object) $property->_tdo;
            }
        } else if($tdo === 'sale') {
            if($property->sale) {
                $property->_tdo = $property->sale->toArray();
                $property->_tdo = (object) $property->_tdo;
            }
        }

        // Asigmamos a la propiedad el ACTUAL tipo de inmueble y tipo de operación
        $property->tdi = $tdi;
        $property->tdo = $tdo;

        return view('admin.properties.create_edit', compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Property $property, Request $request)
    {
        $tdi = $request->_tdi ?? $property->tdi();
        $tdo = $request->_tdo ?? $property->tdo();

        // Se definen las reglas para los formularios
        $rules = [
            'currency_prop' => 'required',
            /*'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10000',*/
            'images' => 'image|max:10000',
            'zone' => 'required'
        ];

        // Se definen los mensajes para los errores de los formularios
        $messages = [
            'currency_sale.required' => 'El tipo de moneda para alquiler es obligatorio',
            'currency_rent.required' => 'El tipo de moneda para venta es obligatorio',
            'currency_prop.required' => 'El tipo de moneda para gastos es obligatorio',
            'images.*.image' => 'Uno de los archivos cargados no es una imagen',
            'images.*.max' => 'Las im&aacute;genes deben pesar menos de 2 MB cada una',
            'zone.required' => 'La zona es obligatoria'
        ];

        if($tdo == 'sale' || $tdo == 'rent-sale') {
            $rules['price'] = 'required';
            $rules['currency_sale'] = 'required';
            $messages['price.required'] = 'El precio es obligatorio';
        } else {
            $rules['currency_rent'] = 'required';
        }

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            // Enviamos atras con los inputs y un mensaje de error
            return back()->withInput();
        }
        
        $property->currency_country_id  = $request->currency_prop;
        $property->zone_id              = $request->zone;
        $property->address              = $request->address;
        $property->description          = $request->description;
        $property->notes                = $request->notes;
        $property->featured             = $request->featured === 'on' ? true : false;

        // Instanciación de los respectivos tipos de operación
        if($tdo === 'rent') {
            $rent = isset($property->rent) 
                ? $property->rent 
                : new Rent;
            $rent->rented = $request->rented == 'on' ? true : false;

            if(isset($property->sale)) {
                $property->sale->delete();
            }
        } elseif($tdo === 'rent-sale') {
            $rent = isset($property->rent)
                ? $property->rent
                : new Rent;
            $sale = isset($property->sale)
                ? $property->sale
                : new Sale;

            $rent->rented = $request->rented == 'on' ? true : false;
            $sale->sold = $request->sold == 'on' ? true : false;
        } elseif($tdo === 'sale') {
            $sale = isset($property->sale)
                ? $property->sale
                : new Sale;
            $sale->sold = $request->sold == 'on' ? true : false;

            if(isset($property->rent)) {
                $property->rent->delete();
            }
        }

        if(isset($rent)) {
            // Si el tipo de inmueble es un local comercial se asignan datos con respecto a la renta

            $rent->currency_country_id = $request->currency_rent;

            if($tdi == 'business_premise') {
                $rent->annual    = $this->get_int($request->annual);
                $rent->temporary = $this->get_int($request->temporary);

            
            } else { 
                // Aplica a casas y apartamentos

                $rent->january                      = $this->get_int($request->january);
                $rent->january_1st_fortnight        = $this->get_int($request->january_1st_fortnight);
                $rent->january_2nd_fortnight        = $this->get_int($request->january_2nd_fortnight);
                $rent->february                     = $this->get_int($request->february);
                $rent->february_1st_fortnight       = $this->get_int($request->february_1st_fortnight);
                $rent->february_2nd_fortnight       = $this->get_int($request->february_2nd_fortnight);
                $rent->season                       = $this->get_int($request->season);
                $rent->easter                       = $this->get_int($request->easter);
                $rent->annual                       = $this->get_int($request->annual);
                $rent->winter                       = $this->get_int($request->winter);
                $rent->high_season_daily            = $this->get_int($request->high_season_daily);
                $rent->high_season_daily_min_days   = $this->get_int($request->high_season_daily_min_days);
                $rent->low_season_daily             = $this->get_int($request->low_season_daily);
                $rent->low_season_daily_min_days    = $this->get_int($request->low_season_daily_min_days);
            }
        }

        // Información compartida por todas las propiedades en venta
        if(isset($sale)) {
            $sale->currency_country_id = $request->currency_sale;
            $sale->price               = $this->get_int($request->price);
            $sale->financing           = (boolean) $request->financing;
        }

        // Guardamos la propiedad o enviamos atrás con un mensaje de error
        if(!$property->save()) {
            parent::message(
                'La propiedad no se pudo guardar. Intente nuevamente o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        // Si se eligió la renta como Tipo de Operación asignamos el ID del inmueble y guardamos.
        if(isset($rent)) {
            $rent->property_id = $property->id;

            if(!$rent->save()) {
                parent::message(
                    'La propiedad no se pudo guardar. Intente nuevamente o contacte con un administrador',
                    '¡Error!',
                    'error'
                );

                return back()->withInput();
            }

        }

        // Si se eligió la venta como Tipo de Operación asignamos el ID del inmueble y guardamos.
        if(isset($sale)) {
            $sale->property_id = $property->id;

            if(!$sale->save()) {
                parent::message(
                    'La propiedad no se pudo guardar. Intente nuevamente o contacte con un administrador',
                    '¡Error!',
                    'error'
                );

                return back()->withInput();
            }

        }

        // Instanciamos los respectivos tipos de propiedad y asignamos sus datos
        switch($tdi) {
            // Apartamento
            case 'apartment':

                // Información básico del inmueble
                $property->built_footage             = (integer) $request->built_footage;
                $property->distance_from_sea         = (integer) $request->distance_from_sea;
                $property->expenses                  = $this->get_int($request->expenses);
                $property->expenses_range            = $request->expenses_range;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Instanciamos el tipo de inmueble en caso de existir un apartamento para el mismo o creamos una nueva instancia
                $property_type = isset($property->apartment)
                    ? $property->apartment
                    : new Apartment;

                // Eliminamos los demás tipos de inmueble ya que cada uno puede tener sólo un tipo
                if($property->business_premise) {
                    $property->business_premise->delete();
                }

                if($property->countryside) {
                    $property->countryside->delete();
                }

                if($property->house) {
                    $property->house->delete();
                }

                if($property->land) {
                    $property->land->delete();
                }

                break;

            // Local comercial
            case 'business_premise':
                // Información básica del inmueble
                $property->area                      = (integer) $request->area;
                $property->expenses                  = $this->get_int($request->expenses);
                $property->expenses_range            = $request->expenses_range;

                // Instanciamos el tipo de inmueble en caso de existir un local comercial para el mismo o creamos una nueva instancia
                $property_type = isset($property->business_premise)
                    ? $property->business_premise
                    : new BusinessPremise;

                $property_type->floors      = (integer) $request->floors;
                $property_type->low_level   = (integer) $request->low_level;
                $property_type->mezzanine   = (integer) $request->mezzanine;
                $property_type->subsoil     = (integer) $request->subsoil;
                $property_type->bathrooms   = (integer) $request->bathrooms;
                $property_type->kitchenette = (boolean) $request->kitchenette;
                $property_type->private     = (boolean) $request->private;
                $property_type->kitchen     = (boolean) $request->kitchen;

                // Eliminamos los demás tipos de inmueble ya que cada uno puede tener sólo un tipo
                if($property->apartment) {
                    $property->apartment->delete();
                }

                if($property->countryside) {
                    $property->countryside->delete();
                }

                if($property->house) {
                    $property->house->delete();
                }

                if($property->land) {
                    $property->land->delete();
                }

                break;

            // Chacra o campo
            case 'countryside':
                // Información básica del inmueble
                $property->area                      = (integer) $request->area;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Instanciamos el tipo de inmueble en caso de existir una chacra o campo para el mismo o creamos una nueva instancia
                $property_type = isset($property->countryside)
                    ? $property->countryside
                    : new Countryside;

                // Eliminamos los demás tipos de inmueble ya que cada uno puede tener sólo un tipo
                if($property->apartment) {
                    $property->apartment->delete();
                }

                if($property->business_premise) {
                    $property->business_premise->delete();
                }

                if($property->house) {
                    $property->house->delete();
                }

                if($property->land) {
                    $property->land->delete();
                }

                // Asignamos información que tienen SÓLO las chacras y campos
                $property_type->location        = $request->property_location;
                $property_type->access          = $request->access;
                $property_type->topography      = $request->topography;
                $property_type->activity        = $request->activity;
                $property_type->productivity    = $request->productivity;

                // Si se marcó la opción de "cuarto de invitados" asignamos su información
                if($request->guest_room) {
                    $property_type->guest_room              = (boolean) $request->guest_room;
                    $property_type->guest_room_rooms        = (integer) $request->guest_room_rooms;
                    $property_type->guest_room_bathrooms    = (integer) $request->guest_room_bathrooms;
                    $property_type->guest_room_kitchen      = (boolean) $request->guest_room_kitchen;
                    $property_type->guest_room_kitchenette  = (boolean) $request->guest_room_kitchenette;
                    $property_type->guest_room_living_room  = (boolean) $request->guest_room_living_room;
                    // Si no se marcó pero existe, borramos
                } else if($property_type->guest_room) {
                    $property_type->guest_room               = null;
                    $property_type->guest_room_rooms         = null;
                    $property_type->guest_room_bathrooms     = null;
                    $property_type->guest_room_kitchen       = null;
                    $property_type->guest_room_kitchenette   = null;
                    $property_type->guest_room_living_room   = null;
                }

                // Especificaciones

                // Si se marcó "Tajamar"
                if($request->spec_cutwater) {
                    $property_type->spec_cutwater = (boolean) $request->spec_cutwater;
                    $property_type->spec_cutwater_description = $request->spec_cutwater_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->spec_cutwater) {
                    $property_type->spec_cutwater = false;
                    $property_type->spec_cutwater_description = null;
                }

                // Si se marcó "Alambrado"
                if($request->spec_wiring) {
                    $property_type->spec_wiring = (boolean) $request->spec_wiring;
                    $property_type->spec_wiring_description = $request->spec_wiring_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->spec_wiring) {
                    $property_type->spec_wiring = false;
                    $property_type->spec_wiring_description = null;
                }

                // Si se marcó "Ríos y arroyos"
                if($request->spec_streams) {
                    $property_type->spec_rivers_streams = (boolean) $request->spec_rivers_streams;
                    $property_type->spec_rivers_streams_description = $request->spec_rivers_streams_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->spec_rivers_streams) {
                    $property_type->spec_rivers_streams = false;
                    $property_type->spec_rivers_streams_description = null;
                }

                // Si se marcó "Montes"
                if($request->spec_hills) {
                    $property_type->spec_hills = (boolean) $request->spec_hills;
                    $property_type->spec_hills_description = $request->spec_hills_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->spec_hills) {
                    $property_type->spec_hills = false;
                    $property_type->spec_hills_description = null;
                }

                // Si se marcó "Luz"
                if($request->spec_light) {
                    $property_type->spec_light = (boolean) $request->spec_light;
                    $property_type->spec_light_description = $request->spec_light_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->spec_light) {
                    $property_type->spec_light = false;
                    $property_type->spec_light_description = null;
                }

                // Si se marcó la opción de "casa" asignamos su información
                if($request->house) {
                    $property_type->house = (boolean) $request->house;
                    // Iteramos los ambientes si existe alguno
                    // Es importante tener en cuenta que los ambientes se guardan de la misma forma en las chacras y campos.
                    if($request->environments) {
                        foreach($request->environments as $k => $env)
                        {
                            // Creamos una nueva instancia por cada ambiente iterado
                            $environment = new PropertyEnvironment;

                            // Obtenemos la propiedad para la cual se va a ingresar la información
                            $get_env = Environment::find($k);

                            // Asignamos el ID del ambiente, el ID de la propiedad, y su valor
                            $environment->environment_id = $k;
                            $environment->property_id = $property->id;

                            // Asignamos un valor booleano o entero dependiendo del tipo de dato de dicha propiedad
                            $environment->value = $get_env->type == 'yesno'
                                ? (boolean) $env
                                : (integer) $env;

                            // Guardamos el ambiente
                            $environment->save();
                        }
                    }


                    // Iteramos los exteriores si existe alguno
                    if($request->exterior) {
                        foreach($request->exterior as $k => $ext)
                        {
                            // Creamos una nueva instancia por cada exterior iterado
                            $exterior = new PropertyExterior;

                            // Obtenemos la propiedad para la cual se va a ingresar la información
                            $get_ext = Environment::find($k);

                            // Asignamos el ID del exterior, el ID de la propiedad, y su valor
                            $exterior->exterior_id = $k;
                            $exterior->property_id = $property->id;
                            $exterior->value = $get_ext->type == 'yesno'
                                ? (boolean) $ext
                                : (integer) $ext;

                            // Guardamos el exterior
                            $exterior->save();
                        }
                    }

                // Si no se marcó pero existe, borramos
                } else if($property_type->house) {
                    $property_type->house = null;
                    // Borramos las comodidades del inmueble
                    foreach($property->environments as $env)
                    {
                        $env->delete();
                    }

                    // Borramos los exteriores del inmueble
                    foreach($property->exteriors as $ext) {
                        $ext->delete();
                    }
                }

                // Si se marcó la opción de "galpones"
                if($request->sheds) {
                    $property_type->sheds = (boolean) $request->sheds;
                    $property_type->sheds_description = $request->sheds_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->sheds) {
                    $property_type->sheds = false;
                    $property_type->sheds_description = null;
                }

                // Si se marcó la opción de "establos"
                if($request->stables) {
                    $property_type->stables = (boolean) $request->stables;
                    $property_type->stables_description = $request->stables_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->stables) {
                    $property_type->stables = false;
                    $property_type->stables_description = null;
                }

                // Si se marcó la opción de "otros"
                if($request->others) {
                    $property_type->others = (boolean) $request->others;
                    $property_type->others_description = $request->others_description;
                // Si no se marcó, pero existe, borramos.
                } else if($property_type->others) {
                    $property_type->others = false;
                    $property_type->others_description = null;
                }

                break;
            // Casa
            case 'house':
                // Información básica del inmueble
                $property->ground_footage               = (integer) $request->ground_footage;
                $property->built_footage                = (integer) $request->built_footage;
                $property->distance_from_sea            = (integer) $request->distance_from_sea;
                $property->distance_from_beach          = (integer) $request->distance_from_beach;
                $property->expenses_property_tax        = $this->get_int($request->expenses_property_tax);
                $property->expenses_school_tax          = $this->get_int($request->expenses_school_tax);
                $property->expenses_gardener            = $this->get_int($request->expenses_gardener);
                $property->expenses_pool_maintenance    = $this->get_int($request->expenses_pool_maintenance);
                $property->expenses_alarm_vigilance     = $this->get_int($request->expenses_alarm_vigilance);

                // Instanciamos o creamos una instancia del tipo de inmueble
                $property_type = isset($property->house)
                    ? $property->house
                    : new House;

                // Borramos todos los otros inmuebles
                if($property->apartment) {
                    $property->apartment->delete();
                }

                if($property->business_premise) {
                    $property->business_premise->delete();
                }

                if($property->countryside) {
                    $property->countryside->delete();
                }

                if($property->land) {
                    $property->land->delete();
                }

                // Si se marcó la opción de "cuarto de invitados" asignamos su información
                if($request->guest_room) {
                    $property_type->guest_room               = (boolean) $request->guest_room;
                    $property_type->guest_room_rooms         = (integer) $request->guest_room_rooms;
                    $property_type->guest_room_bathrooms     = (integer) $request->guest_room_bathrooms;
                    $property_type->guest_room_kitchen       = (boolean) $request->guest_room_kitchen;
                    $property_type->guest_room_kitchenette   = (boolean) $request->guest_room_kitchenette;
                    $property_type->guest_room_living_room   = (boolean) $request->guest_room_living_room;
                // Si no se marcó pero existe, borramos
                } else if($property_type->guest_room) {
                    $property_type->guest_room               = null;
                    $property_type->guest_room_rooms         = null;
                    $property_type->guest_room_bathrooms     = null;
                    $property_type->guest_room_kitchen       = null;
                    $property_type->guest_room_kitchenette   = null;
                    $property_type->guest_room_living_room   = null;
                }

                break;

            // Terrenos
            case 'land':
                // Información básica del inmueble
                $property->distance_from_sea         = (integer) $request->distance_from_sea;
                $property->area                      = (integer) $request->area;
                $property->expenses_school_tax       = $this->get_int($request->expenses_school_tax);
                $property->expenses_property_tax     = $this->get_int($request->expenses_property_tax);

                // Instanciamos o creamos una instancia del tipo deinmueble
                $property_type = isset($property->land)
                    ? $property->land
                    : new Land;
                $property_type->frontyard       = (integer) $request->frontyard;
                $property_type->backyard        = (integer) $request->backyard;
                $property_type->sides           = (integer) $request->sides;
                $property_type->FOT             = (integer) $request->fot;
                $property_type->FOS             = (integer) $request->fos;
                $property_type->ret_frontyard   = (integer) $request->ret_frontyard;
                $property_type->ret_backyard    = (integer) $request->ret_backyard;
                $property_type->ret_bilateral   = (integer) $request->ret_bilateral;

                if($property->apartment) {
                    $property->apartment->delete();
                }

                if($property->business_premise) {
                    $property->business_premise->delete();
                }

                if($property->house) {
                    $property->house->delete();
                }

                break;
        }

        // Actualizamos la propiedad o enviamos atrás con un mensaje de error
        if(!$property->save()) {
            parent::message(
                'La propiedad no se pudo guardar. Intente nuevamente o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        // Ambientes
        foreach($property->environments as $env)
        {
            $env->delete();
        }

        // Sólo si el inmueble NO es una chacra o campo
        if(gettype($property_type) !== "App\Countryside")
        {
            // Ambientes
            // Si existen ambientes
            if($request->environments) {
                // Iteramos los ambientes
                foreach($request->environments as $k => $env)
                {
                    // Si el ambiente está definido
                    if($env) {

                        $environment = new PropertyEnvironment;

                        $environment->environment_id = $k;
                        $environment->property_id = $property->id;
                        $environment->value = $env;

                        $environment->save();
                    }
                }

            }

            // Equipamientos
            if($request->equipment) {
                foreach($request->equipment as $k => $eq)
                {
                    if($eq) {
                        $equipment = new PropertyEquipment;

                        $equipment->equipment_id = $k;
                        $equipment->property_id = $property->id;
                        $equipment->value = $eq;

                        $equipment->save();
                    }
                }
            }

            // Exteriores
            if($request->exterior) {
                foreach($request->exterior as $k => $ext)
                {
                    if($ext) {
                        $exterior = new PropertyExterior;

                        $exterior->exterior_id = $k;
                        $exterior->property_id = $property->id;
                        $exterior->value = $ext;

                        $exterior->save();
                    }
                }
            }

            // Servicios y Amenities
            if($request->services_amenities) {
                foreach($request->services_amenities as $k => $sa)
                {
                    if($sa) {
                        $service_amenity = new PropertyServiceAmenity;

                        $service_amenity->service_amenity_id = $k;
                        $service_amenity->property_id = $property->id;
                        $service_amenity->value = $sa;

                        $service_amenity->save();
                    }
                }
            }
        }

        $property_type->property_id = $property->id;

        if(!$property_type->save()) {
            parent::message(
                'Hubo un problema al subir una imagen. Comprueba que sean correctas o contacta con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        // Guardar Imagenes
        if($request->images) {
            $saved_images = $this->saveImages($request->images, $property->id);

            if($saved_images !== true) {
                parent::message(
                    $saved_images,
                    '¡Error!',
                    'error'
                );

                return back()->withInput();
            }
        }

        /*
         * Hasta ahora ningún error:
         * - Se guardó la propiedad ($property)
         * - Se guardó el tipo de propiedad ($property_type)
         * - Se guardaron los ambientes (App\EnvironmentValue), equipamientos (App\EquipmentValue), exteriores (App\ExteriorValue) o Servicios y Amenities (App\ServiceAmenityValue)
         * - Se guardaron las imágenes (App\Image)
         */

        parent::message(
            'La propiedad fue actualizada correctamente',
            '¡Excelente!',
            'success'
        );

        return redirect(action('PropertyController@edit', $property));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     */
    public function destroy(Property $property)
    {
        if(!$property->delete()) {
            parent::message(
                'Hubo un problema al eliminar la propiedad. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'La propiedad ha sido eliminada correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('PropertyController@index'));
    }


    private function saveImages($images, $property_id)
    {
        foreach($images as $k => $img)
        {
            try {
                //$save = $img->store('images/properties');
                // Generamos un nombre de 65 caracteres (es muy improbable que se generen dos nombres iguales, aunque no es imposible)
                $name = str_random(65);
                // Aniadimos al nombre generado la extension dada por la imagen
                $filename =  $name . '.' . $img->extension();
                // Detectamos la barra separadora del S.O. donde se ejecuta el Web Server
                $d = DIRECTORY_SEPARATOR;
                // Generamos la Full Qualified URL de la ruta donde van las imagenes de las propiedades y el nombre generado.
                $fqurl = "app{$d}images{$d}properties{$d}$filename";

                // Creamos la imagen a partir del archivo iterado, con una resoluicion de 800px de largo y 600px de alto, en la ruta anteriormente generada, a una calidad del 90%.
                $stored_image = Image::make($img)
                    ->resize(800, 600)
                    ->save(storage_path($fqurl));

                // Aca podriamos agregar un codigo para insertar el watermark del logo en la imagen...

                $image = new Img;
                $image->property_id = $property_id;
                $image->name = $filename;

                if(!$image->property->coverImage()) {
                    if(count($images) == 1) { 
                        $image->cover = true; 
                    } else {
                        if($k == 0) {
                            $image->cover = true;
                        }
                    }
                }
               
                if(!$image->save()) {
                    return 'Hubo un problema al subir una imagen. Comprueba que sean correctas o contacta con un administrador';
                }
            } catch(\Exception $e) {
                return 'Hubo un problema al subir una imagen. Comprueba que sean correctas o contacta con un administrador.<br><hr> <strong>Mensaje del servidor</strong>: <i>"' . $e->getMessage() . '"</i>';
            }
        }

        return true;
    }

    public function setCover(Property $property, Request $request) {
        $image = Img::find($request->id);
        foreach($property->images as $img)
        {
            $img->cover = false;
            $img->save();
        }
        $image->cover = true;

        if($image->save()) {
            parent::message(
                'La portada ha sido cambiada correctamente',
                '¡&Eacute;xito!',
                'success'
            );

            return back();
        }
    } 

    public function deleteImages(Property $property, Request $request)
    {
        if($request->img) {
            if(gettype($request->img) !== 'array' || count($request->img) < 1) {
                parent::message(
                    'Error al procesar una imagen. Intente nuevamente o contacte un administrador',
                    '¡Error!',
                    'error'
                );
    
                return redirect(action('PropertyController@edit', [ 'property' => $property ]));
            }

            foreach($request->img as $img) {
                $image = Img::find($img);
                $path = 'images' . DIRECTORY_SEPARATOR . 'properties' . DIRECTORY_SEPARATOR . $image->name;

                // Comprobación si la imagen está en la propiedad
                if($image->property != $property) {
                    parent::message(
                        'Parece que la imagen no corresponde con la propiedad',
                        '¡Error!',
                        'error'
                    );

                    return redirect(action('PropertyController@edit', [ 'property' => $property ]));
                }

                if($image->is_cover()) {
                    $images = $image->property->images;

                    $i = 0;

                    /*
                    * Mientras el ID del índice del array de imágenes sea distinto al ID de la imagen a eliminar
                    * y mientras el índice sea menor al total de imágenes de la propiedad a la que pertenece la imagen a eliminar
                    * y mientras el total de imágenes de la propiedad a la que pertenece la imagen a eliminar sea mayor a 0
                    * 
                    * Definir la primera imagen del array de imágenes como "Portada"
                    */
                    while(count($images) > 0 && $i < count($images)) {
                        if($images[$i]->id !== $image->id) {
                            $images[$i]->cover = true;
                            $images[$i]->save();
                            $i = count($images);
                        }
                        
                        $i++;
                    }
                }

                if(!Storage::delete($path) || !$image->delete()) {
                    parent::message(
                        'Una imagen no pudo ser borrada. Intente nuevamente',
                        '¡Error!',
                        'error'
                    );

                    return redirect(action('PropertyController@edit', [ 'property' => $property ]));
                }
            }

            parent::message(
                'Las im&aacute;genes han sido borradas con &eacute;xito',
                '¡&Eacute;xito!',
                'success'
            );

            return redirect(action('PropertyController@edit', [ 'property' => $property ]));

        }

        if ($request->id === null) {
            parent::message(
                'La imagen no es v&aacute;lida',
                '¡Error!',
                'error'
            );

            return redirect(action('PropertyController@edit', [ 'property' => $property ]));
        }
        
        $image = Img::find($request->id);
        $path = 'images' . DIRECTORY_SEPARATOR . 'properties' . DIRECTORY_SEPARATOR . $image->name;

        // Comprobación si la imagen está en la propiedad
        if($image->property != $property) {
            parent::message(
                'Parece que la imagen no corresponde con la propiedad',
                '¡Error!',
                'error'
            );

            return redirect(action('PropertyController@edit', [ 'property' => $property ]));
        }

        if($image->is_cover()) {
            $images = $image->property->images;

            $i = 0;

            /*
             * Mientras el ID del índice del array de imágenes sea distinto al ID de la imagen a eliminar
             * y mientras el índice sea menor al total de imágenes de la propiedad a la que pertenece la imagen a eliminar
             * y mientras el total de imágenes de la propiedad a la que pertenece la imagen a eliminar sea mayor a 0
             * 
             * Definir la primera imagen del array de imágenes como "Portada"
             */
            while(count($images) > 0 && $i < count($images)) {
                if($images[$i]->id !== $image->id) {
                    $images[$i]->cover = true;
                    $images[$i]->save();
                    $i = count($images);
                }
                
                $i++;
            }
        }

        if(Storage::exists($path)) {
            if(Storage::delete($path) && $image->delete()) {
                parent::message(
                    'La imagen ha sido borrada correctamente',
                    '¡&Eacute;xito!',
                    'success'
                );

                return redirect(action('PropertyController@edit', [ 'property' => $property ]));
            } else {
                parent::message(
                    'La imagen no pudo ser borrada. Intente nuevamente',
                    '¡Error!',
                    'error'
                );

                return redirect(action('PropertyController@edit', [ 'property' => $property ]));
            }
        } else {
            parent::message(
                'La imagen ha sido borrada correctamente',
                '¡&Eacute;xito!',
                'success'
            );

            @$image->delete();

            return redirect(action('PropertyController@edit', [ 'property' => $property ]));
        }        
    }

    public function getImage($name)
    {
        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'properties' . DIRECTORY_SEPARATOR . $name);

        if(file_exists($path)) {
            $image = Image::make($path);

            /*$file = Storage::get($path);
            $response = Response::make($file, 200);
            $response->header('Content-Type', 'image/jpeg');*/

            return $image->response('jpg');
        } else {
            return view('errors.404');
        }
    }

    public function showProperty($name)
    {
        $str = str_replace('-', ' ', $name);

        $property = Property::where('name', $str)->first();

        if(!$property) {
            return view('errors.404');
        }

        return view('admin.properties.show', compact('property'));
    }

    public function newForm()
    {
        return view('admin.properties.new');
    }

    // API: /api/propiedades/ GET
    public function getAll()
    {
        $properties = Property::all();

        return response()->json($properties);
    }

    // API: /api/destacados/ GET
    public function getAllFeatured()
    {
        $properties = Property::where('featured', 1)->get();

        return response()->json($properties);
    }

    // API: /api/propiedades/{q} GET
    public function getProperties($q)
    {
        $q = (int) $q;
        $properties = Property::take($q)->get();

        return response()->json($properties);
    }

    // API:/api/destacados/{q} GET
    public function getFeatured($q = 0)
    {
        $q = (int) $q;
        $properties = Property::where('featured', 1)->take($q)->get();

        return response()->json($properties);
    }

    public function search(Request $request)
    {   
        $results = [];

        $results[] = Property::where('name', 'like', '%' . $request->term . '%')->get();

        $properties = Property::all(); dd($results);

        foreach($properties as $p)
        {
            $match = $p->category->hasTag($request->term);

            if($match) {
                $results[] = $p;
            }
        }

        dd($results);
        
    }

    /**
     * Devuelve un entero de un valor con signo. Ejemplo: $ 50.000,00 lo convierte a 50000
     *
     * @param $currency
     * @return int
     */
    private function get_int($currency)
    {
        $price = str_replace_first('$', '', $currency);
        $price = preg_replace(
            [ '/\$/', '/\./' ],
            [ '', '' ],
            $price);

        return (int) trim($price);
    }
}
