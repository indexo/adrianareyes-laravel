<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function message($message, $title, $type)
    {
        $message = [
           'mensaje' => $this->htmlify($message),
           'titulo' => $title,
           'tipo' => $type
        ];
        
        session()->put('mensaje', $message);
    }
    
    protected function htmlify($message)
    {
        switch (gettype($message)) {
            case 'array':
                if(count($message) > 1) {
                    $msg = '<ul class="text-left">';
                    foreach($message as $m)
                    {
                        $msg .= "<li>$m</li>";
                    }
                    $msg .= "</ul>";
                } else {
                    $msg = "<p>" . $message[0] . "</p>";
                }
                
                break;
            case 'object':
                if($message->count() > 1) {
                    $msg = '<ul class="text-left">';
                    foreach($message->all() as $m)
                    {
                        $msg .= "<li>$m</li>";
                    }
                    $msg .= "</ul>";
                } else {
                    $msg = "<p>" . $message->first() . "</p>";
                }
                break;
            default:
                $msg = "<p>$message</p>";
        }
        
        return $msg;
    }

    // Devuelve el tipo de dato según el string pasado.
    private function realType($string)
    {
        if($string === 'on') {
            return true;
        } else if(is_numeric($string)) {
            return intval($string);
        } else if($string === null) {
            return null;
        } else {
            return $string;
        }
    }
}
