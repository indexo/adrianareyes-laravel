<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Calendar::all();

        return view('admin.calendar.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     *
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [ 
            'title' => 'required|max:50',
            'description' => 'max:600',
            'start_date' => 'required|date',
            'end_date' => 'date',
        ];

        if(!$request->all_day) {
            $rules['end_date'] .= '|required';
        }

        $messages = [
            'title.required' => 'El t&iacute;tulo es obligatorio',
            'title.max' => 'El t&iacute;tulo debe tener como m&aacute;ximo 50 caracteres',
            'description.max' => 'La descripci&oacute;n debe tener como m&aacute;ximo 600 caracteres',
            'start_date.required' => 'La fecha de inicio es obligatoria',
            'start_date.date' => 'La fecha de inicio debe ser una fecha v&aacute;lida',
            'end_date.required' => 'La fecha final es obligatoria',
            'end_date.date' => 'La fecha final debe ser una fecha v&aacute;lida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $event = new Calendar;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->start_time = $request->start_time ?? '00:00';
        if(!$request->all_day) {
            $event->end_date = $request->end_date;
            $event->end_time = $request->end_time;
        }
        $event->user_id = $request->user()->id;
        
        if(!$event->save()) {
            parent::message(
                'No se pudo guardar el evento. Intente de nuevo o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El evento se cre&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CalendarController@edit', $event));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     *
    public function show(Calendar $calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Calendar $event)
    {
        $events = Calendar::all();

        return view('admin.calendar.index', compact('event', 'events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Calendar $event)
    {
        $rules = [ 
            'title' => 'required|max:50',
            'description' => 'max:600',
            'start_date' => 'required|date',
            'end_date' => 'date',
        ];

        if(!$request->all_day) {
            $rules['end_date'] .= '|required';
        }

        $messages = [
            'title.required' => 'El t&iacute;tulo es obligatorio',
            'title.max' => 'El t&iacute;tulo debe tener como m&aacute;ximo 50 caracteres',
            'description.max' => 'La descripci&oacute;n debe tener como m&aacute;ximo 600 caracteres',
            'start_date.required' => 'La fecha de inicio es obligatoria',
            'start_date.date' => 'La fecha de inicio debe ser una fecha v&aacute;lida',
            'end_date.required' => 'La fecha final es obligatoria',
            'end_date.date' => 'La fecha final debe ser una fecha v&aacute;lida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $event->title = $request->title;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->start_time = $request->start_time;
        if(!$request->all_day) {
            $event->end_date = $request->end_date;
            $event->end_time = $request->end_time;
        }
        $event->user_id = $request->user()->id;
        
        if(!$event->save()) {
            parent::message(
                'No se pudo guardar el evento. Intente de nuevo o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'El evento se actualiz&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CalendarController@edit', $event));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calendar $event)
    {
        if(!$event->delete()) {
            parent::message(
                'El evento no pudo ser eliminado. Intente de nuevo o contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El evento se elimin&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('CalendarController@index'));

    }
}
