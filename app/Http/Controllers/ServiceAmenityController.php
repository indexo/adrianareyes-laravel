<?php

namespace App\Http\Controllers;

use App\ServiceAmenity;
use Illuminate\Http\Request;
use Validator;

class ServiceAmenityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services_amenities = ServiceAmenity::all();

        return view('admin.services_amenities.index', compact('services_amenities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services_amenities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:services_amenities',
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $service_amenity = new ServiceAmenity;

        $service_amenity->name = $request->name;
        $service_amenity->type = $request->type;
        $service_amenity->placeholder = $request->placeholder;

        if(!$service_amenity->save()) {
            parent::message(
                'Hubo un problema al guardar el servicio / amenity. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El servicio / amenity se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ServiceAmenityController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceAmenity  $services_amenities
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceAmenity $service_amenity)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceAmenity  $services_amenities
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceAmenity $service_amenity)
    {
        return view('admin.services_amenities.edit', compact('service_amenity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceAmenity  $services_amenities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceAmenity $service_amenity)
    {

        if($request->name != $service_amenity->name) {
            $name = 'required|unique:services_amenities';
        } else {
            $name = 'required';
        }
        $rules = [
            'name' => $name,
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $service_amenity->name = $request->name;
        $service_amenity->type = $request->type;
        $service_amenity->placeholder = $request->placeholder;

        if(!$service_amenity->save()) {
            parent::message(
                'Hubo un problema al guardar el servicio / amenity. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El servicio / amenity se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ServiceAmenityController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceAmenity  $services_amenities
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceAmenity $service_amenity)
    {
        if(!$service_amenity->delete()) {
            parent::message(
                'Hubo un problema al eliminar el servicio / amenity. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El servicio / amenity se elimin&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ServiceAmenityController@index'));
    }
}
