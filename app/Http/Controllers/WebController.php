<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use DB;

class WebController extends Controller
{
    public function __construct()
    {
        $this->month_list = [ 
            'january',
            'january_1st_fortnight',
            'january_2nd_fortnight',
            'february',            
            'february_1st_fortnight',
            'february_2nd_fortnight',
            'season',
            'easter',
            'annual',
            'winter',
            'high_season_daily',
            'low_season_daily'
        ];
    }

    /*
     * Ruta: /
     * Método: GET
     */
    public function index()
    {
        $properties_amount = 8;
        
        if (Property::where('featured', 1)->count() > 0) {
            $properties = Property::where('featured', 1)
                ->take($properties_amount)
                ->orderBy('created_at', 'DESC')
                ->get();
        } else {
            $properties = Property::take($properties_amount)
                ->orderBy('created_at', 'DESC')
                ->get();
        }

        if($properties) {
            // Iteramos los inmuebles
            foreach($properties as $k => $property) {
                // Obtenemos el tipo de operación
                $tdo = $property->getTdo();
                $status = '';
                $price = '';

                // Si el tipo de operación es Alquiler y venta
                if(gettype($tdo) == 'array') {
                    $rent = $tdo[0];
                    $sale = $tdo[1];

                    // Si el inmueble NO está alquilado
                    if(!$rent->rented()) {
                        // Si tiene moneda seleccionada
                        if($rent->country()) {
                            $p_price = 0;
                            $ci = 0;
                            while($ci < count($this->month_list) && $p_price <= 0)
                            {
                                $p_price = $this->num_format($rent[$this->month_list[$ci]]);
                                $ci++;
                            }
                            // Agregamos el precio a un string
                            $price .= $rent->country()->currency_symbol . ' ' . $p_price . ' / ';
                        }

                        // Agregamos el estado (string) a $status
                        $status .= $rent->status() . ' / ';
                    }

                    // Si el inmueble NO está vendido
                    if(!$sale->sold()) {
                        // Si tiene moneda seleccionada
                        if($sale->country()) {
                            // Agregamos el precio a un string
                            $price .= $sale->country()->currency_symbol . ' ' . $this->num_format($sale->price);
                        }

                        // Agregamos el estado (string) a $status
                        $status .= $sale->status();
                    }
                } else {
                    $status = $tdo->status();

                    switch(get_class($tdo)) {
                        case 'App\Sale':
                            $price = $tdo->country()->currency_symbol . ' ' . $this->num_format($tdo->price);
                            break;
                        case 'App\Rent':
                            $p_price = 0;
                            $ci = 0;
                            while($ci < count($this->month_list) && $p_price <= 0)
                            {
                                $p_price = $this->num_format($tdo[$this->month_list[$ci]]);
                                $ci++;
                            }
                            $price = $tdo->country()->currency_symbol . ' ' . $p_price;
                            break;
                    }
                    // Si no es un array agregamos a $status el estado único.
                    $status = $tdo->status();
                }

                // Asignamos el precio
                $properties[$k]->price = $price;

                // Asignamos el estado a la propiedad
                $properties[$k]->status = $status;

                // Obtenemos los ambientes
                $environments = $property->environments;
                $partial_envs = [];

                // Iteramos los ambientes
                foreach($environments as $env)
                {
                    // Asignamos a un array el nombre de cada ambiente y su valor
                    $partial_envs[$env->environment->name] = $env->value;
                }

                // Asignamos el array de ambientes creado a la propiedad
                $properties[$k]->environments = $partial_envs;

                $rooms = 0;
                $bathrooms = 0;

                // Si existen ambientes
                if(count($property->environments) > 0) {
                    // Iteramos los ambientes
                    foreach($property->environments as $name => $value)
                    {
                        // Si existen dormitorios
                        if($name == 'Dormitorios') {
                            $rooms = $value;
                        }

                        // Si existen baños
                        if($name == 'Baños') {
                            $bathrooms = $value;
                        }
                    }
                }

                // Asignamos los dormitorios y baños a la propiedad
                $properties[$k]->rooms = $rooms;
                $properties[$k]->bathrooms = $bathrooms;

                // Asignamos la imagen de portada a la propiedad
                $properties[$k]->cover_image = $property->coverImage();

                // Si el inmueble está alquilado y vendido lo quitamos del array
                if (($property->rent && $property->rent->rented > 0) && ($property->sale && $property->sale->sold > 0)) {
                    unset($properties[$k]);
                }

                if(!$property->apartment && !$property->house && !$property->featured) {
                    unset($properties[$k]);
                }
            }
        }

    	return view('frontend.index', compact('properties'));
    }

    /*
     * Ruta: /nosotros
     * Método: GET
     */
    public function about_us()
    {
        return view('frontend.nosotros');
    }

    public function properties(Request $request)
    {
        /*
         * Tipo de Operación (TDO):
         * Obtenemos el número de inmuebles pasado como argumento de paginate()
         * siempre y cuando estos no estén alquilados ni vendidos.
         *
         * Nota: Usamos if() condicional para evitar XSS ya que las llamadas se
         * realizan por GET y no es posible implementar verificación CSRF
         */
        if($request->tdo == 'rent') {
            // TDO: Alquiler
            $properties = Property::has('rent')
                ->whereHas('rent', function($query) {
                    $query->where('rented', false);
                });
        } else if($request->tdo == 'sale') {
            // TDO: Venta
            $properties = Property::has('sale')
                ->whereHas('sale', function($query) {
                    $query->where('sold', false);
                });
        } else {
            // TDO: null
            $properties = Property::whereHas('rent', function($query) {
                $query->where('rented', false);
            })->orWhereHas('sale', function($query) {
                $query->where('sold', false);
            });
        }

        /*
         * Tipo de Inmueble (TDI):
         * Obtenemos los inmuebles que coincidan con el tipo seleccionado
         *
         * Nota: Usamos if() condicional para evitar XSS ya que las llamadas se
         * realizan por GET y no es posible implementar verificación CSRF
         */
        if($request->tdi) {
            switch($request->tdi) {
                case 'apartment':
                    // TDI: Apartamento
                    $properties = $properties->has('apartment');
                    break;
                case 'business_premise':
                    // TDI: Local Comercial
                    $properties = $properties->has('business_premise');
                    break;
                case 'countryside':
                    // TDI: Chacra o Campo
                    $properties = $properties->has('countryside');
                    break;
                case 'house':
                    // TDI: Casa
                    $properties = $properties->has('house');
                    break;
                case 'land':
                    // TDI: Terreno
                    $properties = $properties->has('land');
                    break;
            }
        }

        /*
         * Localidad:
         * Obtenemos todos los inmuebles que se encuentran en la localidad
         * seleccionada si existe.
         *
         * Flujo inverso: Zona > Localidad > Departamento > País
         */
        if($request->location) {
            // Parseamos el parámetro ?location a entero, con esto también evitamos XSS
            $location = (int) $request->location;

            // Obtenemos los inmuebles que tienen dicha localidad
            $properties = $properties->whereHas('zone', function($query) use ($location) {
                $query->where('location_id', $location);
            });
        }

        /*
         * Precio - Desde:
         * Obtenemos todos los inmuebles que se encuentran por encima
         * del monto seleccionado (en dólares)
         */
        if($request->from) {
            // Parseamos el parámetro ?from a entero y lo convertimos a dólares, con esto también evitamos XSS
            $from = (int) $request->from;
            $usd_exchange = \App\Country::where('iso3', 'USA')->first();
            $usd_exchange = $usd_exchange->currency_exchange || 1;
            $from = $from / $usd_exchange;

            // Obtenemos los inmuebles que tienen un costo superior al especificado, si lo está
            $properties = $properties->whereHas('rent', function($query) use ($from) {
                $query->where('january', '>=', $from);
            })->orWhereHas('sale', function($query)  use ($from) {
                $query->where('price', '>=', $from);
            });
        }

        /*
         * Precio - Hasta:
         * Obtenemos todos los inmuebles que se encuentran por debajo
         * del monto seleccionado (en dólares)
         */
        if($request->to) {
            // Parseamos el parámetro ?to a entero y lo convertimos a dólares, con esto también evitamos XSS
            $to = (int) $request->to;
            $usd_exchange = \App\Country::where('iso3', 'USA')->first();
            if($usd_exchange) {
                $usd_exchange = $usd_exchange->currency_exchange || 1;
                $to = $to / $usd_exchange;

                // Obtenemos los inmuebles que tienen un costo superior al especificado, si lo está
                $properties = $properties->whereHas('rent', function($query) use ($to) {
                    $query->where('january', '>=', $to);
                })->orWhereHas('sale', function($query)  use ($to) {
                    $query->where('price', '>=', $to);
                });
            }
        }

        /*
         * Comodidades: Dormitorios
         * Obtenemos sólo los inmuebles que tienen exactamente la cantidad de
         * dormitorios pasada por parámetro ?rooms
         */
        if($request->rooms) {
            // Parseamos el parámetro ?rooms a entero, con esto evitamos ataques XSS
            $rooms = (int) $request->rooms;

            $properties = $properties->whereHas('environments', function($query) use ($rooms) {
                $query->where('value', $rooms);
                $query->whereHas('environment', function($query) {
                    $query->where('name', 'Dormitorios');
                });
            });
        }

        /*
         * Comodidades: Baños
         * Obtenemos sólo los inmuebles que tienen exactamente la cantidad de
         * baños pasada por parámetro ?bathrooms
         */
        if($request->bathrooms) {
            // Parseamos el parámetro ?rooms a entero, con esto evitamos ataques XSS
            $bathrooms = (int) $request->bathrooms;

            $properties = $properties->whereHas('environments', function($query) use ($bathrooms) {
                $query->where('value', $bathrooms);
                $query->whereHas('environment', function($query) {
                    $query->where('name', 'Baños');
                });
            });
        }

        /*
         * Paginamos las propiedades
         */
        $properties = $properties->with([
            'rent',
            'sale'
        ])->orderBy('created_at', 'desc')->paginate(12);

        /*
         * Tipo de moneda:
         * Convertimos la moneda de todos los inmuebles a la moneda seleccionada, por defecto son dólares
         * Esto lo hacemos después de la paginación porque no excluye resultados, sólo los cambia.
         */

        // Parseamos el parámetro ?currency a entero, con esto evitamos ataques XSS
        $currency = ((int) $request->currency > 0) ? (int) $request->currency : 1;
        // Obtenemos el país App\Country donde el ID es $currency
        $country = \App\Country::find($currency) ?? \App\Country::where('iso3', 'USA')->first();

        // Sólo si existe el país
        if($country) {
            // Función anónima que devuelve un string con la conversión de un cost N
            $cost = function($number) use ($country, $currency) {
                return $country->currency_symbol . ' ' .
                    self::num_format((float) $number / $currency);
            };

            // Iteramos los inmuebles
            foreach($properties as $k => $property)
            {
                // Gastos del inmueble
                $properties[$k]->expenses = $cost($property->expenses);
                $properties[$k]->expenses_school_tax = $cost($property->expenses_school_tax);
                $properties[$k]->expenses_property_tax = $cost($property->expenses_property_tax);
                $properties[$k]->expenses_gardener = $cost($property->expenses_gardener);
                $properties[$k]->expenses_pool_maintenance = $cost($property->expenses_pool_maintenance);
                $properties[$k]->expenses_alarm_vigilance = $cost($property->expenses_alarm_vigilance);

                // Costos específicos tipo de operación
                switch($property->tdo()) {
                    // TDO: Alquiler
                    case 'rent':
                        // Asignamos ya de paso el estado del alquiler
                        $properties[$k]->status = $property->rent->status();

                        // Precio de "alquiler"
                        $ci = 0;
                        $p_price = 0;
                        while($ci < count($this->month_list))
                        {
                            if(!!$property->rent[$this->month_list[$ci]]) {
                                $p_price = $property->rent[$this->month_list[$ci]];
                                $ci = count($this->month_list);
                            } else {
                                $ci++;
                            }
                        }
                        $properties[$k]->price = $cost($p_price);

                        // Enero
                        $properties[$k]->january = $cost($property->rent->january);
                        $properties[$k]->january_1st_fortnight = $cost($property->rent->january_1st_fortnight);
                        $properties[$k]->january_2nd_fortnight = $cost($property->rent->january_2nd_fortnight);

                        // Febrero
                        $properties[$k]->february = $cost($property->rent->february);
                        $properties[$k]->february_1st_fortnight = $cost($property->rent->february_1st_fortnight);
                        $properties[$k]->february_2nd_fortnight = $cost($property->rent->february_2nd_fortnight);

                        // Temporadas
                        $properties[$k]->season = $cost($property->rent->season);
                        $properties[$k]->easter = $cost($property->rent->easter);
                        $properties[$k]->winter = $cost($property->rent->winter);

                        // Diario
                        $properties[$k]->high_season_daily = $cost($property->rent->high_season_daily);
                        $properties[$k]->high_season_daily = $cost($property->rent->low_season_daily);

                        // Locales comerciales
                        $properties[$k]->temporary = $cost($property->rent->temporary);
                        $properties[$k]->annual = $cost($property->rent->annual);
                        break;
                    // TDO: Venta
                    case 'sale':
                        // Asignamos ya de paso el estado de la venta
                        $properties[$k]->status = $property->sale->status();

                        // Precio de venta
                        $properties[$k]->price = $cost($property->sale->price);
                        break;
                    // TDO: Alquiler y venta
                    default:
                        // Asignamos ya de paso el estado del alquiler
                        $properties[$k]->status = $property->rent->status() . " / " . $property->sale->status();

                        // Enero
                        $properties[$k]->january = $cost($property->rent->january);
                        $properties[$k]->january_1st_fortnight = $cost($property->rent->january_1st_fortnight);
                        $properties[$k]->january_2nd_fortnight = $cost($property->rent->january_2nd_fortnight);

                        // Febrero
                        $properties[$k]->february = $cost($property->rent->february);
                        $properties[$k]->february_1st_fortnight = $cost($property->rent->february_1st_fortnight);
                        $properties[$k]->february_2nd_fortnight = $cost($property->rent->february_2nd_fortnight);

                        // Temporadas
                        $properties[$k]->season = $cost($property->rent->season);
                        $properties[$k]->easter = $cost($property->rent->easter);
                        $properties[$k]->winter = $cost($property->rent->winter);

                        // Diario
                        $properties[$k]->high_season_daily = $cost($property->rent->high_season_daily);
                        $properties[$k]->high_season_daily = $cost($property->rent->low_season_daily);

                        // Locales comerciales
                        $properties[$k]->temporary = $cost($property->rent->temporary);
                        $properties[$k]->annual = $cost($property->rent->annual);

                        // Precio de venta
                        $properties[$k]->price = $cost($property->sale->price);
                }// /switch

                // Asignamos la imagen de portada a la propiedad
                $properties[$k]->cover_image = $property->coverImage();

                // Si el inmueble tiene comodidades
                if($property->environments) {
                    // Declaramos un array vacío
                    $environments = [];

                    // Iteramos cada ambiente y lo asignamos a dicho array
                    foreach($property->environments as $env)
                    {
                        // Si el tipo de comodidad no es un entero lo parseamos en boolean
                        if($env->environment->type === 'int') {
                            $value = $env->value;
                        } else {
                            $value = (boolean) $env->value;
                        }

                        // Asignamos el valor $environments['nombre_del_ambiente'] = $valor
                        $environments[$env->environment->name] = $value;
                    } // /foreach

                    $properties[$k]->env = $environments;
                } // / if

                // Si el inmueble tiene equipamiento
                if($property->equipments) {
                    // Declaramos un array vacío
                    $equipments = [];

                    // Iteramos cada equipamiento y lo asignamos a dicho array
                    foreach($property->equipments as $eq)
                    {
                        // Si el tipo de equipamiento no es un entero lo parseamos en boolean
                        if($eq->equipment->type === 'int') {
                            $value = $eq->value;
                        } else {
                            $value = (boolean) $eq->value;
                        }

                        // Asignamos el valor $equipments['nombre_del_equipamiento'] = $valor
                        $equipments[$eq->equipment->name] = $value;
                    } // /foreach

                    $properties[$k]->eq = $equipments;
                } // /if

                // Si el inmueble tiene exteriores
                if($property->exteriors) {
                    // Declaramos un array vacío
                    $exteriors = [];

                    // Iteramos cada exterior y lo asignamos a dicho array
                    foreach($property->exteriors as $ext)
                    {
                        // Si el tipo de exterior no es un entero lo parseamos en boolean
                        if($ext->exterior->type === 'int') {
                            $value = $ext->value;
                        } else {
                            $value = (boolean) $ext->value;
                        }

                        // Asignamos el valor $exteriors['nombre_del_exterior'] = $valor
                        $exteriors[$ext->exterior->name] = $value;
                    } // /foreach

                    $properties[$k]->ext = $exteriors;
                } // /if

                // Si el inmueble tiene Servicios y Amenities
                if($property->exteriors) {
                    // Declaramos un array vacío
                    $services_amenities = [];

                    // Iteramos cada exterior y lo asignamos a dicho array
                    foreach($property->services_amenities as $sa)
                    {
                        // Si el tipo de exterior no es un entero lo parseamos en boolean
                        if($sa->service_amenity->type === 'int') {
                            $value = $sa->value;
                        } else {
                            $value = (boolean) $sa->value;
                        }

                        // Asignamos el valor $services_amenities['nombre_del_servicio_o_comodidad'] = $valor
                        $services_amenities[$sa->service_amenity->name] = $value;
                    } // /foreach

                    $properties[$k]->sa = $services_amenities;
                } // /if
           } // /foreach
        } // if country

        return view('frontend.inmuebles', compact('properties'));
    }

    /*
     * Ruta: /contacto
     * Método: GET
     */
    public function contact_us()
    {
        return view('frontend.contacto');
    }

    /*
     * Ruta: /administración
     * Método: GET
     */
    public function admin()
    {
        return view('web.admin');
    }

    /*
     * Ruta: /contacto
     * Método: POST
     */
    public function contactForm(Request $request)
    {

    }

    public function showProperty($reference)
    {
        $property = Property::get($reference);

        if(!$property) {
            return view('frontend.errors.404');
        }
        // Obtenemos el tipo de operación
        $tdo = $property->getTdo();
        $status = '';
        $symbol = '$';

        // Si el tipo de operación es Alquiler y venta
        if(gettype($tdo) == 'array') {
            $rent = $tdo[0];
            $sale = $tdo[1];

            // Si el inmueble NO está alquilado
            if(!$rent->rented()) {
                // Si tiene moneda seleccionada
                if($rent->country()) {
                    // Agregamos el símbolo
                    $symbol = $rent->country()->currency_symbol;
                }

                // Agregamos el estado (string) a $status
                $status .= $rent->status() . ' / ';
            }

            // Si el inmueble NO está vendido
            if(!$sale->sold()) {
                // Si tiene moneda seleccionada
                if($sale->country()) {
                    // Agregamos el símbolo
                    $symbol = $sale->country()->currency_symbol;
                }

                // Agregamos el estado (string) a $status
                $status .= $sale->status();
            }
        } else {
            // Si no es un array agregamos a $status el estado único.
            $symbol = $tdo->country()->currency_symbol;
            $status = $tdo->status();
        }

        // Removemos caracteres extras de los estados y precios
        $status = trim($status, " / ");

        // Asignamos el símbolo
        $property->currency_symbol = $symbol;

        // Asignamos el estado a la propiedad
        $property->status = $status;

        // Ambientes

        // Obtenemos los ambientes
        $environments = $property->environments;
        $partial_envs = [];

        // Iteramos los ambientes
        foreach($environments as $env)
        {
            // Creamos un objeto y asignamos su valor
            $obj = new \StdClass;
            $obj->type = $env->environment->type;
            $obj->value = $env->value;

            // Asignamos a un array el nombre de cada ambiente y su valor
            $partial_envs[$env->environment->name] = $obj;
        }

        // Asignamos el array de ambientes creado a la propiedad
        $property->environments = $partial_envs;

        // Equipamientos

        // Obtenemos los equipamientos
        $equipments = $property->equipments;
        $partial_eq = [];

        // Iteramos los equipamientos
        foreach($equipments as $eq)
        {
            // Creamos un objeto y asignamos su valor
            $obj = new \StdClass;
            $obj->type = $eq->equipment->type;
            $obj->value = $eq->value;

            // Asignamos a un array el nombre de cada equipamiento y su valor
            $partial_eq[$eq->equipment->name] = $obj;
        }

        // Asignamos el array de equipamientos creado a la propiedad
        $property->equipments = $partial_eq;

        // Exteriores

        // Obtenemos los exteriores
        $exteriors = $property->exteriors;
        $partial_ext = [];

        // Iteramos los exteriores
        foreach($exteriors as $ext)
        {
            // Creamos un objeto y asignamos su valor
            $obj = new \StdClass;
            $obj->type = $ext->exterior->type;
            $obj->value = $ext->value;

            // Asignamos a un array el nombre de cada exterior y su valor
            $partial_ext[$ext->exterior->name] = $obj;
        }

        // Asignamos el array de exteriores creado a la propiedad
        $property->exteriors = $partial_ext;

        // Servicios y Amenities

        // Obtenemos los Servicios y Amenities
        $services_amenities = $property->services_amenities;
        $partial_sa = [];

        // Iteramos los servicios y amenities
        foreach($services_amenities as $sa)
        {
            // Creamos un objeto y asignamos su valor
            $obj = new \StdClass;
            $obj->type = $sa->service_amenity->type;
            $obj->value = $sa->value;

            // Asignamos a un array el nombre de cada servicio y comodidad y su valor
            $partial_sa[$sa->service_amenity->name] = $obj;
        }

        // Asignamos el array de Servicios y Amenities creados a la propiedad
        $property->services_amenities = $partial_sa;

        // Asignamos la imagen de portada a la propiedad
        $property->cover_image = $property->coverImage();
        $property->images = $property->images;

        $tdo = $property->tdo();
        $tdi = $property->tdi();

        $property->tdo = $property->getTdo();
        $property->tdi = $property->getTdi();

        $view = null;

        switch($tdi)
        {
            case 'apartment':
                switch($tdo)
                {
                    case 'rent':
                        $view = 'apartamento-alquiler';
                        break;
                    case 'sale':
                        $view = 'apartamento-venta';
                        break;
                    case 'rent-sale':
                        $view = 'apartamento-alquiler-venta';
                        break;
                }
                break;
            case 'business_premise':
                switch($tdo)
                {
                    case 'rent':
                        $view = 'locales-alquiler';
                        break;
                    case 'sale':
                        $view = 'locales-venta';
                        break;
                    case 'rent-sale':
                        $view = 'locales-alquiler-venta';
                        break;
                }
                break;
            case 'countryside':
                $view = 'chacras-campos';
                break;
            case 'house':
                switch($tdo)
                {
                    case 'rent':
                        $view = 'casa-alquiler';
                        break;
                    case 'sale':
                        $view = 'casa-venta';
                        break;
                    case 'rent-sale':
                        $view = 'casa-alquiler-venta';
                        break;
                }
                break;
            case 'land':
                $view = 'terrenos';
                break;
        }

        if($view) {
            return view('frontend.' . $view, compact('property'));
        }

        return view('errors.404');
    }

    public function currency(Request $request, \App\Country $country)
    {
        return back()->withCookie(cookie()->forever('currency', $country->id));
    }

    public function num_format($number)
    {
        return number_format($number,0,",",".");
    }
}