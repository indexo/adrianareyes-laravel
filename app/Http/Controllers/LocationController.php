<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use Response;
use Validator;

use App\Location;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localities = Location::all();

        return view('admin.localities.index', compact('localities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.localities.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'                  => 'required',
            'department_id'         => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'department_id.required'=> 'El departamento es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $location = new Location;
        $location->name = $request->name;
        $location->department_id = $request->department_id;

        if(!$location->save()) {
            parent::message(
                'Hubo un error al ingresar la localidad. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La localidad ha sido creada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('LocationController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    public function show(App\Location $location)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Location $location)
    {
        return view('admin.localities.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Location $location)
    {
        $rules = [
            'name'                  => 'required',
            'department_id'         => 'required',
        ];

        $messages = [
            'name.required'         => 'El nombre es obligatorio',
            'department_id.required'=> 'El departamento es obligatorio',
        ];

        // Validamos la información ingresada
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        $location->name = $request->name;
        $location->department_id = $request->department_id;

        if(!$location->save()) {
            parent::message(
                'Hubo un error al actualizar la localidad. Intente de nuevo on contacte con un administrador',
                '¡Error!',
                'error'
            );

            return back()->withInput();
        }

        parent::message(
            'La localidad ha sido actualizada correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('LocationController@edit', $location));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Location $location)
    {
        if(!$location->delete()) {
            parent::message(
                'Hubo un problema al eliminar la localidad. Intente de nuevo o contacte con un administrador.',
                '¡Error!',
                'error'
            );
        } else {
            parent::message(
                'La localidad ha sido eliminada correctamente',
                '¡&Eacute;xito!',
                'success'
            );
        }

        return redirect(action('LocationController@index'));
    }
}
