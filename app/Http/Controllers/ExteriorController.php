<?php

namespace App\Http\Controllers;

use App\Exterior;
use Illuminate\Http\Request;
use Validator;

class ExteriorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exterior = Exterior::all();

        return view('admin.exterior.index', compact('exterior'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.exterior.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:exterior',
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $exterior = new Exterior;

        $exterior->name = $request->name;
        $exterior->type = $request->type;
        $exterior->placeholder = $request->placeholder;

        if(!$exterior->save()) {
            parent::message(
                'Hubo un problema al guardar el exterior. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El exterior se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ExteriorController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exterior  $exterior
     * @return \Illuminate\Http\Response
     */
    public function show(Exterior $exterior)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exterior  $exterior
     * @return \Illuminate\Http\Response
     */
    public function edit(Exterior $exterior)
    {
        return view('admin.exterior.edit', compact('exterior'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exterior  $exterior
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exterior $exterior)
    {

        if($request->name != $exterior->name) {
            $name = 'required|unique:exterior';
        } else {
            $name = 'required';
        }
        $rules = [
            'name' => $name,
            'type' => 'required'
        ];
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre ya existe',
            'type.required' => 'El tipo es obligatorio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            parent::message(
                $validator->errors(),
                '¡Error!',
                'error'
            );

            return back();
        }

        $exterior->name = $request->name;
        $exterior->type = $request->type;
        $exterior->placeholder = $request->placeholder;

        if(!$exterior->save()) {
            parent::message(
                'Hubo un problema al guardar el exterior. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El exterior se guard&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ExteriorController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exterior  $exterior
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exterior $exterior)
    {
        if(!$exterior->delete()) {
            parent::message(
                'Hubo un problema al eliminar el exterior. Intenta nuevamente o contacta un administrador',
                '¡Error!',
                'error'
            );

            return back();
        }

        parent::message(
            'El exterior se elimin&oacute; correctamente',
            '¡&Eacute;xito!',
            'success'
        );

        return redirect(action('ExteriorController@index'));
    }
}
