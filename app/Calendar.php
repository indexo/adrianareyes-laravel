<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    public $table = 'calendar';

    protected $fillable = [
    	'title',
		'description',
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'user_id',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
