<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countryside extends Model
{
    public $table = 'countrysides';

    protected $fillable = [
    	'property_id',
        'location',
        'access',
        'topography',
        'activity',
        'productivity',
        'spec_cutwater',
        'spec_cutwater_description',
        'spec_wiring',
        'spec_wiring_description',
        'spec_rivers_streams',
        'spec_rivers_streams_description',
        'spec_hills',
        'spec_hills_description',
        'spec_light',
        'spec_light_description',
        'house',
        'guest_room',
        'guest_room_rooms',
        'guest_room_bathrooms',
        'guest_room_kitchen',
        'guest_room_kitchenette',
        'guest_room_living_room',
        'sheds',
        'stables',
        'others',
    ];

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }

    public function specifications()
    {
    	$spec = new \Stdclass;

    	$spec->cutwater 		= $this->spec_cutwater;
    	$spec->wiring 			= $this->spec_wiring;
    	$spec->rivers_streams 	= $this->spec_rivers_streams;
    	$spec->hills 			= $this->spec_hills;
    	$spec->light 			= $this->spec_light;

    	return $spec;
    }
}
