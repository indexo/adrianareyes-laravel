<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceAmenity extends Model
{
    public $table = 'services_amenities';

    protected $fillable = [
    	'name',
		'placeholder',
    	'type'
    ];

    public function options()
	{
		return $this->hasMany('App\ServiceAmenityOption');
	}

    public function values()
    {
        return $this->hasMany('App\ServiceAmenityValue');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Property');
    }
}
