<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $table = 'images';

	protected $fillable = [
		'name',
		'property_id',
		'cover',
	];

	public function property()
	{
		return $this->belongsTo('App\Property');
	}

	public function is_cover()
	{
		return ($this->cover === 1);
	}
}
