<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exterior extends Model
{
    public $table = 'exterior';

    protected $fillable = [
    	'name',
		'placeholder',
    	'type'
    ];

    public function values()
    {
        return $this->hasMany('App\ExteriorValue');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Property');
    }
}
