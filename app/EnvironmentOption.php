<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnvironmentOption extends Model
{
    public $table = 'environment_options';

    protected $fillable = [
    	'text',
    	'environment_id',
    ];

    public function environment()
    {
    	return $this->belongsTo('App\Environment');
    }
}
