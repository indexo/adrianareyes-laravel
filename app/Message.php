<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $table = 'messages';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'message',
        'ip',
        'user_id',
        'parent_message_id',
        'property_id',
    ];

    // Relación de pertenencia 1-1 con App\User
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // Relación de pertenencia 1-1 con App\Message
    public function parent_message()
    {
        return $this->belongsTo('App\Message');
    }

    public function childMessages()
    {
        return $this->hasMany('App\Message', 'parent_message_id');
    }

    // Función: Devuelve todas las respuestas del mensaje
    public function replies()
    {
        return $this->where('parent_message_id', $this->id)->get();
    }

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
