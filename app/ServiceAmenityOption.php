<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceAmenityOption extends Model
{
    public $table = 'service_amenity_options';

    protected $fillable = [
    	'text',
    	'service_amenity_id',
    ];

    public function serviceAmenity()
    {
    	return $this->belongsTo('App\ServiceAmenity');
    }
}
