<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public $table = 'offices';

    protected $fillable = [
    	'address',
        'city',
        'country',
        'phones',
        'emails',
        'google_map_code',
        'open_mon',
        'close_mon',
        'open_tue',
        'close_tue',
        'open_wed',
        'close_wed',
        'open_thu',
        'close_thu',
        'open_fri',
        'close_fri',
        'open_sat',
        'close_sat',
        'open_sun',
        'close_sun',
        'schedules',
        'is_main',
    ];

    public function phonify()
    {
        $phones = $this->phones;
        if($phones) {
            $array = explode(',', $phones);
            $phones = '';
            foreach($array as $a)
            {
                $_a = $a;
                if($a[0] == '0') {
                    $_a = ltrim($a, '0');
                }

                $phones .= "<a href=\"tel:00598{$_a}\" target=\"_blank\">{$a}</a> | ";
            }
        }

        return trim($phones, "| ");
    }

    public function emailify()
    {
        $emails = $this->emails;
        if($emails) {
            $array = explode(',', $emails);
            $emails = '';
            foreach($array as $a)
            {
                $_a = $a;
                if($a[0] == '0') {
                    $_a = ltrim($a, '0');
                }

                $emails .= "<a href=\"mailto:{$_a}\">{$a}</a> | ";
            }
        }

        return trim($emails, "| ");
    }

    public function main()
    {
        if($this->is_main) {
            return $this->is_main > 0;
        }
        return false;
    }
}
