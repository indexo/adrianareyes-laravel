<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $table = 'departments';

    protected $fillable = [
    	'name',
    	'country_id',
    ];

    public function country()
    {
    	return $this->belongsTo('App\Country');
    }

    public function localities()
    {
    	return $this->hasMany('App\Location');
    }
}
