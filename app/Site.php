<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    public $table = 'site';
    protected $primaryKey = 'company_name';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'company_name',
        'company_text',
        'logo',
    	'services',
    	'about_us',
    	'contact_us',
    	'new_properties_title',
    	'new_properties_text',
        'index_image',
    ];

    public function _services()
    {
    	return App\Service::all();
    }

    public function slider()
    {
    	return App\Slider::all();
    }
}
