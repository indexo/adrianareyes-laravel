<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    public $table = 'equipment';

	protected $fillable = [
		'name',
		'placeholder',
		'type'
	];

	public function options()
	{
		return $this->hasMany('App\EquipmentOption');
	}

	public function values()
    {
        return $this->hasMany('App\EquipmentValue');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Property');
    }
}
