<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyEnvironment extends Model
{
    public $table = 'property_environments';

    protected $fillable = [
    	'environment_id',
    	'property_id',
    	'value',
    ];

    public function environment()
    {
    	return $this->belongsTo('App\Environment');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
