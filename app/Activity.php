<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public $table = 'activities';

    protected $fillable = [
    	'remote_addr',
    	'http_user_agent',
    	'request_uri',
        'referer',
        'country',
    	'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
