<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    public $table = 'zones';

    protected $fillable = [
    	'name',
    	'location_id',
    ];

    public function location()
    {
    	return $this->belongsTo('App\Location');
    }
}
