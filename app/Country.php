<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $table = 'countries';

    protected $fillable = [
    	'name',
    	'code',
    	'iso2',
        'currency',
        'currency_symbol',
        'currency_exchange',
    ];

    public function departments()
    {
    	return $this->hasMany('App\Department');
    }
}
