<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPremise extends Model
{
	public $table = 'business_premises';
	
    protected $fillable = [
    	'property_id',
		'floors',
		'low_level',
		'mezzanine',
		'subsoil',
		'bathrooms',
		'kitchenette',
		'private',
		'kitchen',
    ];
}