<?php

// Funcion para devolver la URL con el DIRECTORY SEPARATOR incluido
if (! function_exists('ds')) {
    function ds($callback)
    {
        $url = $callback;
        if (DIRECTORY_SEPARATOR === '/') {
            $url = str_replace('\\', DIRECTORY_SEPARATOR, $callback);
        }
        if (DIRECTORY_SEPARATOR === '\\') {
            $url = str_replace('/', DIRECTORY_SEPARATOR, $callback);
        }
        return $url;
    }
}