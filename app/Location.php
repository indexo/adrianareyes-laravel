<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public $table = 'localities';

    protected $fillable = [
    	'name',
    	'department_id',
    ];

    public function department()
    {
    	return $this->belongsTo('App\Department');
    }

    public function zones()
    {
    	return $this->hasMany('App\Zone');
    }
}
