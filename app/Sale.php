<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    // Propiedades editables
    protected $fillable = [
    	'property_id',
        'currency_country_id',
        'price',
        'financing',
        'sold',
    ];

    // Relación de pertenencia 1-1 con App\Property
    public function property()
    {
    	return $this->belongsTo('App\Property');
    }

    // Función: Devuelve una cadena de texto: Vendido o En Venta
    public function status()
    {
        return $this->sold ? 'Vendido' : 'En venta';
    }

    // Función: Devuelve un valor boolean: Si el inmueble está marcado como vendido (TRUE) o no (FALSE)
    public function sold()
    {
        return (boolean) $this->sold;
    }

    // Función: Devuelve App\Country para el país marcado como moneda
    public function country()
    {
        return Country::where('id', $this->currency_country_id)->first();
    }

    // Función: Obtiene el costo del inmueble y devuelve un valor float
    public function cost()
    {
        return $this->country()->currency_exchange > 0
            ?(float) $this->price / $this->country()->currency_exchange
            : (float) $this->price;
    }
}
