<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reply extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instancia Message
     *
     * @var Message
     */
    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $site = \App\Site::first();
        $company_name = isset($site)
            ? $site->company_name
            : env('APP_NAME');

        return $this->from([ 'from' => env('MAIL_USERNAME'), 'name '=> $this->message->user->name ])
            ->subject($company_name . ': Respuesta de contacto')
            ->view('emails.reply')
            ->with([
                'data' => $this->message,
                'site' => $site,
                'offices' => \App\Office::all()
            ]);
    }
}
