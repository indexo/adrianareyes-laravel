<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Message extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instancia Message
     *
     * @var Message
     */
    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $site = \App\Site::first();
        $company_name = isset($site)
            ? $site->company_name
            : env('APP_NAME');

        return $this->from(env('MAIL_USERNAME'))
                    ->subject($company_name . ': Su mensaje ha sido recibido.')
                    ->view('emails.message')
                    ->with([
                        'data' => $this->message,
                        'site' => $site,
                        'offices' => \App\Office::all()
                    ]);
    }
}
