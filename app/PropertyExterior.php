<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyExterior extends Model
{
    public $table = 'property_exterior';

    protected $fillable = [
    	'exterior_id',
    	'property_id',
    	'value',
    ];

    public function exterior()
    {
    	return $this->belongsTo('App\Exterior');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
