<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentOption extends Model
{
    public $table = 'equipment_options';

    protected $fillable = [
    	'text',
    	'equipment_id',
    ];

    public function equipment()
    {
    	return $this->belongsTo('App\Equipment');
    }
}
