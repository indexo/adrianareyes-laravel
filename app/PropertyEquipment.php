<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyEquipment extends Model
{
    public $table = 'property_equipment';

    protected $fillable = [
    	'equipment_id',
    	'property_id',
    	'value',
    ];

    public function equipment()
    {
    	return $this->belongsTo('App\Equipment');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
