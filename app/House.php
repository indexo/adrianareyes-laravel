<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    public $table = 'houses';

    protected $fillable = [
    	'property_id',
    	'guest_room',
        'guest_room_rooms',
        'guest_room_bathrooms',
        'guest_room_kitchen',
        'guest_room_kitchenette',
        'guest_room_living_room',
    ];
}
