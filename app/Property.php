<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    // Tabla
    public $table = 'properties';

    // Propiedades disponibles
    protected $fillable = [
    	'ground_footage',
        'built_footage',
        'area',
        'distance_from_sea',
        'distance_from_beach',
        'zone_id',
        'address',
        'currency_country_id',
        'expenses',
        'expenses_range',
        'expenses_school_tax',
        'expenses_property_tax',
        'expenses_gardener',
        'expenses_pool_maintenance',
        'expenses_alarm_vigilance',
        'description',
        'notes',
    ];

    // Relación: Devuelve la relación de pertenencia con App\Zone
    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    // Función: Devuelve un string con el nombre del tipo de inmueble de dicha propiedad
    public function tdi()
    {
        if($this->apartment) { return 'apartment'; }
        if($this->business_premise) { return 'business_premise'; }
        if($this->countryside) { return 'countryside'; }
        if($this->house) { return 'house'; }
        if($this->land) { return 'land'; }
    }

    // Función: Devuelve un string con el nombre del tipo de operación de dicha propiedad
    public function tdo()
    {
        if($this->sale && $this->rent) { return 'rent-sale'; }
        if($this->rent) { return 'rent'; }
        if($this->sale) { return 'sale'; }
    }

    // Función: Devuelve la relación 1-1 del tipo de inmueble de la función $this->tdi()
    public function getTdi()
    {
        return $this->{$this->tdi()};
    }

    // Función: Devuelve la relación 1-1 del tipo de operación de la función $this->tdo()
    public function getTdo()
    {
        $return = ($this->tdo() === 'rent-sale')
            ? [ $this->rent, $this->sale ]
            : $this->{$this->tdo()};

        return $return;
    }

    // Relación: Devuelve la relación 1-m con App\Image
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    // Función: Devuelve la primera imagen seleccionada como portada
    public function coverImage()
    {
        return $this->images->where('cover', true)->first();
    }

    // Tipo de Inmueble:

    // Relación: Devuelve la relación 1-1 con App\Apartment
    public function apartment()
    {
        return $this->hasOne('App\Apartment');
    }

    // Relación: Devuelve la relación 1-1 con App\BusinessPremise
    public function business_premise()
    {
        return $this->hasOne('App\BusinessPremise');
    }

    // Relación: Devuelve la relación 1-1 con App\Countryside
    public function countryside()
    {
        return $this->hasOne('App\Countryside');
    }

    // Relación: Devuelve la relación 1-1 con App\House
    public function house()
    {
        return $this->hasOne('App\House');
    }

    // Relación: Devuelve la relación 1-1 con App\Land
    public function land()
    {
        return $this->hasOne('App\Land');
    }

    // Tipo de Operación:

    // Relación: Devuelve la relación 1-1 con App\Sale
    public function sale()
    {
        return $this->hasOne('App\Sale');
    }

    // Relación: Devuelve la relación 1-1 con App\Rent
    public function rent()
    {
        return $this->hasOne('App\Rent');
    }

    // Extras:

    // Relación: Devuelve la relación 1-m con App\PropertyEnvironment
    public function environments()
    {
        return $this->hasMany('App\PropertyEnvironment');
    }

    // Relación: Devuelve la relación 1-m con App\PropertyEquipment
    public function equipments()
    {
        return $this->hasMany('App\PropertyEquipment');
    }

    // Relación: Devuelve la relación 1-m con App\PropertyExterior
    public function exteriors()
    {
        return $this->hasMany('App\PropertyExterior');
    }

    // Relación: Devuelve la relación 1-m con App\PropertyServiceAmenity
    public function services_amenities()
    {
        return $this->hasMany('App\PropertyServiceAmenity');
    }

    // Función: Devuelve el primer objeto App\PropertyEnvironment cuyo ID de propiedad sea el de la misma en cuestión y el ID del ambiente sea el pasado como argumento
    public function environment($id)
    {
        return PropertyEnvironment::where('property_id', $this->id)
            ->where('environment_id', $id)->first();
    }

    // Función: Devuelve el primer objeto App\PropertyEquipment cuyo ID de propiedad sea el de la misma en cuestión y el ID del equipamiento sea el pasado como argumento
    public function equipment($id)
    {
        return PropertyEquipment::where('property_id', $this->id)
            ->where('equipment_id', $id)->first();
    }

    // Función: Devuelve el primer objeto App\PropertyExterior cuyo ID de propiedad sea el de la misma en cuestión y el ID del exterior sea el pasado como argumento
    public function exterior($id)
    {
        return PropertyExterior::where('property_id', $this->id)
            ->where('exterior_id', $id)->first();
    }

    // Función: Devuelve el primer objeto App\PropertyServiceAmenity cuyo ID de propiedad sea el de la misma en cuestión y el ID del servicio y comodidad sea el pasado como argumento
    public function service_amenity($id)
    {
        return PropertyServiceAmenity::where('property_id', $this->id)
            ->where('service_amenity_id', $id)->first();
    }

    // Función: Devuelve la relación App\Apartment o false dependiendo de la relación $this->apartment
    public function hasApartment()
    {
        return $this->apartment ?? false;
    }

    // Función: Devuelve la relación App\BusinessPremise o false dependiendo de la relación $this->business_premise
    public function hasBusinessPremise()
    {
        return $this->business_premise ?? false;
    }

    // Función: Devuelve la relación App\Countryside o false dependiendo de la relación $this->countryside
    public function hasCountryside()
    {
        return $this->countryside ?? false;
    }

    // Función: Devuelve la relación App\House o false dependiendo de la relación $this->house
    public function hasHouse()
    {
        return $this->house ?? false;
    }

    // Función: Devuelve la relación App\Land o false dependiendo de la relación $this->land
    public function hasLand()
    {
        return $this->land ?? false;
    }

    // Función: Devuelve una cadena de texto amigable para ser mostrada en cada propiedad como posible título
    public function label()
    {
        $property = 'Inmueble';
        $location = $this->zone->location->name;

        switch($this->tdi())
        {
            case 'apartment':
                $property = 'Apartamento';
                break;
            case 'business_premise':
                $property = 'Local comercial';
                break;
            case 'countryside':
                $property = 'Chacra / Campo';
                break;
            case 'house':
                $property = 'Casa';
                break;
            case 'land':
                $property = 'Terreno';
                break;
        }

        return "{$property} en {$location}";
    }

    // Función: Devuelve el país (App\Country) seleccionado en el tipo de moneda de gastos en los inmuebles
    public function country()
    {
        return Country::where('id', $this->currency_country_id)->first();
    }

    // Función: Devuelve un código de referencia del inmueble (Ej.: "APVE0001")
    public function reference()
    {
        $code = '';
        $suffix = '000';

        switch($this->tdi())
        {
            case 'apartment':
                $code .= 'AP';
                break;
            case 'business_premise':
                $code .= 'LC';
                break;
            case 'countryside':
                $code .= 'CC';
                break;
            case 'house':
                $code .= 'CA';
                break;
            case 'land':
                $code .= 'TE';
                break;
        }

        switch($this->tdo())
        {
            case 'rent':
                $code .= 'AL';
                break;
            case 'sale':
                $code .= 'VE';
                break;
            case 'rent-sale':
                $code .= 'AV';
                break;
        }

        $code .= $suffix . $this->id;

        return $code;
    }

    public static function get($reference = null)
    {
        if($reference) {
            $exp = explode("000", $reference);
        } else {
            $exp = explode("000", self::reference());
        }

        return self::find($exp[1]);
    }

    // Función: Busca las comodidades del inmueble y devuelve la entidad o FALSE
    public function searchEnv($name)
    {
        foreach($this->environments as $env)
        {
            if($env->environment->name == $name) {
                return $env;
            }
        }

        return false;
    }

}
