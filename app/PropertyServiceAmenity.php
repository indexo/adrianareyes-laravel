<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyServiceAmenity extends Model
{
    public $table = 'property_services_amenities';

    protected $fillable = [
    	'service_amenity_id',
    	'property_id',
    	'value',
    ];

    public function service_amenity()
    {
    	return $this->belongsTo('App\ServiceAmenity');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
