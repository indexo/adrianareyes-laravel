<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Environment extends Model
{
    public $table = 'environments';

    protected $fillable = [
    	'name',
		'placeholder',
    	'type'
    ];

    // Relación de pertenencia: 1-M con EnvironmentOption
    public function options()
    {
    	return $this->hasMany('App\EnvironmentOption');
    }

    // Relación de pertenencia: 1-1 con EnvironmentValue
    public function value()
    {
        return $this->hasOne('App\EnvironmentValue');
    }

    public function values()
    {
        return $this->hasMany('App\EnvironmentValue');
    }

    // Relación de pertenencia aa Property
    public function properties()
    {
        return $this->belongsToMany('App\Property');
    }
}
