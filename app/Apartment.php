<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    public $table = 'apartments';

    protected $fillable = [
    	'property_id',
    ];
}
