<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable = [
    	'property_id',
        'currency_country_id',
        'january',
        'january_1st_fortnight',
        'january_2nd_fortnight',
        'february',
        'february_1st_fortnight',
        'february_2nd_fortnight',
        'season',
        'easter',
        'winter',
        'high_season_daily',
        'high_season_daily_min_days',
        'low_season_daily_min_days',
        'annual',
        'temporary',
        'rented',
    ];

    // Relación: Devuelve la relación de pertenencia con App\Property
    public function property()
    {
    	return $this->belongsTo('App\Property');
    }

    // Función: Devuelve un string con la evaluación del estado $this->rented
    public function status()
    {
        return $this->rented ? 'Alquilado' : 'En alquiler';
    }

    // Función: Devuelve la evaluación de $this->rented (boolean)
    public function rented()
    {
        return (boolean) $this->rented;
    }

    // Función: Devuelve el objeto App\Country donde el ID corresponde con la moneda seleccionada en la propiedad
    public function country()
    {
        return Country::where('id', $this->currency_country_id)->first();
    }

    // Función: Obtiene el costo del inmueble y devuelve un valor float
    public function cost()
    {
        return $this->country()->currency_exchange > 0
            ?(float) $this->january / $this->country()->currency_exchange
            : (float) $this->january;
    }
}
