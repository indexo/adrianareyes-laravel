<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    public $table = 'lands';

    protected $fillable = [
    	'frontyard',
        'backyard',
        'sides',
        'FOT',
        'FOS',
        'ret_frontyard',
        'ret_backyard',
        'ret_bilateral',
    ];
}
