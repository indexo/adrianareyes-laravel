<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessPremisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_premises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->integer('floors')->nullable();
            $table->integer('low_level')->nullable();
            $table->integer('mezzanine')->nullable();
            $table->integer('subsoil')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->boolean('kitchenette')->nullable();
            $table->boolean('private')->nullable();
            $table->boolean('kitchen')->nullable();
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_premises', function (Blueprint $table) {
            $table->dropForeign(['property_id']);

            $table->dropIfExists('business_premises');
        });
    }
}
