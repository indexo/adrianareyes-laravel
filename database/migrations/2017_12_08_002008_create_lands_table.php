<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->integer('frontyard')->nullable();
            $table->integer('backyard')->nullable();
            $table->integer('sides')->nullable();
            $table->integer('FOT')->nullable();
            $table->integer('FOS')->nullable();
            $table->integer('ret_frontyard')->nullable();
            $table->integer('ret_backyard')->nullable();
            $table->integer('ret_bilateral')->nullable();
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lands', function (Blueprint $table) {
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('lands');
        });
    }
}
