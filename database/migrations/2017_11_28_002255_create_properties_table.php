<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ground_footage')->nullable();
            $table->integer('built_footage')->nullable();
            $table->integer('area')->nullable();
            $table->integer('distance_from_sea')->nullable();
            $table->integer('distance_from_beach')->nullable();
            $table->integer('zone_id')->unsigned();
            $table->string('address')->nullable();
            $table->integer('currency_country_id')->nullable();
            $table->integer('expenses')->nullable();
            $table->string('expenses_range')->nullable();
            $table->integer('expenses_school_tax')->nullable();
            $table->integer('expenses_property_tax')->nullable();
            $table->integer('expenses_gardener')->nullable();
            $table->integer('expenses_pool_maintenance')->nullable();
            $table->integer('expenses_alarm_vigilance')->nullable();
            $table->string('description')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();

            $table->foreign('zone_id')
                ->references('id')
                ->on('zones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign(['zone_id']);
            $table->dropIfExists('properties');
        });
    }
}