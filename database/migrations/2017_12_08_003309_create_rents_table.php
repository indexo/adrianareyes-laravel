<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->integer('currency_country_id')->nullable();
            $table->integer('january')->nullable();
            $table->integer('january_1st_fortnight')->nullable();
            $table->integer('january_2nd_fortnight')->nullable();
            $table->integer('february')->nullable();
            $table->integer('february_1st_fortnight')->nullable();
            $table->integer('february_2nd_fortnight')->nullable();
            $table->integer('season')->nullable();
            $table->integer('easter')->nullable();
            $table->integer('winter')->nullable();
            $table->integer('high_season_daily')->nullable();
            $table->integer('high_season_daily_min_days')->nullable();
            $table->integer('low_season_daily')->nullable();
            $table->integer('low_season_daily_min_days')->nullable();
            $table->integer('temporary')->nullable();
            $table->integer('annual')->nullable();
            $table->boolean('rented');
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rents', function (Blueprint $table) {
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('rents');
        });
    }
}
