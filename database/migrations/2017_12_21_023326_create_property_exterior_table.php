<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyExteriorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_exterior', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exterior_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            $table->foreign('exterior_id')
                ->references('id')
                ->on('exterior')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_exterior', function (Blueprint $table) {
            $table->dropForeign(['exterior_id']);
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('property_exterior');
        });
    }
}
