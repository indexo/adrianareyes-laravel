<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site', function (Blueprint $table) {
            $table->string('company_name')->nullable();
            $table->text('company_text')->nullable();
            $table->string('logo')->nullable();
            $table->text('services')->nullable();
            $table->text('about_us')->nullable();
            $table->text('contact_us')->nullable();
            $table->string('new_properties_title')->nullable();
            $table->text('new_properties_text')->nullable();
            $table->string('index_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site');
    }
}
