<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyServicesAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_services_amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_amenity_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            $table->foreign('service_amenity_id')
                ->references('id')
                ->on('services_amenities')
                ->onDelete('cascade');
                
            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_services_amenities', function (Blueprint $table) {
            $table->dropForeign(['service_amenity_id']);
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('property_services_amenities');
        });
    }
}
