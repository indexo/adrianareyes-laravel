<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->boolean('guest_room')->nullable();
            $table->integer('guest_room_rooms')->nullable();
            $table->integer('guest_room_bathrooms')->nullable();
            $table->boolean('guest_room_kitchen')->nullable();
            $table->boolean('guest_room_kitchenette')->nullable();
            $table->boolean('guest_room_living_room')->nullable();
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('houses');
        });
    }
}
