<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyEnvironmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_environments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('environment_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            $table->foreign('environment_id')
                ->references('id')
                ->on('environments')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_environments', function (Blueprint $table) {
            $table->dropForeign(['environment_id']);
            $table->dropForeign(['property_id']);
            
            $table->dropIfExists('property_environments');
        });
    }
}
