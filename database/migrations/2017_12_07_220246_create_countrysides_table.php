<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountrysidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countrysides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->string('location')->nullable();
            $table->string('access')->nullable();
            $table->string('topography')->nullable();
            $table->string('activity')->nullable();
            $table->string('productivity')->nullable();
            $table->boolean('spec_cutwater')->nullable();
            $table->string('spec_cutwater_description')->nullable();
            $table->boolean('spec_wiring')->nullable();
            $table->string('spec_wiring_description')->nullable();
            $table->boolean('spec_rivers_streams')->nullable();
            $table->string('spec_rivers_streams_description')->nullable();
            $table->boolean('spec_hills')->nullable();
            $table->string('spec_hills_description')->nullable();
            $table->boolean('spec_light')->nullable();
            $table->string('spec_light_description')->nullable();
            $table->boolean('house')->nullable();
            $table->boolean('guest_room')->nullable();
            $table->integer('guest_room_rooms')->nullable();
            $table->integer('guest_room_bathrooms')->nullable();
            $table->boolean('guest_room_kitchen')->nullable();
            $table->boolean('guest_room_kitchenette')->nullable();
            $table->boolean('guest_room_living_room')->nullable();
            $table->boolean('sheds')->nullable();
            $table->string('sheds_description')->nullable();
            $table->boolean('stables')->nullable();
            $table->string('stables_description')->nullable();
            $table->boolean('others')->nullable();
            $table->string('others_description')->nullable();
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countrysides', function (Blueprint $table) {
            $table->dropForeign(['property_id']);

            $table->dropIfExists('countrysides');
        });
    }
}
