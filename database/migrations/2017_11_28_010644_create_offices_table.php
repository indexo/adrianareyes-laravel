<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address')->unique();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('phones')->nullable();
            $table->string('emails')->nullable();
            $table->text('google_map_code')->nullable();
            $table->time('open_mon')->nullable();
            $table->time('close_mon')->nullable();
            $table->time('open_tue')->nullable();
            $table->time('close_tue')->nullable();
            $table->time('open_wed')->nullable();
            $table->time('close_wed')->nullable();
            $table->time('open_thu')->nullable();
            $table->time('close_thu')->nullable();
            $table->time('open_fri')->nullable();
            $table->time('close_fri')->nullable();
            $table->time('open_sat')->nullable();
            $table->time('close_sat')->nullable();
            $table->time('open_sun')->nullable();
            $table->time('close_sun')->nullable();
            $table->text('schedules')->nullable();
            $table->boolean('is_main')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
