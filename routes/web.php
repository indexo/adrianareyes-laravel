<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
| Donde las rutas tienen "->where('{propiedad}', '[0-9]+')" sirve para evitar el XSS 
| y mejorar el redireccionamiento de rutas.
|
*/

Route::get('desconectar', function() {
	if(Auth::check()) {
		Auth::logout();
	}
	return back();
})->name('desconectar');

Route::middleware('activity')->group(function() {

    // Administración - inicio
    Route::middleware('auth')->prefix('administracion')->group(function() {

        // Esto debería ser accesible SÓLO la primera vez
        Route::get('configuracion', 'InstallationController@index');
        Route::post('configuracion/BD', 'InstallationController@createDB');
        Route::post('configuracion/BD/reiniciar', 'InstallationController@resetConf');
        Route::post('configuracion/BD/migrar', 'InstallationController@migrateTables');
        Route::post('configuracion/BD/migrar/reiniciar', 'InstallationController@resetTables');
        Route::post('configuracion/Email', 'InstallationController@configEmail');
        Route::post('configuracion/BD/exportar', 'InstallationController@exportDB');
        Route::post('configuracion/actividades/limpiar', 'InstallationController@erase_activities');

        // Índice
        Route::get('/', 'WebController@admin');

        // Propiedades
        //Route::resource('propiedades', 'PropertyController');
        Route::prefix('inmuebles')->group(function() {
            Route::get('/', 'PropertyController@index');

            Route::get('nuevo', 'PropertyController@create');
            Route::post('/', 'PropertyController@store');

            Route::get('{property}/actualizar', 'PropertyController@edit', function(App\Property $property) {})
                ->where('property', '[0-9]+');
            Route::put('{property}', 'PropertyController@update', function(App\Property $property) {})
                ->where('property', '[0-9]+');

            Route::delete('{property}/eliminar', 'PropertyController@destroy', function(App\Property $property) {})
                ->where('property', '[0-9]+');
                
            Route::delete('{property}/imagenes', 'PropertyController@deleteImages', function(App\Property $property) {})
                ->where('property', '[0-9]+');
            Route::post('{property}/imagenes', 'PropertyController@setCover', function(App\Property $property) {});
        });

        // Exterior
        //Route::resource('exterior', 'ExteriorController', [ 'except' => [ 'show' ]]); Por ahora queda como abajo.
        Route::prefix('exterior')->group(function() {
            Route::get('/', 'ExteriorController@index');

            Route::get('nuevo', 'ExteriorController@create');
            Route::post('/', 'ExteriorController@store');

            Route::get('{exterior}/actualizar', 'ExteriorController@edit', function(App\Exterior $exterior) {})
                ->where('exterior', '[0-9]+');
            Route::put('{exterior}', 'ExteriorController@update', function(App\Exterior $exterior) {})
                ->where('exterior', '[0-9]+');

            Route::delete('{exterior}/eliminar', 'ExteriorController@destroy', function(App\Exterior $exterior) {})
                ->where('exterior', '[0-9]+');
        });

        // Comodidades
        //Route::resource('comodidades', 'EnvironmentController', [ 'except' => [ 'show' ]]); Por ahora queda como abajo.
        Route::prefix('comodidades')->group(function() {
            Route::get('/', 'EnvironmentController@index');

            Route::get('nuevo', 'EnvironmentController@create');
            Route::post('/', 'EnvironmentController@store');

            Route::get('{environment}/actualizar', 'EnvironmentController@edit', function(App\Environment $environment) {})
                ->where('environment', '[0-9]+');
            Route::put('{environment}', 'EnvironmentController@update', function(App\Environment $environment) {})
                ->where('environment', '[0-9]+');

            Route::delete('{environment}/eliminar', 'EnvironmentController@destroy', function(App\Environment $environment) {})
                ->where('environment', '[0-9]+');
        });

        // Equipamiento
        //Route::resource('equipamientos', 'EquipmentController');
        Route::prefix('equipamiento')->group(function() {
            Route::get('/', 'EquipmentController@index');

            Route::get('nuevo', 'EquipmentController@create');
            Route::post('/', 'EquipmentController@store');

            Route::get('{equipment}/actualizar', 'EquipmentController@edit', function(App\Equipment $equipment) {})
                ->where('equipment', '[0-9]+');
            Route::put('{equipment}', 'EquipmentController@update', function(App\Equipment $equipment) {})
                ->where('equipment', '[0-9]+');

            Route::delete('{equipment}/eliminar', 'EquipmentController@destroy', function(App\Equipment $equipment) {})
                ->where('equipment', '[0-9]+');
        });

        // Servicios y Comodidades
        //Route::resource('servicios-comodidades', 'ServiceAmenityController');
        Route::prefix('servicios-comodidades')->group(function() {
            Route::get('/', 'ServiceAmenityController@index');

            Route::get('nuevo', 'ServiceAmenityController@create');
            Route::post('/', 'ServiceAmenityController@store');

            Route::get('{service_amenity}/actualizar', 'ServiceAmenityController@edit', function(App\ServiceAmenity $service_amenity) {})
                ->where('service_amenity', '[0-9]+');
            Route::put('{service_amenity}', 'ServiceAmenityController@update', function(App\ServiceAmenity $service_amenity) {})
                ->where('service_amenity', '[0-9]+');

            Route::delete('{service_amenity}/eliminar', 'ServiceAmenityController@destroy', function(App\ServiceAmenity $service_amenity) {})
                ->where('service_amenity', '[0-9]+');
        });

        // Actividades
        Route::get('actividades', 'ActivityController@index');

        // Usuarios
        //Route::resource('usuarios', 'UserController');
        Route::prefix('usuarios')->group(function() {
            Route::get('/', 'UserController@index');

            Route::get('nuevo', 'UserController@create');
            Route::post('/', 'UserController@store');

            Route::get('{user}/actualizar', 'UserController@edit', function(App\User $user) {})
                ->where('user', '[0-9]+');
            Route::put('{user}', 'UserController@update', function(App\User $user) {})
                ->where('user', '[0-9]+');

            Route::delete('{user}/eliminar', 'UserController@destroy', function(App\User $user) {})
                ->where('user', '[0-9]+');

            Route::get('avatar/{user}', 'UserController@getAvatar', function(App\User $user) {});
        });

        // Calendario
        // Route::resource('calendario', 'CalendarController');
        Route::prefix('calendario')->group(function() {
            Route::get('/', 'CalendarController@index');
            Route::post('/', 'CalendarController@store', function(App\Calendar $event) {});
            Route::get('{event}', 'CalendarController@edit', function(App\Calendar $event) {})
                ->where('event', '[0-9]+');
            Route::put('{event}', 'CalendarController@update', function(App\Calendar $event) {})
                ->where('event', '[0-9]+');
            Route::delete('{event}/eliminar', 'CalendarController@destroy', function(App\Calendar $event) {})
                ->where('event', '[0-9]+');
        });

        // Clientes
        // Route::resource('clientes', 'CustomerController');
        Route::prefix('clientes')->group(function() {
            Route::get('/', 'CustomerController@index');

            Route::get('nuevo', 'CustomerController@create');
            Route::post('/', 'CustomerController@store');

            Route::get('{customer}/actualizar', 'CustomerController@edit', function(App\Customer $customer) {})
                ->where('customer', '[0-9]+');
            Route::put('{customer}', 'CustomerController@update', function(App\Customer $customer) {})
                ->where('customer', '[0-9]+');

            Route::delete('{customer}/eliminar', 'CustomerController@destroy', function(App\Customer $customer) {})
                ->where('customer', '[0-9]+');
        });

        // Servicios
        //Route::resource('servicios', 'ServiceController');
        Route::prefix('servicios')->group(function() {
            Route::get('/', 'ServiceController@index');

            Route::get('nuevo', 'ServiceController@create');
            Route::post('/', 'ServiceController@store');

            Route::get('{service}/actualizar', 'ServiceController@edit', function(App\Service $service) {})
                ->where('service', '[0-9]+');
            Route::put('{service}', 'ServiceController@update', function(App\Service $service) {})
                ->where('service', '[0-9]+');

            Route::delete('{service}/eliminar', 'ServiceController@destroy', function(App\Service $service) {})
                ->where('service', '[0-9]+');
        });

        // Sitio
        Route::prefix('sitio')->group(function() {
            Route::get('/', 'SiteController@index');
            Route::post('nosotros', 'SiteController@about_us');
            Route::post('empresa', 'SiteController@company');
            Route::post('contacto', 'SiteController@contact_us');
            Route::post('servicios', 'SiteController@services');
            Route::get('logo', 'SiteController@getLogo');
            Route::post('nuevas-propiedades', 'SiteController@new_properties');
            Route::post('imagen-inicio', 'SiteController@index_image');
            Route::get('imagen-inicio', 'SiteController@getIndexImage');
        });

        // Oficinas
        // Route::resource('oficinas', 'OfficeController');
        Route::prefix('oficinas')->group(function() {
            Route::get('/', 'OfficeController@index');

            Route::get('nuevo', 'OfficeController@create');
            Route::post('/', 'OfficeController@store');

            Route::get('{office}/actualizar', 'OfficeController@edit', function(App\Office $office) {})
                ->where('office', '[0-9]+');
            Route::put('{office}', 'OfficeController@update', function(App\Office $office) {})
                ->where('office', '[0-9]+');

            Route::delete('{office}/eliminar', 'OfficeController@destroy', function(App\Office $office) {})
                ->where('office', '[0-9]+');
        });

        // Países
        // Route::resource('paises', 'CountryController');
        Route::prefix('paises')->group(function() {
            Route::get('/', 'CountryController@index');

            Route::get('nuevo', 'CountryController@create');
            Route::post('/', 'CountryController@store');

            Route::get('{country}/actualizar', 'CountryController@edit', function(App\Country $country) {})
                ->where('country', '[0-9]+');
            Route::put('{country}', 'CountryController@update', function(App\Country $country) {})
                ->where('country', '[0-9]+');

            Route::delete('{country}/eliminar', 'CountryController@destroy', function(App\Country $country) {})
                ->where('country', '[0-9]+');
        });

        // Departamentos
        // Route::resource('departamentos', 'CountryController');
        Route::prefix('departamentos')->group(function() {
            Route::get('/', 'DepartmentController@index');

            Route::get('nuevo', 'DepartmentController@create');
            Route::post('/', 'DepartmentController@store');

            Route::get('{department}/actualizar', 'DepartmentController@edit', function(App\Department $department) {})
                ->where('department', '[0-9]+');
            Route::put('{department}', 'DepartmentController@update', function(App\Department $department) {})
                ->where('department', '[0-9]+');

            Route::delete('{department}/eliminar', 'DepartmentController@destroy', function(App\Department $department) {})
                ->where('department', '[0-9]+');
        });

        // Localidades
        // Route::resource('localidades', 'LocationController');
        Route::prefix('localidades')->group(function() {
            Route::get('/', 'LocationController@index');

            Route::get('nuevo', 'LocationController@create');
            Route::post('/', 'LocationController@store');

            Route::get('{location}/actualizar', 'LocationController@edit', function(App\Location $location) {})
                ->where('location', '[0-9]+');
            Route::put('{location}', 'LocationController@update', function(App\Location $location) {})
                ->where('location', '[0-9]+');

            Route::delete('{location}/eliminar', 'LocationController@destroy', function(App\Location $location) {})
                ->where('location', '[0-9]+');
        });

        // Zonas
        // Route::resource('zonas', 'ZoneController');
        Route::prefix('zonas')->group(function() {
            Route::get('/', 'ZoneController@index');

            Route::get('nuevo', 'ZoneController@create');
            Route::post('/', 'ZoneController@store');

            Route::get('{zone}/actualizar', 'ZoneController@edit', function(App\Zone $zone) {})
                ->where('zone', '[0-9]+');
            Route::put('{zone}', 'ZoneController@update', function(App\Zone $zone) {})
                ->where('zone', '[0-9]+');

            Route::delete('{zone}/eliminar', 'ZoneController@destroy', function(App\Zone $zone) {})
                ->where('zone', '[0-9]+');
        });

        Route::prefix('mensajes')->group(function() {
            Route::get('/', 'MessageController@index');
            Route::get('{message}', 'MessageController@show', function(App\Message $message) {});
            Route::post('{message}', 'MessageController@reply', function(App\Message $message) {});
            Route::delete('{message}', 'MessageController@destroy', function(App\Message $message) {});


        });

    });
    // Administracion - final

    Route::get('moneda/{country}', 'WebController@currency')
        ->where('currency', '[0-9]+');

    // En caso de modo de mantenimiento
    Route::middleware('maintenance_mode')->group(function () {
        // Páginas principales
        Route::get('/', 'WebController@index');
        Route::get('nosotros', 'WebController@about_us');
        Route::get('inmuebles', 'WebController@properties');
        Route::get('inmuebles/{reference}', 'WebController@showProperty', function($reference) {});
        Route::get('contacto', 'WebController@contact_us');
    });

    // Mensajes (formulario de contacto)
    Route::post('mensajes', 'MessageController@store', function(App\Property $property = null) {});

    Route::get('en-construccion', function() {
        return view('maintenance_mode');
    });


}); // /activities middleware

// Imágenes
Route::get('imagenes/inmuebles/{name}', 'PropertyController@getImage', function($name) {});
Route::get('imagenes/empresa/logo', 'SiteController@getLogo');
Route::get('sitio/imagen-inicio', 'SiteController@getIndexImage');

// Autorización
Auth::routes();