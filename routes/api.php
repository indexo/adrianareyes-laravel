<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Administración - inicio
Route::prefix('admin')->group(function() {
	// Contadores

	Route::get('ambientes/total', 'APIController@totalEnvironments'); 							// Ambientes (comodidades)
	Route::get('equipamientos/total', 'APIController@totalEquipment'); 					    // Equipamiento
	Route::get('exterior/total', 'APIController@totalExterior'); 								// Exterior
	Route::get('servicios-comodidades/total', 'APIController@totalServicesAmenities'); 		// Servicios y Comodidades (amenities)
	Route::get('inmuebles/total', 'APIController@totalProperties');  							// Inmuebles
	Route::get('inmuebles/apartamentos/total', 'APIController@totalApartments'); 				// Apartamentos
	Route::get('inmuebles/locales-comerciales/total', 'APIController@totalBusiness_premises'); // Locales Coemrciales
	Route::get('inmuebles/chacras-campos/total', 'APIController@totalCountrysides'); 			// Chacras / Campos
	Route::get('inmuebles/casas/total', 'APIController@totalHouses'); 							// Casas
	Route::get('inmuebles/terrenos/total', 'APIController@totalLands'); 						// Terrenos
	Route::get('inmuebles/alquileres/total', 'APIController@totalRents'); 						// Alquileres
	Route::get('inmuebles/ventas/total', 'APIController@totalSales');  						// Ventas
	Route::get('inmuebles/alquilados/total', 'APIController@totalRented'); 					// Inmuebles alquilados
	Route::get('inmuebles/vendidos/total', 'APIController@totalSold');  						// Inmuebles vendidos
	Route::get('actividades', 'APIController@getActivities'); 		    	        			// Actividades
	Route::get('usuarios/total', 'APIController@totalUsers'); 									// Usuarios
	Route::get('usuarios/activos/total', 'APIController@totalActiveUsers'); 					// Usuarios activos
	Route::get('usuarios/inactivos/total', 'APIController@totalInactiveUsers'); 				// Usuarios inactivos
	Route::get('oficinas/total', 'APIController@totalOffices'); 								// Oficinas
	Route::get('clientes/total', 'APIController@totalCustomers');								// Clientes

	// /Contadores


	// Getters ORM

	Route::get('ambientes/{total?}', 'APIController@getEnvironments', function ($total = null) {}); 							// Ambientes (comodidades)
	Route::get('equipamientos/{total?}', 'APIController@getEquipment', function ($total = null) {});  							// Equipamiento
	Route::get('exterior/{total?}', 'APIController@getExterior', function ($total = null) {}); 								// Exterior
	Route::get('servicios-comodidades/{total?}', 'APIController@getServicesAmenities', function ($total = null) {});			// Servicios y Comodidades (amenities)
	Route::get('inmuebles/apartamentos/{total?}', 'APIController@getApartments', function ($total = null) {}); 				// Apartamentos
	Route::get('inmuebles/locales-comerciales/{total?}', 'APIController@getBusiness_premises', function ($total = null) {});	// Locales Coemrciales
	Route::get('inmuebles/chacras-campos/{total?}', 'APIController@getCountrysides', function ($total = null) {}); 			// Chacras / Campos
	Route::get('inmuebles/casas/{total?}', 'APIController@getHouses', function ($total = null) {});							// Casas
	Route::get('inmuebles/terrenos/{total?}', 'APIController@getLands', function ($total = null) {}); 							// Terrenos
	Route::get('inmuebles/alquileres/{total?}', 'APIController@getRents', function ($total = null) {}); 						// Alquileres
	Route::get('inmuebles/ventas/{total?}', 'APIController@getSales', function ($total = null) {}); 							// Ventas
	/*Route::get('inmuebles/vendidos/{total?}', 'APIController@getSold', function ($total = null) {}); 							// Propiedades vendidas*/
	Route::get('actividades/{total?}', 'APIController@getActivities', function ($total = null) {}); 							// Actividades
	Route::get('inmuebles/{id}/invitados', 'APIController@getGuestroom', function ($id) {}); 									// Casas de invitados
	Route::get('inmuebles/{total?}', 'APIController@getProperties', function ($total = null) {}); 								// Propiedades
	Route::get('usuarios/{total?}', 'APIController@getUsers', function ($total = null) {}); 									// Usuarios
	Route::get('usuarios/activos/{total?}', 'APIController@activeUsers', function ($total = null) {}); 						// Usuarios activos
	Route::get('usuarios/inactivos/{total?}', 'APIController@inactiveUsers', function ($total = null) {}); 					// Usuarios Inactivos
	Route::get('eventos/{total?}', 'APIController@getEvents', function ($total = null) {}); 									// Eventos - GET
	Route::get('oficinas/{total?}', 'APIController@getOffices', function ($total = null) {});									// Oficinas
	Route::get('clientes/{total?}', 'APIController@getCustomers', function ($total = null) {}); 	 							// Clientes

    // /Getters ORM

	Route::prefix('sitio')->group(function() {
		// Servicios (Introducción)
		Route::get('servicios/introduccion', 'APIController@getServicesIntroduction');
		// Servicios
		Route::get('servicios', 'APIController@getServices');
	}); // Sitio - final

    Route::prefix('mensajes')->group(function() {
        Route::get('/sin-responder', 'APIController@getUnansweredMessages');
        Route::get('/{id?}', 'APIController@getMessages', function($id = null) {});
    });
	
	Route::get('uso', 'APIController@getUsageLocation');

});
// Administración - final

Route::prefix('sitio')->group(function() {
	// Servicios (Introducción)
	Route::get('servicios/introduccion', 'APIController@getServicesIntroduction');
	// Servicios
	Route::get('servicios', 'APIController@getServices');
});
// /Sitio

Route::prefix('inmuebles')->group(function() {
	Route::get('paises/{id?}', 'APIController@getCountries', function($id = null) {}); 		// Paises
	Route::get('pais/{id}', 'APIController@getCountry', function($id) {});						// País

	Route::get('departamentos/{id?}', 'APIController@getDepartments', function($id = null) {});// Departamentos
	Route::get('departamento/{id}', 'APIController@getDepartment', function($id) {});			// Departamento

	Route::get('localidades/{id?}', 'APIController@getLocalities', function($id = null) {});	// Localidades
	Route::get('localidad/{id}', 'APIController@getLocation', function($id) {});				// Localidad

	Route::get('zonas/{id?}', 'APIController@getZones', function($id = null) {});				// Zonas

	Route::get('ultimos/{total?}', 'APIController@lastProperties', function($total = null) {});// Últimos inmuebles
	Route::get('mostrar/{id}', 'APIController@loadProperty', function($id) {});				// Inmueble
	Route::get('{total?}', 'APIController@loadProperties', function($total = null) {});		// Inmuebles

}); // /Inmuebles
